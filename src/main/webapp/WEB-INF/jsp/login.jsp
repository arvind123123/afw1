<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<script src="/resources/zoom-slider/jquery-1.12.4.min.js"></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="header.jsp" />         
		<div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Login</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Login</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="register_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <form:form class="form_contact" action="login" modelAttribute="userForm">
                            <h1>Login</h1>
                            <form:input placeholder="Your Email" class="form-control" path="email" />
                            <form:password placeholder="Password" class="form-control" path="password" />
							<form:hidden path="siteId" value="1"/>
							<form:hidden path="productId"/>
                            <p class="c_checkbox">
                                <label>
                                    <input type="checkbox" name="agree" />
                                    <i></i>
                                    Remember me
                                </label>
                                <a href="javascript:;">Forgot password?</a>
                            </p>
                            <a class="c_btn1" href="javascript:;">
                                <span><input class="sing_btn_bg" type="submit" value="Login"></span>
                                <i></i>
                            </a> 
							 
                            <div class="or_line"><span>OR</span></div><!--or_line-->
                            <p class="social_media">
                                <a href="javascript:;" target="_blank" class="facebook"><i class="fa fa-facebook"></i> Facebook</a>
                                <a href="javascript:;" target="_blank" class="pinterest"><i class="fa fa-pinterest"></i> Pinterest</a>
                            </p>
                            <p class="already_account">Don't Have an Account? <a href="signup">Sign up now</a></p>
                            <div class="clearfix"></div><!--clearfix-->
							<p>${success}</p>
                        </form:form>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--register_area-->
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />        
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
<script src="/resources/js/jquery.min.js"></script>
<script src="/resources/js/jquery.validate.min.js"></script>
</body>
<script>
$(document).ready(function(){

	$("#userForm").validate({
		 rules:{
			email : {
				 required : true,
				 email: true
			 },
			 password: {
			  required: true
			 }
		 },
		  errorPlacement: function(error, element) {   },
		   submitHandler: function(form) {
			 
				form.submit();
			 
		  }
	 })
	 
})	 
</script>
</html>