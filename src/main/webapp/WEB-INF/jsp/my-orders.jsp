<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
    </head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="header.jsp" />          
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">My Orders</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li>Orders</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
     <c:forEach items="${loginresponse.productList}" var="order">
	 <img src="${order.imageUrl}" width="100" height="100" /> <br>
	Name :  ${order.name} <br>
	Price : ${order.price} <br>
	Status : ${order.brand}
	 </c:forEach>
	 
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />           
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>