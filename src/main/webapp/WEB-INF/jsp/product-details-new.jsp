<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />

    <link href="/resources/zoom-slider/jquery.exzoom.css?55" rel="stylesheet" type="text/css"/>

<script src="/resources/zoom-slider/jquery-1.12.4.min.js"></script>


<script src="/resources/zoom-slider/imagesloaded.pkgd.min.js"></script>
<script src="/resources/zoom-slider/jquery.exzoom.js"></script>

<script src="/resources/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$('.zoom_slider').imagesLoaded( function() {
$("#exzoom").exzoom({
autoPlay: false,
});
$("#exzoom").removeClass('hidden')
});
</script>
</head>
<body id="scroll_top">
    <div class="add_to_cart_alert_box animated bounceInDown" style="display:none;">
        <p>Item Added To Cart</p>
    </div>


    <div class="main">
<jsp:include page="/header" />
        <div class="breadcrumb_area breadcrumb_section_img">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Product Details</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Product Details</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
		
		
		
		<c:set var="reviewCount" value="${fn:length(productDetail.reviews)}" />
		
        <div class="main_container">
            <div class="container">
                <div class="row">
                  <!--  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h1>Product Details</h1>
                    </div> -->
					
					<form name="addtocart" id="addtocart" action="" method="post">
		<input type="hidden" name="productId" id="productId" value="${productDetail.productId}" />
		<input type="hidden" name="quantity" id="quantity" value="1" />
		<input type="hidden" name="maxQuantity" id="maxQuantity" value="${productDetail.quantity}" />
		<input type="hidden" name="price" id="price" value="${productDetail.price}" />
		<input type="hidden" name="productName" id="productName" value="${productDetail.name}" />
		<input type="hidden" name="productImage" id="productImage" value="https://drive.google.com/uc?export=view&id=${productDetail.images[0]}" />
		<input type="hidden" name="subSubCatId" id="subSubCatId" value="${productDetail.subSubCatId}" />
		
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 zoom_slider">
                        <div class="product_details_area">
                            <div class="exzoom hidden" id="exzoom">
                                <div class="exzoom_img_box">
                                    <ul class='exzoom_img_ul'>
									<c:forEach items="${productDetail.images}" var="image">
                                      <!--  <li><img src="https://drive.google.com/uc?export=view&id=${image}"/></li>  -->
									  <li><img src="https://drive.google.com/thumbnail?id=${image}&sz=w500-h500"/></li>
                                    </c:forEach>    
                                    </ul>
                                </div><!--exzoom_img_box-->
                                <div class="exzoom_nav"></div><!--exzoom_nav-->
                                <p class="exzoom_btn">
                                    <a href="javascript:void(0);" class="exzoom_prev_btn">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="exzoom_next_btn">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </p>
                            </div><!--exzoom-->
                            <div class="product_details_area_right">
                                <p class="head"><a href="javascript:;">${productDetail.name}</a></p>
                                <ul class="cost_stars">
                                    <li>
                                        <span class="cost"><i class="fa fa-${currency}" aria-hidden="true"></i>${productDetail.sellingPrice}</span>
                                        <span class="cost_line"><i class="fa fa-${currency}" aria-hidden="true"></i>${productDetail.price}</span>
                                      <!--  <span class="cost_off">35% Off</span> -->
                                    </li>
                                    <li>
									<c:forEach  begin="1" end="${productDetail.rating}">
                                        <i class="fa fa-star"></i>
                                      </c:forEach>  
									  <c:forEach  begin="${productDetail.rating+1}" end="5">
                                        <i class="fa fa-star-o"></i>
										</c:forEach> 
                                        <span>(${reviewCount})</span>
                                    </li>
                                </ul><!--cost_stars-->
                                <p class="text">${productDetail.shortDescription}</p>
                                <ul class="icon_list">
                                    <li><i class="fa fa-shield"></i> 1 Year ${productDetail.brandName} Brand Warranty</li>
									<c:if test="${productDetail.returndays > 0}">
									<li><i class="fa fa-undo"></i> ${productDetail.returndays} Day Return Policy</li>
									</c:if>
									<c:if test="${productDetail.returndays == 0}">
									<li><i class="fa fa-undo"></i> No Return Policy</li>
									</c:if>
                                    
                                  <!--  <li><i class="fa fa-money"></i> Cash on Delivery available</li> -->
                                </ul>
                               
								
																
								 <c:forEach items="${productDetail.features}" var="feature">
								 <c:if test="${feature.name == 'Size'}">
                                <p class="sizes_radio">
                                    <span>${feature.name}</span>
									<c:forEach items="${feature.values}" var="value">
                                    <label>
                                        <input type="radio" name="${feature.name}" value="${value}">
                                        <i>${value}</i>
                                    </label>
                                   </c:forEach>
                                </p>
								</c:if>
								<c:if test="${feature.name == 'Color'}">
								 <p class="radio_buttons">
                                    <span>${feature.name}</span>
									<c:forEach items="${feature.values}" var="value">
                                    <label>
                                        <input type="radio" name="${feature.name}" value="${value}">
                                        <i class="back-${fn:toLowerCase(value)}"></i>
                                    </label>
                                     </c:forEach>                                   
                                </p>
								</c:if>
								</c:forEach>  
                                <div class="min_and_max">
                                    <input id="demo0" type="text" readonly="true" value="01" name="demo0" data-bts-min="1" data-bts-max="${productDetail.quantity}" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-success" data-bts-button-up-class="btn btn-success">
                                   <!--  <a class="c_btn1" href="javascript:;">
                                        <span class="addtocart"><i class="fa fa-shopping-cart"></i> Add to cart</span>
                                        <i></i>
                                    </a> -->
									<!-- <input type="submit" value="submit"> -->
                                    <button class="c_btn1 btn_me" type="submit" id="cartButton">
                                        <span class="addtocart"><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                    </button>

                                    <a class="glyphicon glyphicon-transfer icons" href="/product/compare/add/${productDetail.productId}"></a>
                                    <a class="fa fa-heart icons" href="/product/addtowishlist/${productDetail.productId}"></a>
                                </div><!--min_and_max-->
                                <p class="category"><span>SKU:</span> ${productDetail.code}</p>
                                <p class="category"><span>Category:</span> ${productDetail.category}</p>
                                <p class="category"><span>Tags:</span> ${productDetail.tags}</p>
                                <p class="category">
                                    <span>Share:</span>
									
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.thelabelbar.com/product/detail/${productDetail.productId}" target="_blank" class="fa fa-facebook"></a>
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.thelabelbar.com/product/detail/${productDetail.productId}" target="_blank" class="fa fa-linkedin"></a>
									<a href="https://twitter.com/share?url=https://www.thelabelbar.com/product/detail/${productDetail.productId}" class="fa fa-twitter" data-show-count="false"></a>
                                   
                            </div><!--product_details_area_right-->
                        </div><!--product_details_area-->
                    </div><!--cols-->
					</form>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="accordion_area">
                             <div class="panel-group" id="accordion">
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <p class="panel-title">
                                        <a class="active" data-toggle="collapse" data-parent="#accordion" href="#collapse1">Description</a>
                                       <c:if test="${fn:length(productDetail.additionalList) > 0}"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Additional info</a> </c:if>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Reviews (${reviewCount})</a>
                                      </p>
                                    </div><!--panel-heading-->
                                      
                                      
                                    <div id="collapse1" class="panel-collapse collapse in">
                                      <div class="panel-body">
                                         ${productDetail.description} 
                                      </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                                    <div id="collapse2" class="panel-collapse collapse">
                                      <div class="panel-body">
                                          <div class="table_additional_info">
										  <c:forEach items="${productDetail.additionalList}" var="add">
                                              <ul>
                                                <li>${add.key}</li>
                                                <li>${add.value}</li>
                                              </ul>
                                           </c:forEach>   
                                          </div><!--table_additional_info-->
                                      </div><!--panel-body-->
                                    </div><!--panel-collapse-->


                                    <div id="collapse3" class="panel-collapse collapse">
                                      <div class="panel-body">
                                          <p class="head">${reviewCount} Review For <span>${productDetail.name}</span></p>
										  <c:forEach items="${productDetail.reviews}" var="review">
                                          <ul class="reviews_area">
                                          <!--  <img src="images/reviews-1.jpg" />  -->
                                            <li>
                                                <p class="name">${review.name}</p>
                                                <p class="date_star">
                                                    <span> <fmt:parseDate value = "${review.date}" var="datee" pattern = "yyyy-MM-dd" />
													<fmt:formatDate value="${datee}" pattern = "MMM dd, yyyy" />
													</span>
													<c:set var="startRating" value="${review.rating}" />
													<c:set var="half" value="false" />
													<c:if test="${fn:contains(review.rating,'.')}">
													<c:set var="half" value="true" />
													<c:set var="startRating" value="${fn:split(review.rating,'.')[0]}" />
													</c:if>
                                                    <span>
													<c:forEach begin="1" end="${startRating}">
                                                        <i class="fa fa-star"></i>
                                                    </c:forEach>
													<c:if test="${half == 'true'}">
													<i class="fa fa-star-half-o"></i>
													<c:set var="startRating" value="${startRating+1}" />
													</c:if>		
													<c:forEach end="5" begin="${startRating+1}">
                                                        <i class="fa fa-star-o"></i>
                                                    </c:forEach> 		
                                                        
                                                    </span>
                                                </p>
                                                <p class="text">${review.reviewText}</p>  
                                            </li>
                                          </ul>
                                          </c:forEach>
                                          <p class="head">Add a review</p>
                                          <form class="review_form" name="reviewForm" id="reviewForm" action="" method="post">
                                            <fieldset class="star_rating">
                                                <input type="radio" id="star5" name="rating" value="5" />
                                                <label class = "full" for="star5" title=""></label>

                                                <input type="radio" id="star4half" name="rating" value="4.5" />
                                                <label class="half" for="star4half" title=""></label>

                                                <input type="radio" id="star4" name="rating" value="4" />
                                                <label class = "full" for="star4" title=""></label>

                                                <input type="radio" id="star3half" name="rating" value="3.5" />
                                                <label class="half" for="star3half" title=""></label>

                                                <input type="radio" id="star3" name="rating" value="3" />
                                                <label class = "full" for="star3" title=""></label>

                                                <input type="radio" id="star2half" name="rating" value="2.5" />
                                                <label class="half" for="star2half" title=""></label>

                                                <input type="radio" id="star2" name="rating" value="2" />
                                                <label class = "full" for="star2" title=""></label>

                                                <input type="radio" id="star1half" name="rating" value="1.5" />
                                                <label class="half" for="star1half" title=""></label>

                                                <input type="radio" id="star1" name="rating" value="1" checked="checked"/>
                                                <label class = "full" for="star1" title=""></label>

                                                <input type="radio" id="starhalf" name="rating" value="half" />
                                                <label class="half" for="starhalf" title=""></label>
                                            </fieldset>
                                            <textarea class="form-control" name="reviewText" id="reviewText" placeholder="Your review*"></textarea>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <input type="text" class="form-control textOnly" name="name" id="name" placeholder="Enter Name*" />
                                                </div><!--cols-->
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email*" />
                                                </div><!--cols-->
                                            </div><!--row-->
                                          <!--  <a class="c_btn1" href="javascript:void(0)">
                                                <span>Submit Review</span>
                                                <i></i>
                                            </a> -->
											 <button class="c_btn1 btn_me" type="submit">
												<span class="addtocart">Submit Review</span>
											</button>
											
                                          </form>
										  <span id="reviewSuccess"></span>
                                      </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                                  </div><!--panel panel-default-->
                                </div><!--panel-group-->
                        </div><!--accordion_area-->
                    </div><!--cols-->




                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div id="collapse1" class="panel-collapse collapse in">
                                      <div class="panel-body">
                                         ${productDetail.description} 
                                      </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div id="collapse2" class="panel-collapse collapse">
                                      <div class="panel-body">
                                          <div class="table_additional_info">
                                          <c:forEach items="${productDetail.additionalList}" var="add">
                                              <ul>
                                                <li>${add.key}</li>
                                                <li>${add.value}</li>
                                              </ul>
                                           </c:forEach>   
                                          </div><!--table_additional_info-->
                                      </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                    </div>

                    <div class="col-xs-12 col-lg-12">
                        <div class="revier_section">
                            
                                    <div id="collapse3" class="panel-collapse collapse">
                                      <div class="panel-body">
                                          <p class="head">${reviewCount} Review For <span>${productDetail.name}</span></p>
                                          <c:forEach items="${productDetail.reviews}" var="review">
                                          <ul class="reviews_area">
                                          <!--  <img src="images/reviews-1.jpg" />  -->
                                            <li>
                                                <p class="name">${review.name}</p>
                                                <p class="date_star">
                                                    <span> <fmt:parseDate value = "${review.date}" var="datee" pattern = "yyyy-MM-dd" />
                                                    <fmt:formatDate value="${datee}" pattern = "MMM dd, yyyy" />
                                                    </span>
                                                    <c:set var="startRating" value="${review.rating}" />
                                                    <c:set var="half" value="false" />
                                                    <c:if test="${fn:contains(review.rating,'.')}">
                                                    <c:set var="half" value="true" />
                                                    <c:set var="startRating" value="${fn:split(review.rating,'.')[0]}" />
                                                    </c:if>
                                                    <span>
                                                    <c:forEach begin="1" end="${startRating}">
                                                        <i class="fa fa-star"></i>
                                                    </c:forEach>
                                                    <c:if test="${half == 'true'}">
                                                    <i class="fa fa-star-half-o"></i>
                                                    <c:set var="startRating" value="${startRating+1}" />
                                                    </c:if>     
                                                    <c:forEach end="5" begin="${startRating+1}">
                                                        <i class="fa fa-star-o"></i>
                                                    </c:forEach>        
                                                        
                                                    </span>
                                                </p>
                                                <p class="text">${review.reviewText}</p>  
                                            </li>
                                          </ul>
                                          </c:forEach>
                                          <p class="head">Add a review</p>
                                          <form class="review_form" name="reviewForm" id="reviewForm" action="" method="post">
                                            <fieldset class="star_rating">
                                                <input type="radio" id="star5" name="rating" value="5" />
                                                <label class = "full" for="star5" title=""></label>

                                                <input type="radio" id="star4half" name="rating" value="4.5" />
                                                <label class="half" for="star4half" title=""></label>

                                                <input type="radio" id="star4" name="rating" value="4" />
                                                <label class = "full" for="star4" title=""></label>

                                                <input type="radio" id="star3half" name="rating" value="3.5" />
                                                <label class="half" for="star3half" title=""></label>

                                                <input type="radio" id="star3" name="rating" value="3" />
                                                <label class = "full" for="star3" title=""></label>

                                                <input type="radio" id="star2half" name="rating" value="2.5" />
                                                <label class="half" for="star2half" title=""></label>

                                                <input type="radio" id="star2" name="rating" value="2" />
                                                <label class = "full" for="star2" title=""></label>

                                                <input type="radio" id="star1half" name="rating" value="1.5" />
                                                <label class="half" for="star1half" title=""></label>

                                                <input type="radio" id="star1" name="rating" value="1" checked="checked"/>
                                                <label class = "full" for="star1" title=""></label>

                                                <input type="radio" id="starhalf" name="rating" value="half" />
                                                <label class="half" for="starhalf" title=""></label>
                                            </fieldset>
                                            <textarea class="form-control" name="reviewText" id="reviewText" placeholder="Your review*"></textarea>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <input type="text" class="form-control textOnly" name="name" id="name" placeholder="Enter Name*" />
                                                </div><!--cols-->
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email*" />
                                                </div><!--cols-->
                                            </div><!--row-->
                                          <!--  <a class="c_btn1" href="javascript:void(0)">
                                                <span>Submit Review</span>
                                                <i></i>
                                            </a> -->
                                             <button class="c_btn1 btn_me" type="submit">
                                                <span class="addtocart">Submit Review</span>
                                            </button>
                                            
                                          </form>
                                          <span id="reviewSuccess"></span>
                                      </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                        </div>
                    </div>
                </div><!--row-->
            </div><!--container-->
        </div><!--main_container-->
		
		<c:if test="${fn:length(productDetail.similarProducts) > 0}">
        <div class="featured_products">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Similar Products</h2>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 featured_products_slider">
			<c:forEach items="${productDetail.similarProducts}" var="product">
                <div class="item">
                    <div class="product_area">
                        <div class="image_area">
                            <img src="https://drive.google.com/uc?export=view&id=${product.imageUrl}" />
                            <span class="new">New</span>
                            <p>
                                <a href="/product/detail/${product.productId}" class="fa fa-shopping-cart"></a>
                                    <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                            </p>
                        </div><!--image_area-->
                        <div class="head_cost_star_area">
                            <p class="head">
                               <a href="/product/detail/${product.productId}">${product.name}</a>
                            </p>
                             <p class="cost">
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                                  <!--  <span>35% Off</span> -->
                                </p>
                            <p class="stars">
                                <c:forEach  begin="1" end="${product.rating}">
                                        <i class="fa fa-star"></i>
                                      </c:forEach>  
									  <c:forEach  begin="${product.rating+1}" end="5">
                                        <i class="fa fa-star-o"></i>
										</c:forEach> 
                                <span>(${product.ratingCount})</span>
                            </p>
                          <!--  <p class="radio_buttons">
                                <label>
                                    <input type="radio" name="radio" checked />
                                    <i class="back-brown"></i>
                                </label>
                                <label>
                                    <input type="radio" name="radio" />
                                    <i class="back-grey"></i>
                                </label>
                                <label>
                                    <input type="radio" name="radio" />
                                    <i class="back-red"></i>
                                </label>
                            </p> -->
                        </div><!--head_cost_star_area-->
                    </div><!--product_area-->
                </div><!--item-->
                </c:forEach>
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--featured_products-->        
</c:if>
<!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />        
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>

<script src="/resources/js/js.js"></script>
<script>
function gotocart(){
	window.location.href= "/product/viewshoppingcart";
}
$(document).ready(function(){
	$('.textOnly').keyup(function () {
        		  if (this.value != this.value.replace(/[^a-zA-Z ]+/g, '')) {
        		       this.value = this.value.replace(/[^a-zA-Z ]+/g, '');
        		    }
        	});
			
	$("form[name='addtocart']").validate({
		 rules:{
			Color:{
				required : true
			},
			
			 Size: {
			  required: true
			 }
		 },
		  errorPlacement: function(error, element) {   },
		   errorClass : "bob",
				highlight: function(element) {
					$(element).parent().addClass("error");
				},
				unhighlight: function(element) {
					$(element).parent().removeClass("error");
				},
		   submitHandler: function(form) {
				alert("submit");
				var data = {
									productId: $("#productId").val(),
									quantity: $("#demo0").val(),
									price: $("#price").val(),
									productName: $("#productName").val(),
									productImage: $("#productImage").val(),
									maxQuantity: $("#maxQuantity").val(),
									size: $("input[name='Size']:checked").val(),
									color: $("input[name='Color']:checked").val(),
									subSubCatId: $("#subSubCatId").val(),
								}
							$.ajax({
								url: '/product/addtocart',
								type: 'post',
								contentType: 'application/json; charset=utf-8',
								data: JSON.stringify(data),
								dataType: "html",
								success: function(response) {
									//console.log(response)
									$("#cartPopup").html(response);
									$(".add_to_cart_alert_box").show().delay(5000).fadeOut();
									$("#cartButton").attr('onclick', 'gotocart();');
									$("#cartButton").attr('type', 'button');
									$(".addtocart").html("<i class='fa fa-shopping-cart'></i> Go to Cart");
								}
							});
			 
		  }
	 })
	 
	 
	$("form[name='reviewForm']").validate({
		 rules:{
			rating:{
				required : true
			},
			
			 name: {
			  required: true
			 },
			reviewText:{
				required : true
			},
			 email: {
			  required: true,
			  email:true
			 }
			 
		 },
		  errorPlacement: function(error, element) {   },
		   submitHandler: function(form) {
				
				var data = {
							name : $("#name").val(),
							reviewText : $("#reviewText").val(),
							rating : $('input[name=rating]:checked').val(),
							email : $("#email").val(),
							productId: $("#productId").val()							
								}
							$.ajax({
								url: '/product/addreview',
								type: 'post',
								contentType: 'application/json; charset=utf-8',
								data: JSON.stringify(data),
								dataType: "text",
								success: function(response) {
									console.log(response)
									$("#reviewSuccess").html(response);
								}
							});
			 
		  }
	 })		

	 
})
</script>
<script src="/resources/js/plus-minus.js">
/* min and max number */
    
</script>
<script>
    $("input[name='demo0']").TouchSpin();
		 
</script>
</body>
</html>