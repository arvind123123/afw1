<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

 <c:if test="${fn:length(cartList) > 0}">   
<a href="javascript:;" class="fa fa-shopping-cart">
    <span>${fn:length(cartList)}</span>
</a>
<div class="dropdown">
    <c:forEach var="cart" items="${cartList}">

    
        <c:set var="cartTotal" value="${cartTotal+(cart.price*cart.quantity)}" />
            <ul class="add_one">
                <li>
                    <a href="javascript:;">
                        <img src="${cart.productImage}" class="img-responsive" />
                    </a>
                </li>
                <li>
                    <a href="javascript:;">${cart.productName}</a>
                    <!--<p class="vendor"><b>Vendor:</b> Edgar Andrew</p>-->
                    <p class="t_price">
                        <i>${cart.quantity}</i>
                        <span>X</span>
                        <b>${cart.price}</b>
                    </p>
                </li>
                <li><i onclick="removeProductCart('${cart.productId}')" class="fa fa-trash"></i></li>
            </ul><!--add_one-->
        </c:forEach>
        <c:if test="${fn:length(cartList) > 0}">
            <p class="total">
                <span class="left">Subtotal:</span>
                <b>${cartTotal}</b>
            </p>
            <p class="buttons">
                <a href="/product/viewshoppingcart" class="btn-1"><span>View Cart</span></a>
                <a href="/cart/checkout" class="btn-2"><span>Check Out</span></a>
            </p>
        </c:if>
</div><!--dropdown-->
</c:if>
<script src="/resources/js/jquery.min.js"></script>
<script>

				
							function removeProductCart(productId){
							$.ajax({
								url: '/product/removecart/'+productId,
								type: 'get',
								contentType: 'application/json; charset=utf-8',
								dataType: "html",
								success: function(response) {
									//console.log(response)
									$("#cartPopup").html(response);
								}
							});
							}

</script>