<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />   
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <!-- <p class="head">Vip Membership</p> -->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li>Terms & Conditions</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <!-- join now -->
<section>
    <div class="terms-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Terms & Conditions</h1>
                    <p>In case you purchase wine through us or use our website, you will have to abide by the terms and conditions of allfinewines.com.au and you can use our services after registering your details on our website. You can process our services through e-mail or phone after registering on our website. The terms and conditions of the use of services can change at any time without any prior intimation.</p>
                </div>
                <div class="col-md-12">
                    <h2>Commitment towards customer</h2>
                    <p>We uphold our commitment to the quality of wines. Under any circumstances, if you disapprove of any of our bottles after purchase, we provide you with another bottle or you get a refund of the purchase price of the bottle within 90 days of the purchasing date. If you disapprove of the whole case of wine, we replace the case or give you a refund at your convenience. We shall collect the case from you at the earliest and initiate the refund or replacement process.</p>
                </div>
                <div class="col-md-12">
                    <h2>Our conditions of delivery-</h2>
                    <ul class="term-list">
                        <li>The delivery charges are levied according to the distance and delivery area. Delivery to rural areas carries extra charges.</li>
                        <li>Our delivery calculator calculates the delivery charges based on check out of delivery.</li>
                        <li>The delivery charges rise if the cost of delivery exceeds our calculations.</li>
                        <li>There are variations in the delivery timings but generally delivery is complete within 5 days of payment.</li>
                        <li>In case we take more time in delivery, we shall intimate you in advance and you can cancel the order if you wish to with the assurance of 100% refund.</li>
                        <li>If you are not available at the time of delivery, we leave the package at the place notified by you (outside your house)</li>
                        <li>If we do not find any safe place for the wine outside your home, we will deliver it to you at a later date. You can get the delivery of wines at your workplace also  </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>Our policy on Stolen Wine and Breakages</h2>
                    <p>If we deliver your order of wines at some other address and the package gets stolen, we provide you with replacement without any extra charges, but the event must be reported with 14 days of occurrence.</p>
                    <p>If the bottles get damaged during delivery, we provide you with free replacement, but the event must be reported with 14 days of delivery.</p>
                    <p>Allfinewines (GAP VENTURES PTY LTD) can change some of the bottles ordered by you without any prior notice if we run out of sufficient stock at the time of delivery.</p>
                    <p>The replacement shall be made with equivalent or better quality bottles. If you are not satisfied with the equality of replacements, we provide your credit in its place.</p>
                    <p>If we do not find proper replacements, we delivery the package without the 'out of stock wines'.</p>
                    <p>If you are not satisfied with any of our services you can call us <span>on <a href="tel:+61357464631" target="_blank">+61 357464631</a> .or </span>notify us through the mail. <span>Our mail id is- <a href="mailto:info@allfinewines.com.au" target="_blank">info@allfinewines.com.au</a>.</span></p>
                </div>
                <div class="col-md-12">
                    <h2>Termination rights</h2>
                    <p>All the customers abide by the customer commitment which is for the general good of all our customers. We reserve the right to terminate your account if you are found to be in breach of the customer commitment at any time. If you make any purchase through another account, our customer commitment shall be limited to that particular account and not to all other account used by you earlier which now stand terminated.</p>
                </div>
                <div class="col-md-12">
                    <h2>Prices and payment module</h2>
                    <p>All the prices shall be quoted in Australian dollars. Delivery charges shall be in addition to the product price. GST shall be included in the product price itself.</p>
                    <p>You can complete payment through Visa, Mastercard, American Express and direct credit to our bank account. All the payments will be processed in Australian dollars. The goods will not be shipped without complete payment of the goods.</p>
                </div>
                <div class="col-md-12">
                    <h2>Comparison of price</h2>
                    <p>We provide several comparisons in our e-mails and website.</p>
                </div>
                <div class="col-md-12">
                    <h2>Estimated retail price</h2>
                    <p>Allfinewines (GAP VENTURES PTY LTD) sells several wines which are imported from outside Australia or we source them from wineries operating in Australia. the savings generated in these events are endorsed to the customers purchasing from us. The estimated retail price gives us an assumption of the price of similar quality wines in Australia.  </p>
                    <strong>Restriction of age</strong>
                    <p>The wine will not be sold to anyone who is not 18 years old or more. While placing the order you have to confirm that you have crossed the age bar of 18 years. If the wine is ordered as a gift package, the recipient must be over 18 years of age. We ask for ID to cross-check the age in case of any doubt at the time of delivery of wine. We do not deliver the wine if the recipient is not able to provide a satisfactory ID.</p>
                </div>
                <div class="col-md-12">
                    <h2>Return policy </h2>
                    <ul class="term-list">
                        <li>If the bottles are damaged, broken or spoiled at the time of delivery, you may return the whole case of the damaged battles for replacement or refund- as per your discretion.</li>
                        <li>The wines that are due to be returned must be notified with 14 days of delivery or else we are not liable for any compliance.</li>
                        <li>The refunds or reimbursements will be completed within 30 days of the date of notification provided by you. To fulfill this clause the wine should be collected by our carrier.</li>
                        <li>Your statutory rights to damaged and faulty goods shall remain intact without any modifications.</li>
                        <li>If the purchases get cancelled before shipment, the refund process is initiated and completed within 30 days of notification</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>Warranty</h2>
                    <p>We assure with a warranty that the wines shall be of satisfactory quality.</p>
                </div>
                <div class="col-md-12">
                    <h2>Limitation of liability</h2>
                    <ul class="term-list">
                        <li>Allfinewines (GAP VENTURES PTY LTD) shall not be liable for any loss or damage in the following conditions-</li>
                        <li>when the loss or damage was unforeseeable by both parties</li>
                        <li>When the supplier is not responsible for breach of contract and the resulting loss or damage of the shipment.</li>
                        <li>When the non-consumers or related businesses suffer any loss or damage.</li>
                        <li>When the wine gets damaged by mishandling on the part of the customer. We are not liable for any spillages <strong>or breakages</strong></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>Warning to customers</h2>
                    <ul class="term-list">
                        <li>Consume alcohol in moderation.</li>
                        <li>Wine cases are heavy so transfer 1 or 2 bottles at a time to avoid unnecessary damage.</li>
                        <li>There are chances of staining your clothes (in the case of red wine) if the wine case is not handled properly.</li>
                        <li>Take precaution while opening wine bottles as it contains gas and is volatile.</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>Protection of Data</h2>
                    <p>We maintain, record, hold and use the information your information which you provide us at the time of making any purchase through our website. This information helps us in improving our services and promotions.</p>
                </div>
                <div class="col-md-12">
                    <h2>Disclaimer:</h2>
                    <p>We shall not be liable for the delay due to unforeseen factors which are beyond our control such as war, riot, any government order or failure of communication and transportation services. Allfinewines (GAP VENTURES PTY LTD) can use these terms and conditions and make changes for security, legal or regulation purposes. Relevant Australian law will be applicable and the disputes will be resolved within the jurisdiction of courts of Australia.</p>
                </div>
            </div>

        </div>
    </div>
</section>



       <jsp:include page="footer.jsp" />

<script type="text/javascript">
    $(document).ready(function() {
  $('.accordion a').click(function(){
    $(this).toggleClass('active');
    $(this).next('.content').slideToggle(400);
   });
});
</script>

       </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>