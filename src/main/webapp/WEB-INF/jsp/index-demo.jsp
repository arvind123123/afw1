<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>

<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>




</head>
<body id="scroll_top">
    <div class="main">
        <jsp:include page="header.jsp" />  

<div class="banner" style="background:#ffe4d3;">
    <!-- <img alt="Banner" src="/resources/images/banner2.jpg"> -->
    <div class="bnrboxtxt_S">
        <span class="" style="border-color:#f9d6c2;">
            <span class="man_fashion" style="background:#ffe4d3;">
                <!-- <p class="get_upto">50% off in all products</p> -->
                <p class="head">Shop For Christmas</p>
            </span>    
            <!-- <p class="show_now">
                <a class="c_btn1" href="javascript:;">
                    <span>Show Now</span>
                    <i></i>
                </a>
            </p> -->
        </span>
    </div>
    <span class="banner_categroyS">
        <a href="/searchproduct?item=muffler">
            <img src="/resources/images/man-F.jpg" class="img-circle img-responsive">
            <div>Men's Clothing</div>
        </a>
        <a href="/searchproduct?item=women-clothing">
            <img src="/resources/images/woman-F.jpg" class="img-circle img-responsive">
            <div>Women's Clothing</div>
        </a>
        <a href="/searchproduct?item=women%27s-kurti">
            <img src="/resources/images/ethnic-F.jpg" class="img-circle img-responsive">
            <div>Ethnic Clothing</div>
        </a>
        <a href="/searchproduct?item=protective-face-mask">
            <img src="/resources/images/health-F.jpg" class="img-circle img-responsive">
            <div>Health Care</div>
        </a>
        <a href="/searchproduct?item=laptop-messenger-bag">
            <img src="/resources/images/laptop-F.jpg" class="img-circle img-responsive">
            <div>Bags</div>
        </a>
    </span>
</div>

<div class="banner" style="display:none;">
    <div class="item">
        <div class="banner_image banner-1">
            <div class="banner_text">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span class="bannerBox_txt" style="border-color:#dbecfc;">
                                <span class="woman_fashion" style="background:#ecf4ff;">
                                    <p class="get_upto">Get up to 50% off Today Only!</p>
                                    <p class="head">Woman Fashion</p>
                                </span>
                                <p class="show_now">
                                    <a class="c_btn1" href="javascript:;">
                                        <span>Show Now</span>
                                        <i></i>
                                    </a>
                                </p>
                            </span>
                        </div><!--cols-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--banner_text-->
        </div><!--banner_image-->
    </div><!--item-->
    <div class="item">
        <div class="banner_image banner-2">
            <div class="banner_text">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span class="bannerBox_txt" style="border-color:#f9d6c2;">
                                <span class="man_fashion" style="background:#ffe4d3;">
                                    <p class="get_upto">50% off in all products</p>
                                    <p class="head">Man Fashion</p>
                                </span>    
                                <p class="show_now">
                                    <a class="c_btn1" href="javascript:;">
                                        <span>Show Now</span>
                                        <i></i>
                                    </a>
                                </p>
                            </span>    
                        </div><!--cols-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--banner_text-->
        </div><!--banner_image-->
    </div><!--item-->
    <div class="item">
        <div class="banner_image banner-3">
            <div class="banner_text">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span class="bannerBox_txt" style="border-color:#c7f7e9;">
                                <span class="summer_fashion" style="background:#d2fef1;">    
                                    <p class="get_upto">Taking your Viewing Experience to Next Level</p>
                                    <p class="head">Summer Fashion</p>
                                </span>
                                <p class="show_now">
                                    <a class="c_btn1" href="javascript:;">
                                        <span>Show Now</span>
                                        <i></i>
                                    </a>
                                </p>
                            </span>    
                        </div><!--cols-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--banner_text-->
        </div><!--banner_image-->
    </div><!--item-->
</div><!--banner-->
        <div class="ads">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <a href="javascript:;" class="main_ads">
                            <div class="image_area">
                                <img src="/resources/images/ads_img1.jpg" />
                            </div><!--image_area-->
                            <div class="text_area" style="background:#ffdbdf">
                                <p class="text">Super Sale</p>
                                <p class="head">New Collection</p>
                                <p class="shop_now"><span>Shop Now</span></p>
                            </div><!--text_area-->
                        </a>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <a href="javascript:;" class="main_ads">
                            <div class="image_area">
                                <img src="/resources/images/ads_img2.jpg" />
                            </div><!--image_area-->
                            <div class="text_area" style="background:#d6f7fe">
                                <p class="sub_head">Sale 40% Off</p>
                                <p class="head">New Season</p>
                                <p class="shop_now"><span>Shop Now</span></p>
                            </div><!--text_area-->
                        </a>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--ads-->
        <div class="products">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h1>Exclusive Products</h1>
                    </div><!--cols-->
                </div><!--row-->
                <div class="row exclusiveProducts_row">
				<c:forEach items="${exclusiveProducts}" var="product">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="product_area">
                            <div class="image_area">
                                <img src="https://drive.google.com/uc?export=view&id=${product.imageUrl}" />
                                <span class="sale"><!-- Sale -->
                                    <img src="/resources/images/label-sale.png">
                                </span>
                                <p>
                                    <a href="/product/detail/${product.productId}" class="fa fa-shopping-cart"></a>
                                    <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                                </p>
                            </div><!--image_area-->
                            <div class="head_cost_star_area">
                                <p class="head">
                                    <a href="/product/detail/${product.productId}">${product.name}</a>
                                </p>
                                <p class="cost">
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                                   <!-- <span>35% Off</span> -->
                                </p>
                                <p class="stars">
                                <c:forEach  begin="1" end="${product.rating}">
                                        <i class="fa fa-star"></i>
                                      </c:forEach>  
									  <c:forEach  begin="${product.rating+1}" end="5">
                                        <i class="fa fa-star-o"></i>
										</c:forEach> 
                                <span>(${product.ratingCount})</span>
                            </p>
                            <!--    <p class="radio_buttons">
                                    <label>
                                        <input type="radio" name="radio" checked />
                                        <i class="back-brown"></i>
                                    </label>
                                    <label>
                                        <input type="radio" name="radio" />
                                        <i class="back-grey"></i>
                                    </label>
                                    <label>
                                        <input type="radio" name="radio" />
                                        <i class="back-red"></i>
                                    </label>
                                </p> -->
                            </div><!--head_cost_star_area-->
                        </div><!--product_area-->
                    </div><!--cols-->
                   </c:forEach>
                </div><!--row-->
            </div><!--container-->
        </div><!--products-->
        <div class="summer_collection">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0">
                        <div class="imgsctn_"><img src="/resources/images/tranding_img.png" /></div>
                        <div class="content_area">
                            <div>
                                <p class="sub_head">Sale Get up to 50% Off</p>
                                <p class="head">Best Summer Collection</p>
                                <p class="text">New season trends!</p>
                                <p>
                                    <a class="c_btn1" href="javascript:;">
                                        <span>Show Now</span>
                                        <i></i>
                                    </a>
                                </p>
                            </div>
                        </div><!--content_area-->
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--summer_collection-->
        <div class="featured_products">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Featured Products</h2>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 featured_products_slider">
			<c:forEach items="${featuredProducts}" var="product">
                <div class="item">
                    <div class="product_area">
                        <div class="image_area">
                            <img src="https://drive.google.com/uc?export=view&id=${product.imageUrl}" />
                            <span class="new">New</span>
                            <p>
                                <a href="/product/detail/${product.productId}" class="fa fa-shopping-cart"></a>
                                    <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                            </p>
                        </div><!--image_area-->
                        <div class="head_cost_star_area">
                            <p class="head">
                                    <a href="/product/detail/${product.productId}">${product.name}</a>
                                </p>
                                <p class="cost">
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                                  <!--  <span>35% Off</span> -->
                                </p>
                            <p class="stars">
                                <c:forEach  begin="1" end="${product.rating}">
                                        <i class="fa fa-star"></i>
                                      </c:forEach>  
									  <c:forEach  begin="${product.rating+1}" end="5">
                                        <i class="fa fa-star-o"></i>
										</c:forEach> 
                                <span>(${product.ratingCount})</span>
                            </p>
                        <!--    <p class="radio_buttons">
                                <label>
                                    <input type="radio" name="radio" checked />
                                    <i class="back-brown"></i>
                                </label>
                                <label>
                                    <input type="radio" name="radio" />
                                    <i class="back-grey"></i>
                                </label>
                                <label>
                                    <input type="radio" name="radio" />
                                    <i class="back-red"></i>
                                </label>
                            </p> -->
                        </div><!--head_cost_star_area-->
                    </div><!--product_area-->
                </div><!--item-->
                </c:forEach>
                
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--featured_products-->        <div class="testinomials">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="padding:0">
                <div class="col-xs-12 col-sm-4 reviewimg_sctn" style="padding:0">
                    <img src="/resources/images/review-b.jpg" class="img-responsive">
                </div>
                <div class="col-xs-12 col-sm-8 review_vewareaS">
                    <div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h2>Our Client Say!</h2>
                        </div><!--cols-->
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 testinomials_slider">
                            <div class="item">
                                <div class="testimonials_area">
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi nam natus perferendis possimus quasi sint sit tempora voluptatem.</p>
                                    <p class="img_name">
                                        <img src="/resources/images/user_img1.jpg" />
                                        <span>
                                            <b>Lissa Castro</b>
                                            <i>Designer</i>
                                        </span>
                                    </p>
                                </div><!--testimonials_area-->
                            </div><!--item-->
                            <div class="item">
                                <div class="testimonials_area">
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi nam natus perferendis possimus quasi sint sit tempora voluptatem.</p>
                                    <p class="img_name">
                                        <img src="/resources/images/user_img2.jpg" />
                                        <span>
                                            <b>Alden Smith</b>
                                            <i>Designer</i>
                                        </span>
                                    </p>
                                </div><!--testimonials_area-->
                            </div><!--item-->
                            <div class="item">
                                <div class="testimonials_area">
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi nam natus perferendis possimus quasi sint sit tempora voluptatem.</p>
                                    <p class="img_name">
                                        <img src="/resources/images/user_img3.jpg" />
                                        <span>
                                            <b>Daisy Lana</b>
                                            <i>Designer</i>
                                        </span>
                                    </p>
                                </div><!--testimonials_area-->
                            </div><!--item-->
                            <div class="item">
                                <div class="testimonials_area">
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi nam natus perferendis possimus quasi sint sit tempora voluptatem.</p>
                                    <p class="img_name">
                                        <img src="/resources/images/user_img4.jpg" />
                                        <span>
                                            <b>John Becker</b>
                                            <i>Designer</i>
                                        </span>
                                    </p>
                                </div><!--testimonials_area-->
                            </div><!--item-->
                        </div><!--cols-->
                    </div>    
                </div>
            </div>    
        </div><!--row-->
    </div><!--container-->
</div><!--testinomials-->        <div class="why_us">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="why_us_area">
                    <p><span><i class="fa fa-truck"></i></span></p>
                    <p class="head">Free Delivery</p>
                    <p class="text">If you are going to use of Lorem, you need to be sure there anything</p>
                </div><!--why_us_area-->
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="why_us_area">
                    <p><span><i class="fa fa-undo"></i></span></p>
                    <p class="head">30 Day Return</p>
                    <p class="text">If you are going to use of Lorem, you need to be sure there anything</p>
                </div><!--why_us_area-->
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="why_us_area">
                    <p><span><i class="glyphicon glyphicon-headphones"></i></span></p>
                    <p class="head">27/4 Support</p>
                    <p class="text">If you are going to use of Lorem, you need to be sure there anything</p>
                </div><!--why_us_area-->
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--why_us-->        
<jsp:include page="footer-demo.jsp" />
            </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>

<script src="/resources/js/js-demo.js"></script>
<script>
function split(t) {
    return t.split(/,\s*/);
}

function extractLast(t) 
{
    return split(t).pop();
}
function monkeyPatchAutocomplete() {
    var oldFn = $.ui.autocomplete.prototype._renderItem;
    $.ui.autocomplete.prototype._renderItem = function(ul, item) {
       // var re = new RegExp("^" + this.term, "i");
      //  var t = item.label.replace(re, "<span>" + this.term + "</span>");
        return $("<li></li>").data("item.autocomplete", item).append("<a>" + this.term + "</a>").appendTo(ul)
    }
}

function monkeyPatchAutocomplete1(){
	var oldFn=$.ui.autocomplete.prototype._renderItem;
	$.ui.autocomplete.prototype._renderItem=function(ul,item){
		//var t=item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+$.ui.autocomplete.escapeRegex(this.term)+")(?![^<>]*>)(?![^&;]+;)","gi"),"<strong class='highlight'>$1</strong>");
		return $("<li></li>").data("item.autocomplete",item).append("<a>"+item.label+"</a>").appendTo(ul)}
		}

		
  
//monkeyPatchAutocomplete1();
//var t = !0, e = !0;
    $("#searchbox").autocomplete({
        source:function(t, e) {
            $.getJSON("/get_search_list", {
                term:extractLast(t.term)
            }, e);
        },
        search:function() {
            var t = extractLast(this.value);
            return t.length < 1 ? !1 :void 0;
        },
        focus:function() {
          
        },
        select:function(e, i) {
			console.log("select");
			console.log(i.item.value);
			var selected = i.item.value;
			selected = selected.toLowerCase().replace(/\s/g, '-');
			window.location.href = "/searchproduct?item="+selected;
			
        }
    })
	/*.data("ui-autocomplete")._renderItem = function (ul, item) {
         return $("<li></li>")
             .data("item.autocomplete", item)
             .append("<a>" + item.label + "</a>")
             .appendTo(ul);
     };*/
</script>
</body>
</html>