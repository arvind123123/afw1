<div class="footer">
    <div class="footer_top_new pb_20">
    <div class="container">
        <div class="row border_bottom_tran">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <p class="head">Information</p>
                <ul class="footer_links">
                    <li><a href="/about-us"><i class="fa fa-angle-right" aria-hidden="true"></i>About Us</a></li>
                    <!-- <li><a href="/wineReserve">Wine Reserve</a></li> -->
                    <li><a href="/vip"><i class="fa fa-angle-right" aria-hidden="true"></i>Platinum</a></li>
                   <!--  <li><a href="/trophyClub">Trophy Club</a></li> -->
                    <!-- <li><a href="/referAndEarn"><i class="fa fa-angle-right" aria-hidden="true"></i>Refer & Earn</a></li> -->
                   <!--  <li><a href="/blog"><i class="fa fa-angle-right" aria-hidden="true"></i>Blog</a></li> -->
                    <li><a href="/contact-us"><i class="fa fa-angle-right" aria-hidden="true"></i>Contact Us</a></li>
                    <li><a href="/faq"><i class="fa fa-angle-right" aria-hidden="true"></i>FAQ</a></li>
                    <li><a href="/delivery-returns"><i class="fa fa-angle-right" aria-hidden="true"></i>Delivery & Returns</a></li>
                    <!-- <li><a href="/wineries"><i class="fa fa-angle-right" aria-hidden="true"></i>Refer & Earn</a></li> -->
                    <li><a href="/wine-regions"><i class="fa fa-angle-right" aria-hidden="true"></i>Wine Regions</a></li>
                </ul>x
            </div><!--cols-->
            <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                <p class="head">Contact Information</p>
<!--                 <p class="contact_info">
                    <i class="fa fa-map-marker"></i> <strong>Address:</strong>   82 Sackville Street,Mernda Victoria 3754
                </p> -->
                <p class="contact_info address-sec">
                    <i class="fa fa-building" aria-hidden="true"></i>
                    <strong>Company:</strong> <span>GAP VENTURES PTY LTD<br>   DBA: ALL FINE WINES</span>
                </p>

                <p class="contact_info">
                    <i class="fa fa-life-ring"></i>
                    <strong>Support:</strong> Monday to Friday | 9 AM - 6 PM AEST
                </p>
                <p class="contact_info">
                    <i class="fa fa-envelope"></i>
                    <strong>Email:</strong><a href="mailto:info@allfinewines.com.au" target="_blank">  info@allfinewines.com.au </a>
                </p>
                <p class="contact_info">
                    <i class="fa fa-phone"></i>
                    <strong>Phone:</strong><a href="tel:+61 357464631" target="_blank"> +61 357464631</a>
                </p>
                <p class="contact_info">
                    <i class="fa fa-comments"></i>
                    <strong>Live Chat:</strong>
                </p>
            </div><!--cols-->
<%--             <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <p class="head">My Account</p>
                <ul class="footer_links border_bottom">
                    <li><a href="javascript:;"><i class="fa fa-angle-right" aria-hidden="true"></i>My Account</a></li>
                    <li><a href="javascript:;"><i class="fa fa-angle-right" aria-hidden="true"></i>Orders History</a></li>
                    &nbsp;
                </ul>
                <div class="rightblock">
                    <div class="row sslfooterli1">
                        <div class="col-xs-6 col-sm-6 col-lg-6">
                            <a href="javascript:;"><img src="/resources/images/paypal.jpg" style="border-right: 1px #ccc solid;padding-right: 10px;margin-right: -3px;" alt="paypal"></a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-lg-6">
                        <a href="javascript:;"><img src="/resources/images/comodossl.jpg" alt="paypal"></a>
                        </div>
                    </div>
                </div>
            </div> --%>
            
            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5">
<!--                 <div class="footer_wine">
                    <img src="/resources/images/google_store.jpg" alt="google_store">
                </div> -->
                <div class="newsletter_form">
                    <form>
                        <span>Subscribe Now</span>
                        <input type="text" required="" class="form-control" placeholder="Enter Email Address">
                        <button type="submit" class="btn-send2" name="submit" value="Submit"><i class="fa fa-envelope"></i></button>
                    </form>
                </div><!--newsletter_form-->
                <p class="social_icons rounded_social">
                    <a href="https://www.facebook.com/allfinewines" target="_blank" class=""><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/allfinewines/" target="_blank" class=""><i class="fa fa-instagram"></i></a>
<%--                <a href="javascript:;" target="_blank" class=""><i class="fa fa-twitter"></i></a>
                    <a href="javascript:;" target="_blank" class=""><i class="fa fa-pinterest"></i></a>
                    <a href="javascript:;" target="_blank" class=""><i class="fa fa-youtube"></i></a> --%>
                </p>
                <p class="right_area">
                    <img src="/resources/images/payment-icon.jpg" class="img-responsive" width="300" height="30" alt="responsive image"/>
                </p>

            </div><!--cols-->
            
        </div><!--row-->
    </div><!--container-->
</div><!--footer-->
<div class="footer-bottom">
    <div class="container">
        <div class="footerleftcon">
            <div class="sloganfooter">"All Fine Wines - Great Wine for Great People"</div>
        </div>
    </div>
</div>
<div class="footer-links">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="footer-link-box">
                    <p>Red Wine</p>
                    <ul class="foo-box">
                        <li><a href="https://www.allfinewines.com.au/red-wine/pinot-noir/tobacco-road-2019-pinot-noir-australia">Tobacco Road 2019 Pinot Noir Australia</a></li>
                        <li><a href="https://www.allfinewines.com.au/red-wine/shiraz/lot-66-shiraz-2018-australia">Lot 66 Shiraz 2018 Australia</a></li>
                        <li><a href="https://www.allfinewines.com.au/red-wine/shiraz/wine-press-shiraz-2018-australia">Wine Press shiraz 2018 Australia</a></li>
                        <li><a href="https://www.allfinewines.com.au/red-wine/shiraz/zilzie-selection-23-deer-shiraz-australia-2018">Zilzie Selection 23 Deer Shiraz Australia 2018</a></li>
                        <li><a href="https://www.allfinewines.com.au/red-wine/merlot/hidden-story-merlot-australia-2020">Hidden Story Merlot Australia 2020</a></li>
                        <li><a href="https://www.allfinewines.com.au/red-wine/merlot/neverland-merlot-2020-australia">Neverland Merlot 2020 AUSTRALIA</a></li>
                        <li><a href="https://www.allfinewines.com.au/red-wine/shiraz/neverland-shiraz-australia-2020">Neverland Shiraz Australia 2020</a></li>
                        <li><a href="https://www.allfinewines.com.au/red-wine/shiraz/waterstone-bridge-shiraz-australia-2019">Waterstone Bridge Shiraz Australia 2019</a></li>
                    </ul>
                </div>
                <div class="footer-link-box">
                    <p>White Wine</p>
                    <ul class="foo-box">
                        <li><a href="https://www.allfinewines.com.au/white-wine/pinot-grigio/tendril-&-vine-pinot-grigio-2020-white-wine-australia">Tendril & vine Pinot Grigio- 2020 - whitewine Australia</a></li>
                        <li><a href="https://www.allfinewines.com.au/white-wine/sauvignon-blanc/hidden-story-sauvignon-blanc-australia-2020">Hidden Story Sauvignon Blanc Australia 2020</a></li>
                        <li><a href="https://www.allfinewines.com.au/sauvignon-blanc/waterstone-bridge-sauvignon-blanc-australia-2020">Waterstone Bridge Sauvignon Blanc Australia 2020</a></li>
                        <li><a href="https://www.allfinewines.com.au/white-wine/chardonnay/hidden-story-chardonnay-australia-2020">Hidden Story Chardonnay Australia 2020</a></li>
                        <li><a href="https://www.allfinewines.com.au/white-wine/chardonnay/neverland-chardonnay-australia-2020">Neverland Chardonnay Australia 2020</a></li>
                    </ul>
                </div>
<!--                 <div class="footer-link-box">
                    <p>Mixed Wine</p>
                    <ul class="foo-box">
                        <li><a href="#"></a></li>
                    </ul>
                </div>
                <div class="footer-link-box">
                    <p>Sparkling Wines</p>
                    <ul class="foo-box">
                        <li><a href="#"></a></li>
                    </ul>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="footer-mid">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <p><strong>Disclaimer: </strong><span class="spacer"> ABN No. 56 652 958 060, &nbsp; Liquor Licence No. 36166448 & 33778262.   <br>
                 It is against the law to sell or supply alcohol to, or to obtain alcohol on behalf of a person under the age of 18 years.</span></p>
                <div class="footer-space">
            		<p><strong>WARNING: </strong> 
                        <div class="spacer">Under the Liquor Control Reform Act 1998 it is an offence  <br>
            			<ul style="margin-left:17px;">
            				<li>To supply alcohol to a person under the age of 18 years (Penalty exceeds $17,000)</li>
            				<li>For a person under the age of 18 years to purchase or receive liquor (Penalty exceeds $700)</li>
            			</ul>
                        </div></p>
                </div>
            </div>
            <div class="countryy">
                <div class="head-count">Allfinewines Australia 2021</div>
            </div>
        </div>
    </div>
</div>
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <ul class="footer-list">
                    <li><a href="/why-us">Why Buy From Us</a></li>
                    <li><a href="/why-sell">Why Sell Through Us</a></li>
                    <li><a href="/terms-conditions">Terms & Conditions</a></li>
                    <li><a href="/privacy-policy">Privacy Policy</a></li>
                    <!-- <li><a href="#">Sitemap</a></li> -->
                </ul>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <p class="copy">&copy; 2021 All Rights Reserved by GAP VENTURES PTY LTD <a href="/">(allfinewines.com.au)</a></p>
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--copyright-->
</div>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/61973ea36bb0760a49435268/1fkrcpc86';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->