<!DOCTYPE html>
<html lang="en">

<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>

<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="header.jsp" />     
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                       <!--  <p class="head">About Us</p> -->
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li>About Us</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="about-banner">
        <img src="/resources/images/about-banner1.jpg" alt="banner">
        <div class="about-content"><h2>About Us</h2></div>
        </div>
        <div class="main_container">
            <div class="container">
                <div class="row about-page">
                    <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6 hidden-xs about-img-lft">
                    <picture>
                        <img src="/resources/images/about-left.jpg" class="about_img" />
                        </picture>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                        <h1>Who We are</h1>
                        <p class="about_text">Allfinewines (GAP VENTURES PTY LTD) have, since its launch, provided a navigable platform for the wineries in Australia. We have always fulfilled our motto of supporting the Australian wineries from the time when we first ventured into the e-commerce sector. Since our launch, we have grown at a significant rate in the past few years. The feat of becoming a part of the largest online wine retailers in Australia could be achieved only through the dedication and hard work displayed by our entire team and we are still scaling new heights with the years passing by.</p>
                        <p class="about_text">We possess one of the largest wine collections in Australia. We have access to the remotest wine regions operating in Australia from where we resource our stock accommodations. We supply the wines to our customers without having any physical presence in the country. Wine is our speciality which we gather from across Australia through the most trusted and reliable suppliers operating particularly in the wine segment.</p>
                        <p class="about_text">Allfinewines (GAP VENTURES PTY LTD) provide you with a wide range of benefits with attractive offers and discounts on several wines. In case, you are not satisfied with our services, we provide you 100% money back without causing you any trouble. We have the privilege of dealing in all the popular brands available in Australia with the assistance of 1000 onboard wineries and over 6000 wines in our collection.</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6 hidden-sm hidden-md hidden-lg">
                        <img src="/resources/images/about_img.jpg" class="about_img" />
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--main-container-->
        <div class="why_us_about">
            <div class="container">
            <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 why-sec">
    			<h2>Why Choose Us?</h2>
    			<!-- <p class="sub_head">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p> -->
			</div>
            </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="why_us_area">
                            <p><i class="fa fa-lightbulb-o"></i></p>
                            <p class="head">Inspiration</p>
                            <p class="text">Since its launch, Allfinewines has witnessed several phases of the online wine industry. Today, we stand among the largest online wine retailers in Australia. </p>
                        </div><!--why_us_area-->
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="why_us_area">
                            <p><i class="glyphicon glyphicon-headphones"></i></p>
                            <p class="head">Support</p>
                            <p class="text">We possess a clear vision of assisting the wineries in Australia in letting their stock out across the country. We provide them with a single online platform to gain the attention of the customers for maximum sales. </p>
                        </div><!--why_us_area-->
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="why_us_area">
                            <p><i class="fa fa-undo"></i></p>
                            <p class="head">Procurement</p>
                            <p class="text">Our access reaches to the remotest parts producing wines in Australia. Our passion combined with spectacular products gives us a so-desired edge in the competition. </p>
                        </div><!--why_us_area-->
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--why_us-->
<!--         <div class="our_team">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h2 class="text-center">Our Team Members</h2>
                        <p class="sub_head">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="product_area">
                            <div class="image_area">
                                <img src="/resources/images/team_img1.jpg" />
                                <p>
                                    <a href="javascript:;" class="fa fa-facebook"></a>
                                    <a href="compare.html" class="fa fa-twitter"></a>
                                    <a href="javascript:;" class="fa fa-pinterest"></a>
                                    <a href="javascript:;" class="fa fa-youtube"></a>
                                    <a href="javascript:;" class="fa fa-instagram"></a>
                                </p>
                            </div>
                            <div class="head_cost_star_area">
                                <p class="main_head">John Muniz</p>
                                <p class="post">Project Engineer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="product_area">
                            <div class="image_area">
                                <img src="/resources/images/team_img2.jpg" />
                                <p>
                                    <a href="javascript:;" class="fa fa-facebook"></a>
                                    <a href="compare.html" class="fa fa-twitter"></a>
                                    <a href="javascript:;" class="fa fa-pinterest"></a>
                                    <a href="javascript:;" class="fa fa-youtube"></a>
                                    <a href="javascript:;" class="fa fa-instagram"></a>
                                </p>
                            </div>
                            <div class="head_cost_star_area">
                                <p class="main_head">Alea Brooks</p>
                                <p class="post">Graphics Designer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="product_area">
                            <div class="image_area">
                                <img src="/resources/images/team_img3.jpg" />
                                <p>
                                    <a href="javascript:;" class="fa fa-facebook"></a>
                                    <a href="compare.html" class="fa fa-twitter"></a>
                                    <a href="javascript:;" class="fa fa-pinterest"></a>
                                    <a href="javascript:;" class="fa fa-youtube"></a>
                                    <a href="javascript:;" class="fa fa-instagram"></a>
                                </p>
                            </div>
                            <div class="head_cost_star_area">
                                <p class="main_head">Anders Glick</p>
                                <p class="post">Software Developer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="product_area">
                            <div class="image_area">
                                <img src="/resources/images/team_img4.jpg" />
                                <p>
                                    <a href="javascript:;" class="fa fa-facebook"></a>
                                    <a href="compare.html" class="fa fa-twitter"></a>
                                    <a href="javascript:;" class="fa fa-pinterest"></a>
                                    <a href="javascript:;" class="fa fa-youtube"></a>
                                    <a href="javascript:;" class="fa fa-instagram"></a>
                                </p>
                            </div>
                            <div class="head_cost_star_area">
                                <p class="main_head">Richard Tice</p>
                                <p class="post">Web Developer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="exclusive">
	        <div class="container">
		        <div class="row">
			        <div class="col-md-6">
				        <div class="exclusive-inn">
					        <h2>Exclusives</h2>
					        <ul>
						        <li>Deals and discounts</li>
						        <li>100% money back</li>
						        <li>Only Australian wines</li>
						        <li>The facility of membership combined with additional benefits</li>
						        <li>Query resolution through customer service</li>
					        </ul>
				        </div>
			        </div>
			        <div class="col-md-6">
				        <div class="exclusive-inn">
					        <h2>Journey till now</h2>
					        <ul>
						        <li>Supporting 400+ wineries</li>
						        <li>Selection among 2000+ wines</li>
						        <li>All popular brands on one platform</li>
						        <!-- <li>200000 wine sales till now</li> -->
					        </ul>
				        </div>
			        </div>
		        </div>
	        </div>
        </div>
        <div class="testinomials about-test">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 testinomials_slider">
                    <div class="item">
                        <div class="testimonials_area">
                            <p class="text">Fantastic!!!   The quickest delivery I ever got . They delivered my dozen yesterday- exactly on the same date when they promised to do. Placing online order was also very easy and the package was delivered in good condition.</p>
                            <p class="img_name">Lissa Castro - Designer</p>
                        </div><!--testimonials_area-->
                    </div><!--item-->
                    <div class="item">
                        <div class="testimonials_area">
                            <p class="text">This is the best wine shopping portal I ever saw. I booked my wines within minutes. They have quite a huge collection and the prompt reply and smooth process of booking really surprised me.</p>
                            <p class="img_name">Alden Smith - Designer</p>
                        </div><!--testimonials_area-->
                    </div><!--item-->
                    <div class="item">
                        <div class="testimonials_area">
                            <p class="text">One of my friends referred this website. I am really glad he did. My choice of wine displayed ‘out of stock’ on other websites. But I found a lot of stock here.  Thanks for delivering it on time.</p>
                            <p class="img_name">Daisy Lana - Designer</p>
                        </div><!--testimonials_area-->
                    </div><!--item-->
                    <div class="item">
                        <div class="testimonials_area">
                            <p class="text">It has been more than 4 years since I have been purchasing wine through websites but never have I got my choice of wine on the date it displayed at the time of booking. I got very reliable service here.</p>
                            <p class="img_name">John Becker - Designer</p>
                        </div><!--testimonials_area-->
                    </div><!--item-->
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </div><!--testinomials-->        
<div class="why_us">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="why_us_area">
                    <p><i class="fa fa-truck"></i></p>
                    <p class="head">Free Delivery</p>
                </div><!--why_us_area-->
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="why_us_area border">
                    <p><i class="fa fa-undo"></i></p>
                    <p class="head">30 Day Return</p>
                </div><!--why_us_area-->
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="why_us_area">
                    <p><i class="glyphicon glyphicon-headphones"></i></p>
                    <p class="head">27/4 Support</p>
                </div><!--why_us_area-->
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--why_us-->        
<!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />
</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>

</html>