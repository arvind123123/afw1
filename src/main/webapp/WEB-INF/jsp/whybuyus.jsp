<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />   
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <!-- <p class="head">Vip Membership</p> -->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li>Why Buy From Us</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <!-- join now -->
<section>
    <div class="why-buy-banner">
        <img src="/resources/images/whybanner.jpg" alt="image">
        <div class="buy-content"><h2>Why Buy From Us</h2></div>
    </div>
    <div class="buy-one">
        <div class="container">
            <h2>Allfinewines assures you 100% customer happiness</h2>
            <h3>We offer more than <span>6000 </span>products with promised delivery of the wines ordered through our website.</h3>
        </div>
    </div>
    <div class="buy-two">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="buy-two-list">
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            Allfinewines is one of the largest wine websites with a variety of offers for the customers.</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            Allfinewines has become a platform for sale for hundreds of wineries who ship directly from their cellar doors.</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            We provide 100% moneyback to ensure customer satisfaction. We provide 100% refund at such instances.</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            Subscription to our VIP, Trophy club or wine reserve membership is always open. This provides you exclusive benefits and offers.</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                           <p> To resolve any query or complaint you can contact our customer service at <a href="mailto:info@allfinewines.com.au" target="_blank">  info@allfinewines.com.au </a></p></li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <picture><img src="/resources/images/wine1.jpg" alt="image"></picture>
                </div>
            </div>
        </div>
    </div>
</section>



       <jsp:include page="footer.jsp" />

<script type="text/javascript">
    $(document).ready(function() {
  $('.accordion a').click(function(){
    $(this).toggleClass('active');
    $(this).next('.content').slideToggle(400);
   });
});
</script>

       </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>