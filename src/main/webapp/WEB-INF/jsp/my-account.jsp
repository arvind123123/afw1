<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />



</head>
<body id="scroll_top">

<div class="order_detail_popup" style="display: none;">
    
</div>
<div class="popup_out_fade"></div>

    <div class="main">
<jsp:include page="header.jsp" />       
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">My Account</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li>My Account</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->


<!--  My account tabbing section start -->

<section class="my_account_sec">   
 <div class="container">
    <div class="row">
        
            <div class="vertical-tab" role="tabpanel">
                <!-- Nav tabs -->
                <div class="col-md-3">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-th-large"></span> Dashboard</a></li>
					<li role="presentation"><a href="#Section5" aria-controls="messages" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Account Details</a></li>
                   
				   <li role="presentation"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-shopping-cart"></span> Orders</a></li>
				   <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-heart"></span> Wishlist</a></li>
                    <li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-map-marker"></span> My Address</a></li>
                    
                    <li role="presentation"><a href="/logout"><span class="glyphicon glyphicon-user"></span> Log Out</a></li>
                </ul>
            </div>
                <!-- Tab panes -->
                <div class="col-md-9">
                <div class="tab-content tabs">
                    <div role="tabpanel" class="tab-pane fade in active" id="Section1">
                        <h3>Dashboard</h3>
                        <p>From your account dashboard. you can easily check & view your recent orders, manage your shipping and billing addresses and edit your password and account details.</p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Section2">
                        <h3>Orders</h3>
                        <div class="table-responsive2">
                                    <table class="table table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Order</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Total</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<c:forEach items="${customerDetails.orderList}" var="order">
                                            <tr>
                                                <td>#${order.orderNum}</td>
                                                <td>${order.orderDate}</td>
                                                <td>${order.orderStatus}</td>
                                                <td>${order.price} for ${order.quantity} item</td>
                                                <td>${order.paymentStatus}</td>
                                                <%-- <td><a data-orderid="${order.orderNum}" class="btn btn-order order_detail_btn btn-sm">View</a></td> --%>
                                            </tr>
                                          </c:forEach>  
                                        </tbody>
                                    </table>
                                </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Section3">
                        
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card mb-3 mb-lg-0">
                                    <div class="card-header">
                                        <h3>Billing Address</h3>
                                    </div>
                                    <div class="card-body">
                                        <address>${loginresponse.billingAddress[0].address}<br>${loginresponse.billingAddress[0].address1}<br>${loginresponse.billingAddress[0].city}<br>${loginresponse.billingAddress[0].state} <br> ${loginresponse.billingAddress[0].country} <br>${loginresponse.billingAddress[0].zipCode}</address>
                                        <p>${loginresponse.billingAddress[0].firstName} ${loginresponse.billingAddress[0].lastName}</p>
                                       <!-- <a href="#" class="btn btn-fill-out">Edit</a>  -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h3>Shipping Address</h3>
                                    </div>
                                    <div class="card-body">
                                         <address>${loginresponse.shippingAddress[0].address}<br>${loginresponse.shippingAddress[0].address1}<br>${loginresponse.shippingAddress[0].city}<br>${loginresponse.shippingAddress[0].state} <br> ${loginresponse.shippingAddress[0].country} <br>${loginresponse.shippingAddress[0].zipCode}</address>
                                        <p>${loginresponse.shippingAddress[0].firstName} ${loginresponse.shippingAddress[0].lastName}</p>
                                       <!-- <a href="#" class="btn btn-fill-out">Edit</a>  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Section4">
                        <h3>Wishlist</h3>
                        <div class="wishlist">
                        <ul class="main_wishlist_area_head">
                            <li>Product</li>
                            <li>Price</li>
                            <li>Stock Status</li>
                            <li>Remove</li>
                        </ul>
						<c:forEach items="${customerDetails.wishList}" var="product">
                        <ul class="main_wishlist_area">
                            <li>
                                <img src="${product.image}" class="product_img">
                            </li>
                            <li>
                                <a href="javascript:;">${product.name}</a>
                            </li>
                            <li>
                                <span class="cost"><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                            </li>
                            <li>
                                <span class="in_stock">${product.status}</span>
                            </li>
                            
							<c:if test="${product.status == 'In Stock'}">
                            <li>
                                <a class="c_btn1" href="/product/detail/${product.productId}">
                                    <span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                    <i></i>
                                </a>
                            </li>
							</c:if>
							<c:if test="${product.status != 'In Stock'}">
                            <li>
                                <a class="c_btn1" href="javascript:;">
                                    <span><i class="fa fa-shopping-cart"></i> Unavailable</span>
                                    <i></i>
                                </a>
                            </li>
							</c:if>
                           <li><a href="/product/removewishlist/${product.productId}" class="fa fa-close"></a></li>
                        </ul>
                        </c:forEach>
                    </div>
                    </div>
					
					<div role="tabpanel" class="tab-pane fade" id="Section5">
                       <!--  <h3>Account details</h3> -->
                        <div class="card">
                            <div class="card-header">
                                <h3>Account Details</h3>
                            </div>
                            <div class="card-body">
                               <!-- <p>Already have an account? <a href="#">Log in instead!</a></p>  -->
                                <form method="post" action="" id="userForm" name="userForm">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Name <span class="required">*</span></label>
                                            <input type="text" class="form-control" name="name" id="name"  value="${loginresponse.name}" />
                                         </div>
                                                                                 
                                        <div class="form-group col-md-6">
                                            <label>Email Address <span class="required">*</span></label>
                                            <input type="text" class="form-control" id="email" name="email" value="${loginresponse.emailId}" />
                                        </div>
                                       
                                        <div class="form-group col-md-6">
                                            <label>New Password <span class="required">*</span></label>
                                            <input type="password" class="form-control" id="password" name="password" />
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Confirm Password <span class="required">*</span></label>
                                            <input class="form-control" id="confirmPassword" name="confirmPassword" type="password">
                                        </div>
										<input type="hidden" name="siteId" id="siteId" value="1"/>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-fill-out" name="submit" value="Submit">Save</button>
                                        </div>
										<span id="updateSucess"></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</section>

<!--  My account tabbing section end -->

<jsp:include page="footer.jsp" />
</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
<script src="/resources/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
      $(".order_detail_btn").click(function(){
        $(".order_detail_popup").show();
        $(".popup_out_fade").addClass("popup_overly");
      });
      $(".order_close").click(function(){
        $(".order_detail_popup").hide();
        $(".popup_out_fade").removeClass("popup_overly");
      });

    });
	
	$("#userForm").validate({
		 rules:{
			 name : {
				 required : true
			 },
			 email : {
				 required : true,
				 email: true
			 },
			 password: {
			  required: true
			 },
			 confirmPassword: {
			  equalTo : "#password"
			 }
		 },
		  errorPlacement: function(error, element) {   },
		   submitHandler: function(form) {
			 var data = {
									name: $("#name").val(),
									email: $("#email").val(),
									password: $("#password").val(),
									siteId:$("#siteId").val()
									
								}
							$.ajax({
								url: '/updateregisteruser',
								type: 'post',
								contentType: 'application/json; charset=utf-8',
								data: JSON.stringify(data),
								dataType: "text",
								success: function(response) {
									console.log(response);
									$("#updateSucess").html(response);
									
								}
							});
				
			 
		  }
	 })
	 
	 $(".order_detail_btn").click(function(){
		var orderid = $(this).data("orderid");
		
		$.ajax({
                       type: "GET",
                       url: "/vieworderdetail/"+orderid,
                        dataType: "html",
                          success: function(data) 
                          { 
                          $(".order_detail_popup").html(data);
						    $('.order_detail_popup').show();   
							$('.popup_out_fade').addClass('popup_overly'); 
							
							 $(".order_close").click(function(){
								$(".order_detail_popup").hide();
								$(".popup_out_fade").removeClass("popup_overly");
							  }); 
                            
                          }

                         });
	})	
</script>
</body>
</html>