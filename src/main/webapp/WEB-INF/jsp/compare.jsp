<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    </head>
<body id="scroll_top">
    <div class="main"> 
<jsp:include page="/header" />     
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Compare</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li>Compare</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="compare_area">
            <div class="container">
                <div class="row">
                  <!--  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h1>Product Compare</h1>
                    </div> -->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="product_compare_box">
            <ul>
                <li class="product_compare_head_list">
                    <div class="product_img prdct_compare_had_title"><img src="/resources/images/compare_product_image.jpg"></div>
                    <div class="prdct_compare_had_title">PRODUCT NAME</div>
                    <div class="prdct_compare_had_title">PRICE</div>
                    <div class="prdct_compare_had_title">RATING</div>    
                    <div class="prdct_compare_had_title p_h_addto">ADD TO CART</div>    
                    <div class="prdct_compare_had_title p_h_desc">DESCRIPTION</div>    
                    <div class="prdct_compare_had_title p_h_color">COLOR</div>    
                    <div class="prdct_compare_had_title">SIZES AVAILABLE</div>    
                    <div class="prdct_compare_had_title">ITEM AVAILABILITY</div>    
                    <div class="prdct_compare_had_title">BRAND</div>    
                       
                    <div class="prdct_compare_had_title">...</div>    
                </li>
				<c:forEach items="${compareList}" var="product">
                <li class="product_compare_detail_box">
                    <div class="product_detail_list"><img src="https://drive.google.com/uc?export=view&id=${product.images[0]}"></div>
                    <div class="product_detail_list">
                        <a href="">${product.name}</a>
                    </div>
                    <div class="product_detail_list p_price"><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</div>
					<c:set var="reviewCount" value="${fn:length(product.reviews)}" />
                    <div class="product_detail_list p_rating">
									<c:forEach  begin="1" end="${product.rating}">
                                        <i class="fa fa-star"></i>
                                      </c:forEach>  
									  <c:forEach  begin="${product.rating+1}" end="5">
                                        <i class="fa fa-star-o"></i>
										</c:forEach> 
                        <span>(${reviewCount})</span>
                    </div>
                    <div class="product_detail_list p_add_to_btn">
					<c:if test="${product.quantity > 0}">
					<button class="c_btn1 btn_me" type="button" onclick="gotocart('/product/detail/${product.productId}')" >
                            <span class="addtocart"><i class="fa fa-shopping-cart"></i> <small>Add to cart</small></span>
                        </button>
					</c:if>
					<c:if test="${product.quantity == 0}">
					<button class="c_btn1 btn_me" type="button" disabled="disabled">
                            <span class="addtocart"><i class="fa fa-shopping-cart"></i> <small>Out of Stock</small></span>
                        </button>
					</c:if>
                        
                    </div>
                    <div class="product_detail_list p_detail">
                        <div class="p_detail_cesrip">
                            <p>${product.shortDescription}</p>
                        </div>
                    </div>
                    <div class="product_detail_list">
                        <p class="p_radio_buttons">
                            <label>
                                <input type="radio" name="radio">
                                <i class="back-brown"></i>
                            </label>
                            <label>
                                <input type="radio" name="radio">
                                <i class="back-grey"></i>
                            </label>
                            <label>
                                <input type="radio" name="radio">
                                <i class="back-red"></i>
                            </label>
                        </p>
                    </div>
                    <div class="product_detail_list p_size">
					<c:forEach items="${product.features}" var="feature">
					<c:if test="${feature.name == 'Size'}">
                        <span>
						<c:forEach items="${feature.values}" var="value" varStatus="loop">
						${value} <c:if test="${!loop.last}">,</c:if>
						</c:forEach>	
						</span>
						</c:if>
					</c:forEach>	
                    </div>
                    <div class="product_detail_list p_stock p_outstock">
					<c:if test="${product.quantity > 0}">
					In Stock
					</c:if>
					<c:if test="${product.quantity == 0}">
					Out of Stock
					</c:if>
                        
                    </div>
                    <div class="product_detail_list p_empty">${product.brandName}</div>
                    
                    <div class="product_detail_list p_remove">
                        <a href="/product/compare/remove/${product.productId}">Remove <i class="fa fa-close"></i></a>
                    </div>
                </li>
				</c:forEach>
                
            </ul>
        </div>




                </div><!--row-->
            </div><!--container-->
        </div><!--compare_area-->
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
        <jsp:include page="footer.jsp" />
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
<script>
function gotocart(detail){
	window.location.href= detail;
}
</script>
</body>
</html>