<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
<script src="/resources/zoom-slider/jquery-1.12.4.min.js"></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />     
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Register</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li>Register</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="register_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <form:form class="form_contact" action="signup" modelAttribute="userForm">
                            <h1>Create an Account</h1>
                            <form:input placeholder="Enter Your Name" class="form-control textOnly" path="name" />
                            <form:input placeholder="Enter Your Email" class="form-control" path="email" />
                            <form:password placeholder="Password" class="form-control" path="password" />
							<form:hidden path="siteId" value="1"/>
                            <input type="password" placeholder="Confirm Password" class="form-control" id="confirmPassword" name="confirmPassword" />
                            <p class="c_checkbox">
                                <label>
                                    <input type="checkbox" name="agree" />
                                    <i></i>
                                    I agree to Terms & Policy.
                                </label>
                            </p>
                            <a class="c_btn1" href="javascript:;">
                                <span><input class="sing_btn_bg" type="submit" value="Register"></span>
                                <i></i>
                            </a>
                            <div class="or_line"><span>OR</span></div><!--or_line-->
                            <p class="social_media">
                                <a href="javascript:;" target="_blank" class="facebook"><i class="fa fa-facebook"></i> Facebook</a>
                                <a href="javascript:;" target="_blank" class="pinterest"><i class="fa fa-pinterest"></i> Pinterest</a>
                            </p>
                            <p class="already_account">Already have an account? <a href="login">Log in</a></p>
                            <div class="clearfix"></div><!--clearfix-->
							<p>${success}</p>
                        </form:form>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--register_area-->
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />        
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>

<script src="/resources/js/js.js"></script>
<script src="/resources/js/jquery.min.js"></script>
<script src="/resources/js/jquery.validate.min.js"></script>
</body>

<script>
$(document).ready(function(){
$('.textOnly').keyup(function () {
        		  if (this.value != this.value.replace(/[^a-zA-Z ]+/g, '')) {
        		       this.value = this.value.replace(/[^a-zA-Z ]+/g, '');
        		    }
        	});

	$('.numbersOnly').keyup(function () {
		if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
		   this.value = this.value.replace(/[^0-9\.]/g, '');
		}
	});

	$("#userForm").validate({
		 rules:{
			 name : {
				 required : true
			 },
			 email : {
				 required : true,
				 email: true
			 },
			 password: {
			  required: true
			 },
			 confirmPassword: {
			  equalTo : "#password"
			 },
			 agree:{
				 required : true
			 }
		 },
		  errorPlacement: function(error, element) {   },
		   submitHandler: function(form) {
			 
				form.submit();
			 
		  }
	 })
	 
})	 
</script>

</html>