<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />   
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <!-- <p class="head">Vip Membership</p> -->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li>Refer & Earn</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <!-- join now -->
<section>
    <div class="refer-banner">
        <img src="/resources/images/refer-banner1.jpg" alt="image">
        <div class="buy-content"><h2>Refer & Earn</h2></div>
    </div>
    <div class="ref-two">
        <div class="container">
            <h2>Refer & Get 20% off</h2>
            <h4>Earn a $25 off voucher for your friends too</h4>
            <div class="row">
                <div class="col-md-6">
                    <picture><img src="/resources/images/refer-site.jpg" alt="image"></picture>
                </div>
                <div class="col-md-6">
                    <ul class="ref-list">
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                             it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum </li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                           There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="ref-three">
        <div class="container">
            <h2>Why refer Allfine Wines to your friends?</h2>
            <div class="row">
                <div class="col-md-3">
                    <div class="ref-three-inn">
                        <i class="fas fa-dollar-sign"></i>
                        <h2>Australia's 2nd Largest <br> Wine Website</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="ref-three-inn">
                       <i class="fas fa-truck"></i>
                        <h2>6000+ <br>Wines</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="ref-three-inn">
                      <i class="fas fa-dollar-sign"></i>
                        <h2>100% Moneyback <br> Guarantee</h2>
                        <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="ref-three-inn">
                      <i class="fas fa-exchange-alt"></i>
                        <h2>Free 90 <br> Day Returns</h2>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ref-four">
        <div class="container">
            <div class="row">
                <div class="four-sec">
                    <h2>How to Get Started</h2>
                    <ul class="ref-four-inn">
                        <li>Click 'Refer Now'</li>
                        <li>Lorem ipsum dolor sit amet,</li>
                        <li>Sed ut perspiciatis unde omnis iste natus error sit</li>
                        <li>But I must explain to you how all this mistaken idea of denouncing pleasure </li>
                        <li>At vero eos et accusamus et iusto odio dignissimos</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>



       <jsp:include page="footer.jsp" />

<script type="text/javascript">
    $(document).ready(function() {
  $('.accordion a').click(function(){
    $(this).toggleClass('active');
    $(this).next('.content').slideToggle(400);
   });
});
</script>

       </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>