<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
<style>
    section.imag_resp {
    margin-bottom: 20px;
}
</style>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />   
		<div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <!-- <p class="head">Vip Membership</p> -->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li>Platinum Membership</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <!-- join now -->
        <section class="imag_resp">
            <div class="top_banner_vip">
                <img src="/resources/images/membership-banner.jpg" alt="vip-banner-new">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-12">
<!--                         <div class="btn_upper">
                            <a href="javascript:;"><button class="btn btn-success" value="Join Now">Join Now</button></a>
                        </div> -->
                    </div>
                </div>
            </div>
        </section>
        <!-- end of join now -->

        <!-- Benefits Of Becoming A VIP Member -->
        <section class="befi">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-12">
                        <div class="benefits_heading">
                            <h1>Benefits of becoming a Platinum Member:</h1>
                            <p>Enjoy these jaw-dropping advantages for 12 months</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end of Benefits Of Becoming A VIP Member -->

        <!-- FREE BOTTLE OF WINE -->

        <section class="sell-sec-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 sl-box">
                        <h1><span>1. Free Bottle of Wine</span></h1>
                        <ul style="margin-bottom:15px;">
                            <li>A bottle of premium wine from a top winery is sent to you with every order**.</li>
                            <li>We send a different wine each month so that you get to try as many wines as possible.</li>
                        </ul>
                        <h1><span>2. Free Shipping</span></h1>
                        <ul>
                            <li>Browse through our huge catalogue of 1500+ wines &amp; enjoy free delivery on every order.</li>
                            <li>Recover your annual fee in just three orders by saving big on shipping &amp; the extra bottle of wine.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </section>

        <!-- end of FREE BOTTLE OF WINE -->

        <!-- SECRET DEALS -->

        <section class="sell-sec-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 sl-box2">
                        <h1><span>3. Secret Deals</span></h1>
                        <ul style="margin-bottom:15px;">
                            <li>From discount vouchers to jaw-dropping prices, get 4 secret deals in your inbox
                            every month.</li>
                            <li>Exclusive previews to upcoming events mean Platinum Members get to buy before
                            everyone else.</li>
                        </ul>
                        <h1><span>4. Personalised Service</span></h1>
                        <ul>
                            <li>A dedicated account manager to help you with queries, offer you special deals &amp; so on.
                            </li>
                            <li>Your order is delivered hassle-free as per the date &amp; time of your convenience.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </section>

        <!-- end of SECRET DEALS -->

        <!-- Become a Member Today -->
        <div class="vip-member">
            <span>Become a Member Today</span>    
            <a href="javascript:;" class="vip-addtocart">Join Now</a>            
        </div>
        <!-- end of Become a Member Today -->



        <!-- VIP membership -->
        <div class="WhyChooseUs-wrap1">
            <div class="container">
                <div class="col-md-6 vipjoins-1"><a href="#"><img class="WhyChooseUs-icon" alt="Become the vip membership" src="/resources/images/platinum-membership.jpg"></a>


                <div class="vip-member2ns">
                    <a href="javascript:;" class="vip-addtocart">Join Now</a>         
                </div>

            </div>
            <div class="small-box-vip">      
                <div class="started-vip">        
                    <h1> How to Get Started </h1>
                    <ul>                   
                        <li>
                            <i class="fa fa-check" aria-hidden="true"></i>
                             <h2 class="circle1-vip">Click on "Join Now"</h2>
                        </li>

                        <li>
                           <i class="fa fa-check" aria-hidden="true"></i>
                            <h2 class="circle2-vip">Purchase the membership subscription</h2>
                        </li>

                        <li>
                        <i class="fa fa-check" aria-hidden="true"></i>
                            <h2 class="circle3-vip">Make your first order as our platinum member</h2>
                        </li>

                        <li>
                        <i class="fa fa-check" aria-hidden="true"></i>
                            <h2 class="circle4-vip">Get your FREE Bottle &amp; offers for further purchases</h2>
                        </li>
                    </ul>  
                </div>
            </div>
        </div>
    </div>

<!-- end of VIP membership -->







       <jsp:include page="footer.jsp" />

<script type="text/javascript">
    $(document).ready(function() {
  $('.accordion a').click(function(){
    $(this).toggleClass('active');
    $(this).next('.content').slideToggle(400);
   });
});
</script>

	   </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>