<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="header">
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 left_area">
                    <div class="select_icon">
                        <i class="fa fa-flag left_top"></i>
                        <select class="flag">
                            <option selected>United States</option>
                            <option>France</option>
                            <option>English</option>
                        </select>
                    </div><!--select_icon-->
                    <div class="select_icon ">
                        <i class="fa fa-money left_top"></i>
                        <select class="flag">
                            <option selected>USD</option>
                            <option>EUR</option>
                            <option>GBR</option>
                        </select>
                    </div><!--select_icon-->
                    <a href="javascript:;" target="_blank">
                        <i class="glyphicon glyphicon-phone"></i>
                        <span>000-000-0000</span>
                    </a>
                </div><!--cols-->
				<input type="hidden" id="userId" value="${loginresponse.customerId}" />
				<input type="hidden" value="${siteId}" id="siteId" />
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right_area">
                    <a href="/product/compare"><i class="glyphicon glyphicon-transfer"></i> Compare</a>
                    <a href="/wishlist"><i class="fa fa-heart"></i> Wishlist</a>
                    <c:if test="${loginresponse != null}">
					<a href="/myorders"><i class="glyphicon glyphicon-user"></i> ${loginresponse.name}</a>
					</c:if>
					<c:if test="${loginresponse == null}">
					<a href="/login"><i class="glyphicon glyphicon-user"></i> Login</a>
					</c:if>
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </div><!--header_top-->
    <div class="header_bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="/" class="logo"><img src="/resources/images/logo.png" /></a>
                    <ul class="right_area">
                        <li>
                            <a href="javascript:;" class="search"><i class="fa fa-search"></i></a>
                        </li>
                        <li id="cartPopup">
                            
                        </li>
                        <li>
                            <!-- <a href="javascript:;" class="menu">
                                <div></div>
                                <div></div>
                                <div></div>
                            </a> -->
                            <button class="navbar-toggle custom_mobile_toggle_btn" type="button" data-toggle="collapse" data-target="#main-navbar">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="mb_tgle_icon">
                                  <div></div>
                                <div></div>
                                <div></div>
                              </span>
                           </button>
                        </li>
                    </ul><!--right_area-->


                    <nav class="custom_nav_">
         <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="nav navbar-nav">
               <li><a href="/">Home</a></li>
			   <c:forEach items="${categories}" var="category">
               <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">${category.name} <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>
                
                <ul class="dropdown-menu mega-dropdown-menu row">
                     <li class="col-sm-12"> <!-- col-sm-3 -->
                        <ul>
						 <c:forEach items="${category.subCatList}" var="subcategory">
                           <li class="dropdown-header">${subcategory.name}</li>
						    <c:forEach items="${subcategory.subSubCatList}" var="subsubcategory">
                            <li><a href="/searchproduct?item=${fn:toLowerCase(fn:replace(subsubcategory.name,' ','-'))}">${subsubcategory.name}</a></li>
                           </c:forEach>
						   </c:forEach>
                        </ul>
                     </li>

                     

                  </ul>

              </li>
              </c:forEach>
               <li><a href="/contactus">Contact Us</a></li>
            </ul>

         </div>
         <!-- /.nav-collapse -->
      </nav>


                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </div><!--header_bottom-->
</div><!--header--> 

<script>
$(document).ready(function(){
					$.ajax({
								url: '/product/getcartpopup',
								type: 'get',
								contentType: 'application/json; charset=utf-8',
								dataType: "html",
								success: function(response) {
									//console.log(response)
									$("#cartPopup").html(response);
								}
							});
							function removeProductCart(productId){
							$.ajax({
								url: '/product/removecart/'+productId,
								type: 'get',
								contentType: 'application/json; charset=utf-8',
								dataType: "html",
								success: function(response) {
									//console.log(response)
									$("#cartPopup").html(response);
								}
							});
							}
})
</script>