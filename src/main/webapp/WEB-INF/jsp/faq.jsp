<!DOCTYPE html>
<html lang="en">
<head>
  <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>

<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
<script src="/resources/js/parallax.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="header.jsp" />   
	   <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <!-- <p class="head">Faq</p> -->
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li>Faq</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="faq-banner">
          <img src="/resources/images/faq-5.jpg" alt="banner" />
          <div class="faq-banner-content"><h2>FAQ<small>s</small></h2></div>
        </div>
        <div class="main_container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 faqs">
                        <h1>FAQ<small>s</small></h1>
                        <div class="faq_area">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading active" role="tab" id="headingOne">
                                  <p class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">What is the surety of the wines to be in good taste?</a>
                                  </p>
                                </div><!--panel-heading-->
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    <p> We assure the quality of all the wines which we place on our portal. If you are not satisfied with the equality of wine for any reason, we replace it with an alternative or provide you with a refund as per your discretion. We have a separate corner for honest reviews of the customers on our website. These reviews help you in making transparent decisions based on our quality.</p>
                                  </div><!--panel-body-->
                                </div><!--panel-collapse-->
                              </div><!--panel-default-->
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                  <p class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                      How can you keep such a good price for wines?
                                    </a>
                                  </p>
                                </div><!--panel-heading-->
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                  <div class="panel-body">
                                    <p> We have arranged for a distribution system that bifurcates cost and supply chain. We do not operate in any rented building as we do not have any physical presence. We do not buy from the importers or wholesalers- we buy only when we get an extremely awesome deal. We eliminate the wholesale margin which helps us in marking a good price of the wines.</p>
                                  </div><!--panel-body-->
                                </div><!--panel-collapse-->
                              </div><!--panel-default-->
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <p class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      How much can we place a minimum order?
                                    </a>
                                  </p>
                                </div><!--panel-heading-->
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body">
                                    <p>You can buy either 6 bottles or 12 bottles in a single order .</p>
                                  </div><!--panel-body-->
                                </div><!--panel-collapse-->
                              </div><!--panel-default-->
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                  <p class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                      Do you supply all the wines without any break in supply? 
                                    </a>
                                  </p>
                                </div><!--panel-heading-->
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                  <div class="panel-body">
                                    <p>We make limited purchases in the form of spot buying to avoid purchases at full price as we do not want to sell the wine at a high cost on our website. Sometimes we run out of stock.</p>
                                  </div><!--panel-body-->
                                </div><!--panel-collapse-->
                              </div><!--panel-default-->
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFive">
                                  <p class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                      What are your delivery charges?
                                    </a>
                                  </p>
                                </div><!--panel-heading-->
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                  <div class="panel-body">
                                    <p>Our delivery charges vary from $9.99 to $15.90 depending on the distance of delivery</p>
                                  </div><!--panel-body-->
                                </div><!--panel-collapse-->
                              </div><!--panel-default-->
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingSix">
                                  <p class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                      What are the security measures that you take to avoid stealing wine?
                                    </a>
                                  </p>
                                </div><!--panel-heading-->
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                  <div class="panel-body">
                                    <p>If you are not at home at the time of delivery, we keep it at some safe place as directed by you. We can also deliver it to your workplace.</p>
                                  </div><!--panel-body-->
                                </div><!--panel-collapse-->
                            </div><!--panel-default-->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingSeven">
                                      <p class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                          What payment modes do you accept?
                                        </a>
                                      </p>
                                    </div><!--panel-heading-->
                                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                      <div class="panel-body">
                                    <p>Visa, the master card is accepted while making payment. You can also make direct credit into our account. If you are a part of the business, we can set up a credit account with you.</p>
                                  </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                                </div><!--panel-default-->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingEight">
                                      <p class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                          Are my details secure on your website?
                                        </a>
                                      </p>
                                    </div><!--panel-heading-->
                                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                      <div class="panel-body">
                                    <p>Yes, your details are completely secure with us. You can go through the privacy policy on our website.</p>
                                  </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                                </div><!--panel-default-->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingNine">
                                      <p class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                          Do I have to place my order on the website only?
                                        </a>
                                      </p>
                                    </div><!--panel-heading-->
                                    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                      <div class="panel-body">
                                    <p>No, you don't need to place your order on the website only. You can place your order on phone no.<a href="tel:+61357464631" target="_blank">+61 357464631</a>.or you can e-mail us on <a href="mailto:info@allfinewines.com.au" target="_blank">  info@allfinewines.com.au </a>.</p>
                                  </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                                </div><!--panel-default-->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTen">
                                      <p class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                          Where will I complain regarding your services?
                                        </a>
                                      </p>
                                    </div><!--panel-heading-->
                                    <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                      <div class="panel-body">
                                    <p>If you are unhappy with our services you can e-mail us on ---------.or call on ----------
                                        We shall resolve your queries on a priority basis.
                                    </p>
                                  </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                                </div><!--panel-default-->
                            </div><!--panel-group-->
                        </div><!--faq_area-->
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--main_container-->
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />        
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>

</html>