<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />   
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <!-- <p class="head">Vip Membership</p> -->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li>Privacy Policy</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <!-- join now -->
<section>
    <div class="privacy-banner">
        <img src="/resources/images/banner_privacy-policy.jpg" alt="banner">
        <div class="privacy-content"><h2>Privacy Policy</h2></div>
    </div>
    <div class="privacy-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>The privacy policy is to provide you with the assurance that the personally identifiable information (PII) is not at the risk of being misused.</p>
                    <p>PII denotes for the information used to identify, contact or locate an individual in any context. You should go through our privacy policy to know the manner of our operation- how we collect, how we use & protect or how we handle your PII.</p>
                </div>
                <div class="col-md-12">
                    <h2>What information do we collect-</h2>
                    <p>When you access our website to place an order for any wine of your preference we ask for your –</p>
                    <ul class="privacy-list">
                        <li>Name </li>
                        <li>e-mail</li>
                        <li>mailing address</li>
                        <li>phone number</li>
                        <li>other details (if required)</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>When do we collect your personal information?</h2>
                    <p>Your personal information is required when-</p>
                    <ul class="privacy-list">
                        <li>you make any purchase  </li>
                        <li>Subscribe to our newsletter  Some of your details may be used by cookies in the pretext of improving the customer service.</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>Use of your PII</h2>
                    <p>We collect your personal information when you register on our website or make any purchase or subscribe to any newsletter. After we collect the information we use it to-</p>
                    <ul class="privacy-list">
                        <li>Improve our website to serve you in a better way </li>
                        <li>Process your transactions in a short time </li>
                        <li>Provide you-mails with an update of other products and services</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>Cookies are used to-</h2>
                    <ul class="privacy-list">
                        <li>Compile data (site traffic & interactions) to improve site experience and tools. A third party may be hired to perform this task on our behalf.</li>
                        <li>Provide you easy access the next time when you log in our website</li>
                    </ul>
                    <p>You can set a reminder to sell an intimation when cookies are sent to you or you can deny such permissions. In case you deny access of cookies, some of the features of our website may not be fully operational which affects your search and the results that our website displays. Although, denying your permission to cookies does not affect the process of placing orders through our website.</p>
                </div>
                <div class="col-md-12">
                    <h2>Disclosure of IIIrd party</h2>
                    <p>We do not deal (sale or trade) your PII with any outside party without your prior permission. The term (third party) shall be exclusive of the people who assist in running our website help in successfully conducting our business or assist in providing you with the required service. These parties get access to your PII data till the time they maintain the decorum of keeping the data safe and secure according to our standards.</p>
                    <ul class="privacy-list">
                        <li>We disclose your PII to IIIrd parties-</li>
                        <li>During the sale or purchase of any business or asset</li>
                        <li>When an IIIrd party acquired all or partial assets of allfinewines.com.au</li>
                        <li>When we have to comply with the norms of law enforcement agencies</li>
                        <li>Data that does not come under the purview of PII may be provided to IIIrd party for marketing purposes </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>IIIrd party links-</h2>
                    <p>No links to IIIrd parties are included on the product and services available on our website. </p>
                </div>
                <div class="col-md-12">
                    <h2>Payment through credit card -</h2>
                    <p>SSL technology is used for the payment process. Through this technology, we store your payment details (by your prior permission) to make your payment details (by your prior permission) to make transactions on allfinewines.com.au easily. Your credit card details are not stored by us and neither does our staff have any access to your data. SSL tech is renowned around the world for its safety and privacy features of the data stored by it. </p>
                </div>
                <div class="col-md-12">
                    <h2>Make a change in stored data-</h2>
                    <ul class="term-list">
                        <li>You can make changes in stored data through-</li>
                        <li>Calling our customer service</li>
                        <li>Logging into your account</li>
                        <li>Intimating us by e-mail </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>Do not track signals</h2>
                    <p>allfinewines.com.au does not track any cookie if the DNT mechanism is in place.</p>
                </div>
                <div class="col-md-12">
                    <h2>Do we allow tracking of IIIrd party behavior</h2>
                    <p>Yes, we allow IIIrd party behavioral tracking.</p>
                </div>
                <div class="col-md-12">
                    <h2>Why do we need your e-mail address</h2>
                    <p>We need your e-mail address to</p>
                    <ul class="term-list">
                        <li>Provide you with any information, resolve your queries or requests.</li>
                        <li>Provide you updates according to your requirements</li>
                        <li>Provide you additional detail related to any product or service.</li>
                        <li>Send fresh mails after the original transaction</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <h2>Unsubscribe from our services</h2>
                    <p>We provide instructions for unsubscribing from our mails at the bottom of each mail. If you unsubscribe on the given links, you will be removed from the list of our marketing correspondence. After unsubscribing, you shall receive emails regarding order confirmation and delivery updates (if any) only.</p>
                </div>
                <div class="col-md-12">
                    <h2>Contact us</h2>
                </div>
            </div>

        </div>
    </div>
</section>



       <jsp:include page="footer.jsp" />

<script type="text/javascript">
    $(document).ready(function() {
  $('.accordion a').click(function(){
    $(this).toggleClass('active');
    $(this).next('.content').slideToggle(400);
   });
});
</script>

       </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>