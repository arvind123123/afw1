<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%@page contentType="text/html;charset=UTF-8" %>

<script src="/resources/zoom-slider/jquery-1.12.4.min.js"></script>

<form action="/cart/order/complete" method="POST"> <!-- // Replace this with your website's success callback URL -->
<script
    src="https://checkout.razorpay.com/v1/checkout.js"
    data-key="rzp_test_20Y6DRtftYvwHA" // Enter the Key ID generated from the Dashboard
    data-amount="${amt}" // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    data-currency="USD"
    data-order_id="${Razorpay.id}"//This is a sample Order ID. Pass the id obtained in the response of the previous step.
    data-buttontext="Pay with Razorpay"
    data-name="The Label Bar"
    data-description="${description}"
    data-image="http://test.thelabelbar.com/resources/images/logo.png"
    data-prefill.name="${billingName}"
    data-prefill.email="${email}"
    data-prefill.contact="${contact}"
    data-theme.color="#F37254"
></script>

 <input type="hidden" name="shopping_order_id" value="21">
 
</form>
<script>
    $(document).ready(function(){
         $(".razorpay-payment-button").hide(); 
        setTimeout(
  function() 
  {
    //alert(1);
    $(".razorpay-payment-button").trigger('click'); 
  }, 1000);
    
});

    function cancelform(){

  setTimeout('$("#payDivId").hide()',7000);
   setTimeout('$("#bookDivId").show()',7000);
     window.location.href= '/cart/checkout';
}  
   
</script>

 <div class="main">
         
    <div class="confor-mid" id="payDivId">  

Want to cancel your purchase please click below button.
<div>
<input type="button" onclick="cancelform();" value="Cancel Pruchase"/>
</div>  


<div id="bookDivId" style="display:none">Your Order has been canceled,<br/> Please Wait,
While we are redirecting you on home page.<br>
<img style="max-width:100%;" src="/resources/images/confirm-mid-progress.gif">
</div>
</div>

    </div>