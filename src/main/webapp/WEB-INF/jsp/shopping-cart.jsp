<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
<script src="/resources/zoom-slider/jquery-1.12.4.min.js"></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />  
<input type="hidden" value="${siteId}" id="siteId" />       
		<div class="breadcrumb_area breadcrumb_section_cart">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Shopping Cart</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li>Shopping Cart</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="shopping_cart">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="main_shopping_cart_area_head">
                            <li>Product</li>
                            <li>Price</li>
                            <li>Quantity</li>
                            <li>Total</li>
                            <li>Remove</li>
                        </ul>
						<form name="updateCart" action="/product/updateshoppingcart" method="post">
						<c:forEach var="cart" items="${cartList}" varStatus="loop">
						<c:set var="cartTotal" value="${cartTotal+(cart.price*cart.quantity)}" />
						<c:if test="${not  fn:contains(cartProductsIds,cart.productId)}">
						<c:set var="cartProductsIds" value="${cartProductsIds}${cart.productId}," />
						</c:if>
						<c:if test="${not  fn:contains(cartSubSubCatIds,cart.subSubCatId)}">
						<c:set var="cartSubSubCatIds" value="${cartSubSubCatIds}${cart.subSubCatId}," />
						</c:if>
						
                        <ul class="main_shopping_cart_area">
                            <li>
                                <img src="${cart.productImage}"  class="product_img" />
                            </li>
                            <li>
                                <a href="javascript:;">${cart.productName}</a>
                            </li>
                            <li>
                                <span class="cost"><i class="fa fa-${currency}" aria-hidden="true"></i>${cart.price}</span>
                            </li>
                            <li>
                                <div class="min_and_max">
                                    <input class="quantityProd" data-index="${loop.index}" data-price="${cart.price}" id="quantity${loop.index}" type="text" readonly="true" value="${cart.quantity}" name="quantity${loop.index}" data-bts-min="1" data-bts-max="${cart.maxQuantity}" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-success" data-bts-button-up-class="btn btn-success">
                                </div><!--min_and_max-->
                            </li>
                            <li><span id="totalPrice${loop.index}"><i class="fa fa-${currency}" aria-hidden="true"></i>${cart.price*cart.quantity}</span></li>
                            <li><a href="/product/viewshoppingcartremove/${cart.productId}" class="fa fa-close"></a></li>
                        </ul>
                        </c:forEach>
						<input type="hidden" value="${cartProductsIds}" id="productIds" />
						<input type="hidden" value="${cartSubSubCatIds}" id="subSubCatIds" />
						<input type="hidden" id="totalAmount" value="${cartTotal}" />
                        <ul class="coupon_and_update_cart">
                            <li>
                                <div class="apply_coupon">
                                    <input type="text" class="form-control"  placeholder="Enter Coupon Code..." id="couponCode" />
                                    <a class="c_btn1" href="javascript:;" onclick="checkcoupon();">
                                        <span>Apply Coupon</span>
                                        <i></i>
                                    </a>
									<p style="color:red" id="couponMsg"></p>
                                </div>
                            </li>
                            <li>
                              <!--  <a class="c_btn3" href="/product/updateshoppingcart">
                                    <span>Update Cart</span>
                                    <i></i>
                                </a> 
								<input type="submit" value="submit" /> -->
								<button class="c_btn1 btn_me" type="submit">
                                        <span class="addtocart"><i class="fa fa-shopping-cart"></i> Update Cart</span>
                                    </button>
                            </li>
                        </ul>
						</form>
                        <div class="shopping_icon_lines">
                            <i class="fa fa-shopping-cart"></i>
                        </div><!--shopping_icon_lines-->
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="calculate_shipping">
                            <p class="head">Calculate Shipping</p>
                            <form>
                                <select class="form-control">
                                    <option selected value=""><span>Choose a option...</span></option>
                                    <option value="AX">Aland Islands</option>
                                    <option value="AF">Afghanistan</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua and Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BY">Belarus</option>
                                    <option value="PW">Belau</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia</option>
                                    <option value="BQ">Bonaire, Saint Eustatius and Saba</option>
                                    <option value="BA">Bosnia and Herzegovina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BV">Bouvet Island</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="VG">British Virgin Islands</option>
                                    <option value="BN">Brunei</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>
                                    <option value="CA">Canada</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo (Brazzaville)</option>
                                    <option value="CD">Congo (Kinshasa)</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CW">CuraÇao</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FK">Falkland Islands</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GG">Guernsey</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea-Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HM">Heard Island and McDonald Islands</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IM">Isle of Man</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="CI">Ivory Coast</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JE">Jersey</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Laos</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macao S.A.R., China</option>
                                    <option value="MK">Macedonia</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia</option>
                                    <option value="MD">Moldova</option>
                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>
                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NL">Netherlands</option>
                                    <option value="AN">Netherlands Antilles</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="KP">North Korea</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PS">Palestinian Territory</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn</option>
                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="QA">Qatar</option>
                                    <option value="IE">Republic of Ireland</option>
                                    <option value="RE">Reunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russia</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="ST">São Tomé and Príncipe</option>
                                    <option value="BL">Saint Barthélemy</option>
                                    <option value="SH">Saint Helena</option>
                                    <option value="KN">Saint Kitts and Nevis</option>
                                    <option value="LC">Saint Lucia</option>
                                    <option value="SX">Saint Martin (Dutch part)</option>
                                    <option value="MF">Saint Martin (French part)</option>
                                    <option value="PM">Saint Pierre and Miquelon</option>
                                    <option value="VC">Saint Vincent and the Grenadines</option>
                                    <option value="SM">San Marino</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="GS">South Georgia/Sandwich Islands</option>
                                    <option value="KR">South Korea</option>
                                    <option value="SS">South Sudan</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard and Jan Mayen</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syria</option>
                                    <option value="TW">Taiwan</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TL">Timor-Leste</option>
                                    <option value="TG">Togo</option>
                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad and Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks and Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom (UK)</option>
                                    <option value="US">USA (US)</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VA">Vatican</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Vietnam</option>
                                    <option value="WF">Wallis and Futuna</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="WS">Western Samoa</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                </select>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <input placeholder="State / Country" class="form-control" type="text" />
                                    </div><!--cols-->
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <input placeholder="PostCode / ZIP" class="form-control" type="text" />
                                    </div><!--cols-->
                                </div><!--row-->
                                <a class="c_btn2" href="javascript:;">
                                    <span>Update Totals</span>
                                    <i></i>
                                </a>
                            </form>
                        </div><!--calculate_shipping-->
                    </div><!--cols-->
					<form:form action="/cart/checkout" methood="post" modelAttribute="checkoutForm">
					<form:hidden path="couponApplied" value=""/>
					<form:hidden path="couponText" value=""/>
					<form:hidden path="couponDiscount" value=""/>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="cart_totals">
                            <p class="head">Cart Totals</p>
                            <ul>
                                <li>Cart Subtotal</li>
                                <li><i class="fa fa-${currency}" aria-hidden="true"></i>${cartTotal}</li>
                            </ul>
                            <ul>
                                <li>Shipping</li>
                                <li>Free Shipping</li>
                            </ul>
							
							<ul id="couponDiv" style="display:none;">
                                <li>Coupon Discount</li>
                                <li><i class="fa fa-${currency}" aria-hidden="true"></i><span id="couponAmount"></span></li>
                            </ul>
							
                            <ul>
                                <li>Total</li>
                                <li><strong><i class="fa fa-${currency}" aria-hidden="true"></i><span id="finalTotalAmount">${cartTotal}</span></strong></li>
                            </ul>
                         <!--   <a class="c_btn1" href="/cart/checkout">
                                <span>Proceed To CheckOut</span>
                                <i></i>
                            </a> -->
							
							<button class="c_btn1 btn_me" type="submit">
                                        <span class="addtocart">Proceed To CheckOut</span>
                                    </button>
									
                        </div><!--cart_totals-->
                    </div><!--cols-->
					</form:form>
                </div><!--row-->
            </div><!--container-->
        </div><!--shopping_cart-->
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />        
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
<script src="/resources/js/plus-minus.js">/* min and max number */</script>
<script>$("input[class='quantityProd']").TouchSpin();
$(".quantityProd").on('touchspin.on.startspin', function () {
	var quantity = $(this).val();
	var price = $(this).data("price");
	var index = $(this).data("index");
	$("#totalPrice"+index).html("<i class='fa fa-${currency}' aria-hidden='true'></i>"+(quantity*price).toFixed(2));
	})
	
function checkcoupon(){
	var couponCode = $("#couponCode").val();
	if(couponCode == ''){
		$("#couponMsg").text("Please enter coupon code");
		return false;
	}
	var totalAmount = $("#totalAmount").val();
	alert(totalAmount)
	var siteId = $("#siteId").val();
	var customerId = $("#userId").val();
	var productIds = $("#productIds").val();
	var subSubCatIds = $("#subSubCatIds").val();
	
	var data = {
									couponCode : $("#couponCode").val(),
									siteId : $("#siteId").val(),
									customerId : $("#userId").val(),
									totalAmount : $("#totalAmount").val(),
									productId : $("#productIds").val(),
									subSubCategoryId : $("#subSubCatIds").val(),
								}
								
							$.ajax({
								url: '/product/checkcoupon',
								type: 'post',
								contentType: 'application/json; charset=utf-8',
								data: JSON.stringify(data),
								dataType: "text",
								success: function(response) {
									var couponResponse  = JSON.parse(response);
									console.log(couponResponse);
									$("#couponMsg").text(couponResponse.message);
									if(couponResponse.responseStatus.code == 1){
										$("#couponApplied").val("Yes");
										$("#couponDiv").show();
										$("#couponAmount").text(couponResponse.discount);
										$("#finalTotalAmount").text(totalAmount-couponResponse.discount);
										$("#couponText").val(couponCode);
										$("#couponDiscount").val(couponResponse.discount);
									}
								}
							});
}	
</script>
</body>

</html>