<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />   
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <!-- <p class="head">Vip Membership</p> -->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li> Why Sell Through Us</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <!-- join now -->
<section>
    <div class="why-buy-banner">
        <img src="/resources/images/alcohol-banner.jpg" alt="image">
        <div class="buy-content"><h2>Why Sell Through Us</h2></div>
    </div>
    <div class="buy-one">
        <div class="container">
            <h2>We support all the big and small wineries operating in Australia.</h2>
            <h3>Allfinewines (GAP VENTURES PTY LTD) provides a competitive platform to those who choose us to sell their collection through our website.</h3>
        </div>
    </div>
    <div class="buy-two">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="buy-two-list">
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            Life time validity for allfinewine membership.</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            The wineries get an exclusive page to showcase their products on our website.</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            We provide all marketing assistance to attract maximum buyers.</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            Provision of disbursement of payment with 14-21 working days.</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            We provide honest feedback to help you improve your product.</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                            Arrange for special deals to attract the membership cardholders.</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <picture><img src="/resources/images/sell-image.jpg" alt="image"></picture>
                </div>
            </div>
        </div>
    </div>
</section>



       <jsp:include page="footer.jsp" />

<script type="text/javascript">
    $(document).ready(function() {
  $('.accordion a').click(function(){
    $(this).toggleClass('active');
    $(this).next('.content').slideToggle(400);
   });
});
</script>

       </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>