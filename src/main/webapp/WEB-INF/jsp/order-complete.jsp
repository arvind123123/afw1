<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    </head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="header.jsp" />       
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Order Complete</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Order Complete</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="four_zero_four_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="main_404">
                            <i class="fa fa-check-circle"></i>
                            <p class="sub_heading1">#${orderNum} <br>Your order is completed!</p>
                            <p class="sub_heading1">${transID}<br>${status}</p>
                            <p class="text">Thank you for your order! Your order is being processed and will be completed within 3-6 hours. You will receive an email confirmation when your order is completed.</p>
                            <form>
                                <a class="c_btn1" href="/">
                                    <span>Continue Shopping</span>
                                    <i></i>
                                </a>
                            </form>
                            <div class="clearfix"></div>
                        </div><!--main_404-->
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--four_zero_four_area-->
        <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--subscribe-->
<jsp:include page="footer.jsp" />        
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>