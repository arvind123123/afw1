<!DOCTYPE html>
<html lang="en">

<head>
    <title>All Fine Wines</title>
    <meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="stylesheet" href="/resources/css/style.css" type="text/css" />
<link rel="shortcut icon" type="image/x-icon" href="/resources/images/favicon.png" />
<link rel="shortcut icon" type="image/x-icon" href="/resources/images/favicon.html" />
<!--
<filesmatch ".(html|php|css|js|ico|jpg|jpeg|png|gif|ttf|eot|woff|woff2|svg)$"=""></filesmatch> 
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
--></head>
<body id="scroll_top">
    <div class="header_search">
    <form>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="input_area">
                        <input type="text" placeholder="Search" />
                        <a href="javascript:;" class="fa fa-search"></a>
                        <a href="javascript:;" class="fa fa-close"></a>
                        <div class="clearfix"></div>
                    </div><!--input_area-->
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </form>
</div><!--header_search-->    <div class="main">
<jsp:include page="/header" />        
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Compare</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Compare</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="compare_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h1>Product Compare</h1>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="compare_area_box desktop_version">
                            <ul>
                                <li class="img"><img src="images/compare_product_image.jpg" /></li>
                                <li class="img"><img src="images/product_img1.jpg" /></li>
                                <li class="img"><img src="images/product_img2.jpg" /></li>
                                <li class="img"><img src="images/product_img3.jpg" /></li>
                            </ul>
                            <ul>
                                <li class="head">Product Name</li>
                                <li class="head"><a href="product-details.html">Blue Dress For Woman</a></li>
                                <li class="head"><a href="product-details.html">Lether Gray Tuxedo</a></li>
                                <li class="head"><a href="product-details.html">woman full sliv dress</a></li>
                            </ul>
                            <ul>
                                <li class="head">Price</li>
                                <li class="price">$45.00</li>
                                <li class="price">$55.00</li>
                                <li class="price">$68.00</li>
                            </ul>
                            <ul>
                                <li class="head">Rating</li>
                                <li class="star_rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <span>(21)</span>
                                </li>
                                <li class="star_rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <span>(15)</span>
                                </li>
                                <li class="star_rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <span>(25)</span>
                                </li>
                            </ul>
                            <ul>
                                <li class="head">Add To Cart</li>
                                <li>
                                    <a class="c_btn1" href="javascript:;">
                                        <span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                        <i></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="c_btn1" href="javascript:;">
                                        <span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                        <i></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="c_btn1" href="javascript:;">
                                        <span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                        <i></i>
                                    </a>
                                </li>
                            </ul>
                            <ul>
                                <li class="head">Description</li>
                                <li class="des_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</li>
                                <li class="des_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</li>
                                <li class="des_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</li>
                            </ul>
                            <ul>
                                <li class="head">Color</li>
                                <li class="color_area">
                                    <p class="radio_buttons">
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-brown"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-grey"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-red"></i>
                                        </label>
                                    </p>
                                </li>
                                <li>&nbsp;</li>
                                <li class="color_area">
                                    <p class="radio_buttons">
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-grey"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-brown"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-skyblue1"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-brown1"></i>
                                        </label>
                                    </p>
                                </li>
                            </ul>
                            <ul>
                                <li class="head">Sizes Available</li>
                                <li class="size">S, M, L, XL</li>
                                <li class="size">S, M, L, XL</li>
                                <li class="size">S, M, L, XL</li>
                            </ul>
                            <ul>
                                <li class="head">Item Availability</li>
                                <li class="in_stock">In Stock</li>
                                <li class="out_stock">Out Of Stock</li>
                                <li class="in_stock">In Stock</li>
                            </ul>
                            <ul>
                                <li class="head">Weight</li>
                                <li>&nbsp;</li>
                                <li>&nbsp;</li>
                                <li>&nbsp;</li>
                            </ul>
                            <ul>
                                <li class="head">Dimensions</li>
                                <li class="dimensions">N/A</li>
                                <li class="dimensions">N/A</li>
                                <li class="dimensions">N/A</li>
                            </ul>
                            <ul>
                                <li class="head">&nbsp;</li>
                                <li class="remove_btn"><a href="javascript:;">Remove <i class="fa fa-close"></i></a></li>
                                <li class="remove_btn"><a href="javascript:;">Remove <i class="fa fa-close"></i></a></li>
                                <li class="remove_btn"><a href="javascript:;">Remove <i class="fa fa-close"></i></a></li>
                            </ul>
                        </div><!--compare_area_box-->
                        
                        
                        
                        <div class="compare_area_box mobile_version">
                            <ul>
                                <li class="img"><img src="images/product_img1.jpg" /></li>
                                <li class="head prd_name"><a href="product-details.html">Blue Dress For Woman</a></li>
                                <li class="price price_head">$45.00</li>
                                <li class="star_rating rating_head">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <span>(21)</span>
                                </li>
                                <li class="add_to_cart">
                                    <a class="c_btn1" href="javascript:;">
                                        <span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                        <i></i>
                                    </a>
                                </li>
                                <li class="des_text des_head">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</li>
                                <li class="color_area color_head">
                                    <p class="radio_buttons">
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-brown"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-grey"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-red"></i>
                                        </label>
                                    </p>
                                </li>
                                <li class="size size_head">S, M, L, XL</li>
                                <li class="item_available in_stock">In Stock</li>
                                <li class="weight_head">&nbsp;</li>
                                <li class="dimensions head_dimensions">N/A</li>
                                <li class="remove_btn"><a href="javascript:;">Remove <i class="fa fa-close"></i></a></li>
                            </ul>
                            <ul>
                                <li class="img"><img src="images/product_img2.jpg" /></li>
                                <li class="head prd_name"><a href="product-details.html">Blue Dress For Woman</a></li>
                                <li class="price price_head">$45.00</li>
                                <li class="star_rating rating_head">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <span>(21)</span>
                                </li>
                                <li class="add_to_cart">
                                    <a class="c_btn1" href="javascript:;">
                                        <span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                        <i></i>
                                    </a>
                                </li>
                                <li class="des_text des_head">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</li>
                                <li class="color_area color_head">
                                    <p class="radio_buttons">
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-brown"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-grey"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-red"></i>
                                        </label>
                                    </p>
                                </li>
                                <li class="size size_head">S, M, L, XL</li>
                                <li class="item_available out_stock">Out Of Stock</li>
                                <li class="weight_head">&nbsp;</li>
                                <li class="dimensions head_dimensions">N/A</li>
                                <li class="remove_btn"><a href="javascript:;">Remove <i class="fa fa-close"></i></a></li>
                            </ul>
                            <ul>
                                <li class="img"><img src="images/product_img3.jpg" /></li>
                                <li class="head prd_name"><a href="product-details.html">Blue Dress For Woman</a></li>
                                <li class="price price_head">$45.00</li>
                                <li class="star_rating rating_head">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <span>(21)</span>
                                </li>
                                <li class="add_to_cart">
                                    <a class="c_btn1" href="javascript:;">
                                        <span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                        <i></i>
                                    </a>
                                </li>
                                <li class="des_text des_head">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</li>
                                <li class="color_area color_head">
                                    <p class="radio_buttons">
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-brown"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-grey"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="radio" />
                                            <i class="back-red"></i>
                                        </label>
                                    </p>
                                </li>
                                <li class="size size_head">S, M, L, XL</li>
                                <li class="item_available in_stock">In Stock</li>
                                <li class="weight_head">&nbsp;</li>
                                <li class="dimensions head_dimensions">N/A</li>
                                <li class="remove_btn"><a href="javascript:;">Remove <i class="fa fa-close"></i></a></li>
                            </ul>
                        </div><!--compare_area_box-->
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--compare_area-->
        <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--subscribe-->
        <jsp:include page="footer.jsp" />
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>