<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    

<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />   
		<div class="breadcrumb_area breadcrumb_section_conatct">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Contact Us</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Contact Us</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="why_us_about background_none">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="why_us_area contact_style3">
                            <div class="contact_icon">
                            <i class="fa fa-map-marker"></i>
                            </div>
                            <p class="head">Contact Information</p>
                            <p class="text address1">GAP VENTURES PTY LTD <br>DBA: ALL FINE WINES
                            </p>
                            <p class="text">Mernda Victoria Australia 3754
                            </p>
                        </div>
                    </div> 
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="why_us_area contact_style3">
                            <div class="contact_icon">
                                 <i class="fa fa-envelope"></i>
                            </div>
                            <p class="head">Email Address</p>
                            <p class="text"><a href="mailto:info@allfinewines.com.au" target="_blank">info@allfinewines.com.au</a></p>
                        </div><!--why_us_area-->
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="why_us_area contact_style3">
                            <div class="contact_icon">
                            <i class="fa fa-phone"></i></div>
                            <p class="head">Phone</p>
                            <p class="text"><a href="tel:+61357464631" target="_blank">+61 357464631</a></p>
                        </div><!--why_us_area-->
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--why_us-->
        <div class="get_in_touch">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="image-one">
                            <div class="contact_img_view">
                                <img src="/resources/images/contact-img.svg" alt="contact-img">
                            </div>
                        </div>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h1>Get In Touch !</h1>
                        <form class="form_contact">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <input placeholder="Enter Name *" class="form-control" type="text" />
                                </div><!--cols-->
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <input placeholder="Enter Email *" class="form-control" type="text" />
                                </div><!--cols-->
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <input placeholder="Enter Phone No. *" class="form-control" type="text" />
                                </div><!--cols-->
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <input placeholder="Enter Subject *" class="form-control" type="text" />
                                </div><!--cols-->
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <textarea placeholder="Message *" class="form-control" rows="4"></textarea>
                                </div><!--cols-->
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <a class="c_btn1" href="javascript:;">
                                        <span>Send Message</span>
                                        <i></i>
                                    </a>
                                </div><!--cols-->
                            </div><!--row-->
                        </form>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--get_in_touch-->
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
       <jsp:include page="footer.jsp" />
	   </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>