<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html lang="en" ng-app="productApp">

<!-- Mirrored from way2career.net/nomaan/ecommerce/products.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Jun 2020 11:00:50 GMT -->
<head ng-controller="productController" id="productController">
    <title>${metaTittle}</title>
    <meta name="description" content="${metaDescription}">
    <meta name="keywords" content="${metaKeywords}">
    <meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="stylesheet" href="/resources/css/style.css" type="text/css" />
<link rel="stylesheet" href="/resources/css/style_new.css" type="text/css" />
<link rel="shortcut icon" type="image/x-icon" href="/resources/images/favicon.png" />
<link rel="stylesheet" href="/resources/css/responsive.css" type="text/css">
<!--
<filesmatch ".(html|php|css|js|ico|jpg|jpeg|png|gif|ttf|eot|woff|woff2|svg)$"=""></filesmatch> 
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
-->    <link rel="stylesheet" href="/resources/css/range-slider-jquery-ui.css" type="text/css" />
<script src="/resources/zoom-slider/jquery-1.12.4.min.js"></script>
<script src="/resources/js/angular.min.js"></script>
<script src="/resources/js/ng-infinite-scroll.min.js"></script>
<script type="text/javascript" src="/resources/js/products.js"></script>
</head>
<body id="scroll_top" ng-controller="productController" id="productController">
<input type="hidden" ng-model="item" value="${products}" id="item">
    <div class="header_search">
    <form>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="input_area">
                        <input type="text" placeholder="Search" />
                        <a href="javascript:;" class="fa fa-search"></a>
                        <a href="javascript:;" class="fa fa-close"></a>
                        <div class="clearfix"></div>
                    </div><!--input_area-->
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </form>
</div><!--header_search-->    <div class="main">
<jsp:include page="/header" />        
        <div class="breadcrumb_area breadcrumb_section_conatct">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Products</p>
                    </div><!--cols-->
                    
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li id="breadCrumCat"></li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="products_page">
            <div class="right_sidebar_background" id="right_sidebar_background"></div><!--right_sidebar_background-->
            <a href="javascript:;" class="fa fa-close close_right_sidebar" id="close_right_sidebar"></a>
			<input type="hidden" id="currency" value="${currency}">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="right_sidebar" id="right_sidebar">
                            <p class="head main-cat">Categories</p>
                            <ul class="categories_area">
                                <li ng-repeat="category in categoryList">
                                    <a href="javascript:;" ng-click="selectedCategory(category.name)">
                                        <span class="categories_name">{{category.name}}</span>
                                        <span class="categories_num">({{category.count}})</span>
                                    </a>
                                </li>
                               
                            </ul>
                            <p class="head">Filter</p>
                            <div class="range_slider_area">
                                <div id="slider-range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                                    <div class="ui-slider-range ui-corner-all ui-widget-header"></div><!--ui-slider-range ui-corner-all ui-widget-header-->
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default ui-state-focus"></span>
                                </div><!--ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content-->
                                <p class="amount_area">
                                    <label for="amount">Price:</label>
                                   <!-- <input type="text" id="amount" readonly="readonly" value="$102 - $317"> -->
									<span id="amount"></span>
                                </p>
                            </div><!--range_slider_area-->
<!--                             <p class="head">Brand</p>
                            <div class="brands_area">
                                <p class="c_checkbox" ng-repeat="brand in brandList">
                                    <label>
                                        <input type="checkbox" name="" ng-model="selectedBrands[brand]">
                                        <i></i>
                                        {{brand}}
                                    </label>
                                </p>
                                
                            </div>
                            <p class="head">Size</p>
                            <p class="sizes_radio">
                                <label ng-repeat="size in sizeList" ng-if="size != ''">
                                    <input type="radio" name="size" value="{{size}}" ng-click="onclickSize(size)">
                                    <i>{{size}}</i>
                                </label>
                                
                            </p> -->
                           <!-- <p class="head">Color</p>
                            <p class="radio_buttons">
                                <label>
                                    <input type="radio" name="color">
                                    <i class="back-brown"></i>
                                </label>
                                <label>
                                    <input type="radio" name="color">
                                    <i class="back-grey"></i>
                                </label>
                                <label>
                                    <input type="radio" name="color">
                                    <i class="back-red"></i>
                                </label>
                                <label>
                                    <input type="radio" name="color">
                                    <i class="back-skyblue1"></i>
                                </label>
                                <label>
                                    <input type="radio" name="color">
                                    <i class="back-skyblue2"></i>
                                </label>
                                <label>
                                    <input type="radio" name="color">
                                    <i class="back-grey1"></i>
                                </label>
                                <label>
                                    <input type="radio" name="color">
                                    <i class="back-lightbrown"></i>
                                </label>
                                <label>
                                    <input type="radio" name="color">
                                    <i class="back-skyblue"></i>
                                </label>
                                <label>
                                    <input type="radio" name="color">
                                    <i class="back-grey"></i>
                                </label>
                            </p>  -->
                            <div class="ads_area">
                                <img src="/resources/images/cat-image.jpg" />
                                <div class="text_area">
                                    <p class="sub_head">Vintage Wine</p>
                                    <p class="sale"> Reser 1985</p>
                                    <a class="c_btn4" href="javascript:;">
                                        <span>Shop Now</span>
                                        <i></i>
                                    </a>
                                </div><!--text_area-->
                            </div><!--ads_area-->
                        </div><!--right_sidebar-->
                        <div class="right_main_area">
                            <div class="row select_boxes">
                                <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <select class="form-control form-control-sm">
                                        <option value="order">Default sorting</option>
                                        <option value="popularity">Sort by popularity</option>
                                        <option value="date">Sort by newness</option>
                                        <option value="price">Sort by price: low to high</option>
                                        <option value="price-desc">Sort by price: high to low</option>
                                    </select>
                                    <a href="javascript:;" class="filter_area" id="filter_area"><i class="fa fa-filter" aria-hidden="true"></i></a>
                                    <select class="form-control in_right">
                                        <option value="">Showing</option>
                                        <option value="9">9</option>
                                        <option value="12">12</option>
                                        <option value="18">18</option>
                                    </select> 
                                </div>-->
                            </div><!--row-->
                            
                            
                            <!-- Old Code Prince  -->
                            <div class="row" >
							<c:forEach items="${products}" var="product">
							
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="product_area">
                                        <div class="image_area">
                                        
                                        <img src="http://localhost:8090/resources/upload-dir/${product.imageUrl}" />
                                            
                                            <span class="sale">Sale</span>
                                            <p>
                                                <a href="/${catName}/${subCatMap.get(product.subCatId)}/${categoryMap.get(product.productId)}" class="fa fa-shopping-cart"></a>
                                               <%--  <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a> --%>
                                               <!-- <a href="javascript:;" class="fa fa-plus"></a> -->
                                                <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                                            </p>
                                        </div><!--image_area-->
                                        <div class="head_cost_star_area">
                                            <p class="head">
                                                <a href="/${catName}/${subCatMap.get(product.subCatId)}/${categoryMap.get(product.productId)}">${product.name}</a>
                                            </p>
                                            <p class="cost"> 
                                               <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                                <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span> 
                                                <!-- <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span> -->
                                              <!--  <span>35% Off</span> -->
                                            </p>
                                            <p class="stars">
											     <i class="fa fa-star" ng-repeat="x in [].constructor(4-product.rating) track by $index"></i>
                                                <i class="fa fa-star-o" ng-repeat="x in [].constructor(product.rating) track by $index"></i>
                                                
											   <%--  <span>(${product.ratingCount})</span> --%>

                                                <%
                                                    Random rand = new Random();
                                                    int n = rand.nextInt(90000) + 10000;
                                                    System.out.println(n);
                                                %>
                                               <span><%="("+ (int) (Math.random() * 1000) +")"%>  </span>


                                            </p>
                                         <!--   <p class="radio_buttons">
                                                <label>
                                                    <input type="radio" name="radio" checked />
                                                    <i class="back-brown"></i>
                                                </label>
                                                <label>
                                                    <input type="radio" name="radio" />
                                                    <i class="back-grey"></i>
                                                </label>
                                                <label>
                                                    <input type="radio" name="radio" />
                                                    <i class="back-red"></i>
                                                </label>
                                            </p> -->
                                        </div><!--head_cost_star_area-->
                                    </div><!--product_area-->
                                </div><!--cols-->
                             </c:forEach>
                            </div><!--row-->
                        	<!-- Old Code Prince -->
                        
                          <!--  <nav aria-label="Page navigation example" class="pagination_area">
                              <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-left"></i></a></li>
                                <li class="page-item"><a class="page-link" href="#">First</a></li>
                                <li class="page-item"><a class="page-link active" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Last</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-right"></i></a></li>
                              </ul>
                            </nav> -->
                        </div><!--right_main_area-->
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--products_page-->
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />        
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
<script src="/resources/js/range-slider-jquery-ui.js"></script>

<script type="text/javascript">
    $("#filter_area").click(function(){
        $("#right_sidebar, #right_sidebar_background, #close_right_sidebar").fadeIn();
    });
    $("#close_right_sidebar").click(function(){
        $("#right_sidebar, #right_sidebar_background, #close_right_sidebar").fadeOut();
    });
</script>
 <script type="text/javascript">
                    function titleCase(str) {
                    	   var splitStr = str.toLowerCase().split(' ');
                    	   for (var i = 0; i < splitStr.length; i++) {
                    	       // You do not need to check if i is larger than splitStr length, as your for does that for you
                    	       // Assign it back to the array
                    	       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
                    	   }
                    	   // Directly return the joined string
                    	   return splitStr.join(' '); 
                    	}
                    var title = titleCase('${breadCrumCatName}');
                    document.getElementById("breadCrumCat").innerHTML = title;
</script>
    
</body>

<!-- Mirrored from way2career.net/nomaan/ecommerce/products.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Jun 2020 11:00:58 GMT -->
</html>