<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->
<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />   
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <!-- <p class="head">Vip Membership</p> -->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="./">Home</a></li>
                            <li>Refer & Earn</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <!-- join now -->
<section>
    <div class="winery-banner">
        <img src="/resources/images/creater-banner.jpg" alt="image">
        <div class="brand-content"><h2>Refer & Earn</h2></div>
    </div>
    <div class="winery-one">
        <div class="container">
            <div class="row">
                <h2>Up to 20% off on referrals</h2>
                <h3>Get a voucher of $25 for your friend</h3>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <ul class="wineries-content">
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i> Refer All Fine Wines (GAP VENTURES PTY LTD) to your friends as well as your family to get amazing discounts</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i> A referral link will be generated as soon as you enter your details</li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i> To avail of this offer you have to share the link with 2 or more people</li>
                        <!-- <li><i class="fa fa-angle-right" aria-hidden="true"></i> The person who opens the link gets a $25 discount voucher valid till the first purchase.</li> -->
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i> If your friend purchases all fine wines, you get a voucher for a 20% discount by e-mail.  </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <picture><img src="/resources/images/wine-img.jpg" alt="image"></picture>
                </div>
            </div>
        </div>
    </div>
    
<div class="winery-two">
    <div class="container">
        <div class="row">
            <div class="col-md-3"><picture><img src="/resources/images/wine1.jpg" alt="image"></picture></div>
            <div class="col-md-9">
                <h3>One of the top Website of Australia</h3>
                <p>Get the best wine experience from one of the largest online stores in Australia.</p>
            </div>
        </div>
        <div class="row order">
            <div class="col-md-9">
                <h3> 1500+ Wines</h3>
                <p>We assure you of our quality. We have our collections from the top-rated Australian brands.</p>
            </div>
            <div class="col-md-3"><picture><img src="/resources/images/wine2.jpg" alt="image"></picture></div>
        </div>
        <div class="row">
            <div class="col-md-3"><picture><img src="/resources/images/wine3.jpg" alt="image"></picture></div>
            <div class="col-md-9">
                <h3>100% Moneyback Guarantee</h3>
                <p>If ever you are dissatisfied with our quality or taste, you get full replacement or refund*</p>
            </div>
        </div>
        <div class="row order">
            <div class="col-md-9">
                <h3>Free 90 Day Returns</h3>
                <p>We have a simple and easy return policy to ensure that you don't face any trouble**</p>
            </div>
            <div class="col-md-3"><picture><img src="/resources/images/wine4.jpg" alt="image"></picture></div>
        </div>
    </div>
</div>



</section>



       <jsp:include page="footer.jsp" />

<script type="text/javascript">
    $(document).ready(function() {
  $('.accordion a').click(function(){
    $(this).toggleClass('active');
    $(this).next('.content').slideToggle(400);
   });
});
</script>

       </div><!--main-->
<a href="javascript:void(0)scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>