<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    

<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
</head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="/header" />   
		<div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
<!--                         <p class="head">Delivery & Returns Policy</p> -->
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Delivery & Returns Policy</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="return-banner">
               <img src="/resources/images/return-bg.jfif" alt="image">
               <div class="return-content">
                   <h2>Delivery & Returns</h2>
               </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="policy-heading">
                        <!-- <h1>Delivery & Returns</h1> -->
                        <h2>How much will delivery cost me?</h2>
                        <p>Simply refer to the following chart for shipping fees, which is subject to your region. (This may differ in conjunctions with promotional offers.). We are currently not shipping to Northern Territory as we are working on complying with the updates in the Alcohol Legislation of Northern Terrority. Our courier delivery timings are listed below.</p>
                        <p><strong>Please note: </strong>In regional areas, sometimes it might take longer to deliver than mentioned, depending on the location.

                      </p>
                    </div>
                    <div class="policy-table table-responsive">
                        <table class="shippingtbl" style="width: 100%;height: 100%;border: #000000 solid 1px;text-align: center;" border="2">
                            <tbody>
                                <tr style="background: #e1e1e1; text-align: center;">
                                    <th><strong>Region</strong></th>
                                    <th><strong>Area</strong></th>
                                    <th><strong>Postcodes</strong></th>
                                    <th><strong>Shipping Rates</strong></th>
                                    <th><strong>Delivery Time</strong></th>
                                </tr>
                                <tr>
                                    <td rowspan="1"><strong>ACT</strong></td>
                                    <td>Metro Areas</td>
                                    <td>0200 , 0221 , 2600 - 2612 , 2614 - 2617 , 2900 - 2906 , 2911 - 2914</td>
                                    <td>$9.99</td>
                                    <td>1 to 2 business days</td>
                                </tr>
                                <tr>
                                    <td rowspan="2"><strong>NSW</strong></td>
                                    <td>Metro Areas</td>
                                    <td>1001 - 1010 , 1020 - 1023 , 1025 - 1046 , 1100 - 1101 , 1105 - 1110 , 1112 - 1199 , 1200 - 1240 , 1291 - 1299 , 1300 , 1335 , 1340 , 1355 , 1360 , 1401 , 1420 , 1422 - 1430 , 1435 , 1440 , 1441 , 1445 , 1450 , 1455 , 1460 , 1465 , 1466 , 1470 , 1475 , 1476 , 1480 , 1481 , 1485 , 1490 , 1495 , 1499 , 1515 , 1560 , 1565 , 1570 , 1885 , 1590 , 1595 , 1630 , 1635 , 1640 , 1655 , 1660 , 1670 , 1675 , 1680 , 1685 , 1700 , 1710 , 1712 , 1715 , 1730 , 1750 , 1755 , 1765 , 1790 , 1800 , 1805 , 1811 , 1825 , 1835 , 1851 , 1860 , 1871 , 1875 , 1885 , 1890 , 1891 , 2000 , 2001 , 2004 , 2006 , 2007 - 2012 , 2017 - 2052 , 2055 , 2057 - 2077 , 2079 - 2097 , 2099 , 2100 - 2148 , 2150 - 2168 , 2170 - 2179 , 2190 - 2199 , 2200 , 2203 - 2214 , 2216 - 2234 , 2250 - 2252 , 2256 - 2263 , 2282 - 2299 , 2300 - 2310 , 2500 - 2506 , 2515 - 2530 , 2555 - 2574 , 2601 - 2620 , 2740 - 2786 , 2890 , 2899</td>
                                    <td>$9.99</td>
                                    <td>1 to 2 business days</td>
                                </tr>
                                <tr>
                                    <td>Regional Areas</td>
                                    <td>2264 - 2281 , 2311 - 2499 , 2507 - 2514 , 2531 - 2554 , 2575 - 2599 , 2621 - 2739 , 2787 - 2889 , 2891 - 2898</td>
                                    <td>$13.90</td>
                                    <td>2 to 3 business days</td>
                                </tr>
                                <tr>
                                    <td rowspan="2"><strong>Victoria</strong></td>
                                    <td>Metro Areas</td>
                                    <td>3000 - 3006 , 3008 , 3010 - 3220 , 3335 - 3341 , 3425 - 3443 , 3750 - 3811 , 3910 - 3920 , 3926 - 3944 , 3972 - 3978 , 3980 - 3983 , 8001 - 8999</td>
                                    <td>$9.99</td>
                                    <td>2 to 3 business days</td>
                                </tr>
                                <tr>
                                    <td>Regional Areas</td>
                                    <td>3221 - 3334 , 3342-3424, 3350-3352, 3355, 3357-3424 , 3444 - 3749 , 3812 - 3909 , 3921-3925, 3945 - 3971 , 3979 , 3984 - 3999</td>
                                    <td>$13.90</td>
                                    <td>3 to 4 business days</td>
                                </tr>
                                <tr>
                                    <td rowspan="3"><strong>Queensland</strong></td>
                                    <td>Metro Areas</td>
                                    <td>4000 - 4229 , 4300 - 4307 , 4500 - 4549 , 4900 - 4999 , 9000 - 9299 , 9400 - 9596 ,9700 - 9799 .</td>
                                    <td>$9.99</td>
                                    <td>2 to 3 business days</td>
                                </tr>
                                <tr>
                                    <td>Regional Areas</td>
                                    <td>4230 - 4299 , 4308 - 4499 , 4550 - 4689 , 9597 - 9599 , 9880 - 9919 , 4482 , 4470 , 4690 - 4899 , 9920 - 9999 .</td>
                                    <td>$14.90</td>
                                    <td>3 to 4 business days</td>
                                </tr>
                                <tr>
                                    <td>Dry Areas (Non Delivery Areas)</td>
                                    <td style="text-align: justify;" colspan="3">
										All Fine Wines (GAP VENTURES PTY LTD) will not deliver wine under any circumstances to any of the following Dry Area Locations: <br>
										Aurukun Postcode - 4871, Bamaga Postcode - 4876, Cherbourg Postcode - 4605, Doomadgee Postcode - 4830, <br>
										Hope Vale Postcode - 4895, Injinoo Postcode - 4876,Kowanyama Postcode - 4871, Lockhart Postcode - 4871, <br>
										Mapoon Postcode - 4874,Mornington Island Postcode - 4871,Napranum Postcode - 4874, <br>
										New Mapoon Postcode - 4876, Palm Island Postcode - 4816,Pormpuraaw Postcode - 4871,Seisia Postcode - 4876, <br>
										Umagico Postcode - 4876, Woorabinda Postcode - 4713, Wujal Wujal Postcode - 4895, Yarrabah Postcode - 4871. 
                                    </td> 
                                </tr>
                                <tr>
                                    <td rowspan="2"><strong>SA</strong></td>
                                    <td>Metro Areas</td>
                                    <td>5000 - 5199 , 5800 - 5999</td>
                                    <td>$9.99</td>
                                    <td>4 business days</td>
                                </tr>
                                <tr>
                                    <td>Regional Areas</td>
                                    <td>5200 - 5749</td>
                                    <td>$14.90</td>
                                    <td>5 business days</td>
                                </tr>
                                <tr>
                                    <td rowspan="2"><strong>WA</strong></td>
                                    <td>Metro Areas</td>
                                    <td>6000 - 6206 , 6800 - 6999</td>
                                    <td>$10.99</td>
                                    <td>7 to 10 business days</td>
                                </tr>
                                <tr>
                                    <td>Regional Areas</td>
                                    <td>6207 - 6799 , 6700 - 6797</td>
                                    <td>$15.90</td>
                                    <td>7 to 10 business days</td>
                                </tr>
                                <tr>
                                    <td rowspan="1"><strong>Tasmania</strong></td>
                                    <td>Regional Areas</td>
                                    <td>7000 - 7999</td>
                                    <td>$15.90</td>
                                    <td>5 to 6 business days</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="policy-q">
                        <p><strong>Note:</strong> It might take 2-3 additional days to process DropShip & Virtual products.</p>
                        <ul class="policy-list">
                            <li>
                                <h2>What happens after I pay for my order?</h2>
                                <p>After ordering online, you will receive an email confirmation containing your order details (provided you have entered a valid email address). We will then attempt to deliver your purchase(s) from our warehouse in Sydney via our preferred carrier - Australia Post. Depending on how remote the delivery address is, the expected date of delivery may vary by 2 to 14 days. Please note that under circumstances like a heatwave or flood, it can take us longer to serve you.</p>
                            </li>
                            <li>
                                <h2>When will my order be delivered?</h2>
                                <p>Wine is normally delivered from Monday to Friday between 9.00 am and 5.00 pm. Most orders will be delivered to your door, however in rural or remote areas, the delivery will be left at the nearest Australia Post office or freight depot.</p>
                                <p>A signature is required on all deliveries. If the recipient is not available at the time of delivery, the courier will leave a note for later collection by you from the nearest local Post Office. The person collecting the wines from the Post Office must be 18 or over and able to present identification corresponding to the name of the consignee. If the order is not collected within 10 working days, it will be returned to All Fine Wines (GAP VENTURES PTY LTD) and 
                                    <strong>NO REFUND </strong>will be available.</p>
                            </li>
                            <li>
                                <h2>Who can accept my delivery?</h2>
                                <p>All Fine Wines will not deliver wines to anyone less than 18 years of age. If there is any doubt about the age of the recipient on delivery, our delivery drivers are obliged to request some form of photographic ID. If that recipient is unable to produce appropriate ID, then the delivery driver will be unable to leave the wine.</p>
                            </li>
                            <li>
                                <h2>What happens if I enter the wrong address?</h2>
                                <p>All Fine Wines cannot be held responsible for deliveries to incorrect or incomplete addresses provided by customers. If the address is incorrect and the delivery is made, the customer assumes responsibility for the entire purchase. If the address is incorrect and/or the delivery is refused or unable to be completed, then the order will be returned to our distribution centre and additional handling costs plus freight charges will be applied to a request for re-delivery. If the address is incorrect and the delivery is refused or unable to be completed and the customer chooses no re-delivery, then a handling charge and all applicable freight charges will be deducted from any refund made. To avoid these costs, we recommend that you double check your address details to ensure that all information provided is correct and in full.</p>
                            </li>
                            <li>
                                <h2>Can the order be left at my door if I am not home?</h2>
                                <p>By selecting our "Authority to leave" option, you authorise the courier to leave the goods unattended at the delivery address and you accept that once the courier has done so, the complete order has been received.</p>
                                <p>All Fine Wines, and contractors associated with the delivery, do not accept responsibility for any loss or damage which results from the Authority to Leave delivery method. You agree to release All Fine Wines from and against any and all claims, demands, liabilities, losses, costs and expenses, including financial and other consequential losses, made, suffered or incurred by you or any other person or entity as a result of this Authority to Leave.</p>
                                <p>Please note that under certain circumstances, All Fine Wines may not be able to leave your delivery as directed under an Authority to Leave request. In these instances, your complete order will be delivered to your nearest Australia Post Office for your collection.</p>
                            </li>
                            <li>
                                <h2>Do you deliver outside of Australia?</h2>
                                <p>All Fine Wines is an Australia-based company, licensed to sell wine to Australian residents. As such, we are only able to deliver wine to addresses located in Australia. Should you wish to have a purchase delivered to a location outside of Australia, you will need to make arrangements with your own shipping agent. All Fine Wines will not be responsible in any way for delivery outside Australia arranged by you.</p>
                            </li>
                            <li>
                                <h2>How does your 100% Moneyback Guarantee work?</h2>
                                <p>In the rare instance that you are not satisfied with a wine purchased from All Fine Wines, we will be more than happy to provide you a replacement or full refund of your purchase.</p>
                                <p>We highly recommend that you open one bottle of wine to begin with, try it and ask for a replacement or refund if dissatisfied. We'll refund or credit you (including the freight cost) an amount equal to the value of the unopened bottles you return plus one bottle. Please note that if more than one bottle of wine has been opened by you, All Fine Wines will refund the cost of one bottle in addition to the unopened ones that have been returned.</p>
                                <p>In the case of a mixed pack, we provide the replacement/return on a bottle by bottle basis and suggest that you continue to try the other wines as well rather than sending them back without tasting.</p>
                                <p>All refunds are processed within 7 days of your request or from the date that the wines are returned to us.</p>
                                <p>If All Fine Wines believes that a customer is abusing the 100% Moneyback Guarantee, All Fine Wines reserves the right to suspend or cancel any pending orders by them and/or close their account.</p>
                                <p>Any loss, damage or deterioration to the product after delivery is the responsibility of the customer and therefore no refunds will be offered for wines in such cases.</p>
                                <p>If you require further information or would like to submit a claim, simply contact our Customer Service Team at <a href="mailto:info@allfinewines.com.au">info@allfinewines.com.au</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


       <jsp:include page="footer.jsp" />
	   </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>