<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

    
<div class="header_search">
    <a href="javascript:;" class="fa fa-close"></a>
    <form>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="input_area">
                        <input type="text" placeholder="Search over 1,500 Aussie Wines" class="ui-autocomplete-input" id="searchbox" autocomplete="off">
                        <a href="javascript:;" class="fa fa-search"></a>
                        <div class="clearfix"></div>
                    </div><!--input_area-->
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </form>
</div><!--header_search-->
<div class="header-top">
    <div class="container">
        <marquee onmouseover="stop()" onmouseout="start()" class="marquee">Some Delay in delivery is being experienced in some parts of Australia. We thank you for your patience.</marquee>
    </div>
</div>
<div class="header_new">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="/" class="logo_area">
                    <img src="/resources/images/allfinewine-logo.svg" width="179" height="33" alt="responsive image" />
                </a>
                <!-- <div class="mobile-wrap"> -->
                <ul class="navigation_bar">
                    <a href="javascript:;" class="close_btn">Close <i class="fa fa-close"></i></a>
                     <c:forEach items="${categories}" var="category">
                      <li class="hug-dropdown"><a href="javascript:void(0)" class="menu-anchor">${category.name} <i class="fa fa-angle-down"></i></a>
                        <ul class="sub-menuu">
                           <!--  <img src="/resources/images/drop_down-1.jpg" class="dropdown_banner" /> -->
                            <div class="dropdown-banner"></div>
                            <div class="row">
                                <c:forEach items="${category.subCatList}" var="subcategory">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p class="head"><a href="/${categoriesMap.get(category.id)}/${subCategoriesMap.get(subcategory.id)}"> ${subcategory.name}</a></p>
                                </div><!--cols-->
                               </c:forEach>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p class="head allwine"><a href="/${categoriesMap.get(category.id)}">All Wines</a></p>
                                </div><!--cols-->
                            </div><!--row-->
                        </ul>
                    </li>
                </c:forEach>
                    <li><a href="/vip">Platinum</a></li>
                    <!-- <li><a href="/contactus">Contact Us</a></li> -->
                </ul>
                <a href="javascript:;" class="menu"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="user_shoping">
                    <li class="users">
                        <a href="javascript:;" class="fa fa-user-circle-o"></a>
                        <div class="dropdown">
                        	<input type="hidden" id="userId" value="${loginresponse.customerId}" />
							<input type="hidden" value="${siteId}" id="siteId" />
                            <!-- <a href="/login">My Account</a> -->
                            <c:if test="${loginresponse != null}">
							<a href="/myorders"><i class="fa fa-user-circle-o" aria-hidden="true"></i> ${loginresponse.name}</a>
							</c:if>
							<c:if test="${loginresponse == null}">
							<a href="/login"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Login</a>
							</c:if>
                            <a href="javascript:;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Checkout</a>
                            <a href="javascript:;"><i class="fa fa-heart" aria-hidden="true"></i> Wishlist</a>
                        </div><!--dropdown-->
                    </li>
                    <li>
                        <a href="javascript:;" class="fa fa-search"></a>
                    </li>
                    <li class="cart_add" id="cartPopup"></li>
                </ul><!--user_shoping-->
           <!--  </div> -->
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--header_new-->
<script>

function split(t) {
    return t.split(/,\s*/);
}

function extractLast(t) 
{
    return split(t).pop();
}
function monkeyPatchAutocomplete() {
    var oldFn = $.ui.autocomplete.prototype._renderItem;
    $.ui.autocomplete.prototype._renderItem = function(ul, item) {
       // var re = new RegExp("^" + this.term, "i");
      //  var t = item.label.replace(re, "<span>" + this.term + "</span>");
      console.log("Dheeraj Data "+ item);
    	console.log("Dheeraj Data "+ ul);
    	
        return $("<li></li>").data("item.autocomplete", item).append("<a>aaaaaaaaaaaaaaaaaa" + this.term +  "</a>").appendTo(ul)
    }
}

function monkeyPatchAutocomplete1(){
    var oldFn=$.ui.autocomplete.prototype._renderItem;
    $.ui.autocomplete.prototype._renderItem=function(ul,item){
    	
    	console.log("Dheeraj Data "+ item);
    	console.log("Dheeraj Data "+ ul);
        //var t=item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+$.ui.autocomplete.escapeRegex(this.term)+")(?![^<>]*>)(?![^&;]+;)","gi"),"<strong class='highlight'>$1</strong>");
        return $("<li></li>").data("item.autocomplete",item).append("<a>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+item.label+"</a>").appendTo(ul)}
        }

        
  
//monkeyPatchAutocomplete1();
//var t = !0, e = !0;
    $("#searchbox").autocomplete({
        source:function(t, e) {
            $.getJSON("/get_search_list", {
                term:extractLast(t.term)
            }, e);
        },
        search:function() {
        	console.log("DDDDDDDDDDDDDDD"+this.value)
        	console.log("DDDDDDDDDDDDDDD"+this.label)
            var t = extractLast(this.value);
            return t.length < 1 ? !1 :void 0;
        },
        focus:function(e, i) {
          
        },
        select:function(e, i) {
            console.log("select");
            console.log(i.item.value);
            var splittedArr= i.item.value.split('|');
            var catId=0;
            if(splittedArr.length ==2){
            	catId=splittedArr[1];
            }
            
            var selected = catId;//i.item.value;
            //selected = selected.toLowerCase().replace(/\s/g, '-');
            window.location.href = "/searchproduct?item="+selected;
            
        }
    })
    /*.data("ui-autocomplete")._renderItem = function (ul, item) {
         return $("<li></li>")
             .data("item.autocomplete", item)
             .append("<a>" + item.label + "</a>")
             .appendTo(ul);
     };*/
</script>

<script>
$(document).ready(function(){
    $.ajax({
        url: '/product/getcartpopup',
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        dataType: "html",
        success: function(response) {
            //console.log(response)
            $("#cartPopup").html(response);
        }
    });
    function removeProductCart(productId){
    $.ajax({
        url: '/product/removecart/'+productId,
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        dataType: "html",
        success: function(response) {
            //console.log(response)
            $("#cartPopup").html(response);
        }
    });
    }
})
</script>
<script>
$(document).ready(function(){
  $(".menu").click(function(){
    $("body").addClass("intro");
  });
    $(".close_btn").click(function(){
    $("body").removeClass("intro");
  });
});
</script>

<script>
    $(document).ready(function(){
$(".menu-anchor").click(function () {
    $(this).closest('li').toggleClass('dropdown');
    $(this).closest('li').siblings().removeClass('dropdown');
});
    });
</script>
