
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">


<head>
<title>All Fine Wines</title>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport"
	content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="stylesheet" href="/resources/css/style.css" type="text/css" />
<link rel="stylesheet" href="/resources/css/style_new.css"
	type="text/css" />
<link rel="shortcut icon" type="image/x-icon"
	href="/resources/images/favicon.png" />
<link rel="shortcut icon" type="image/x-icon"
	href="/resources/images/favicon.html" />
<link rel="stylesheet" href="/resources/css/responsive.css"
	type="text/css">
<!--
<filesmatch ".(html|php|css|js|ico|jpg|jpeg|png|gif|ttf|eot|woff|woff2|svg)$"=""></filesmatch> 
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
-->
<script src="/resources/zoom-slider/jquery-1.12.4.min.js"></script>
<script src="/resources/js/jquery.creditCardValidator.js"></script>
<style>
.pop-btn .close.age-btn {
    background: #ff324d; padding: 10px 30px;
    font-weight: 400; color: #fff; box-shadow: none; text-shadow: none; opacity: 1;
     border-radius: 5px;  font-size: 14px; min-width: 207px; min-height: 40px;
    margin: 0 10px; -webkit-transition: all .5s linear 0s;  -moz-transition: all .5s linear 0s;
    transition: all .5s linear 0s;    text-transform: uppercase;
    line-height: 20px;
}
.pop-btn .close.age-btn:hover { background: rgb(255 50 77 / 80%);}

</style>
</head>
<body id="scroll_top">

	<c:if test="${user == 'false'}">
		<div id="myModal" class="modal fade show">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<h2>Sorry for Interruption</h2>
						<h3>Please Login to Place Your Order</h3>
						<div class="pop-btn">
							<a href="/login" class="age-btn close"
								data-dismiss="modal">Login</a>
							<!-- <button type="button" class="close" data-dismiss="modal">I AM OVER 18</button> -->
							<a href="/signup" class="age-btn">Sign Up</a>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-backdrop fade show"></div>
	</c:if>

	<div class="header_search">
		<form>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="input_area">
							<input type="text" placeholder="Search" /> <a
								href="javascript:;" class="fa fa-search"></a> <a
								href="javascript:;" class="fa fa-close"></a>
							<div class="clearfix"></div>
						</div>
						<!--input_area-->
					</div>
					<!--cols-->
				</div>
				<!--row-->
			</div>
			<!--container-->
		</form>
	</div>
	<!--header_search-->
	<div class="main">
		<jsp:include page="/header" />
		<div class="breadcrumb_area breadcrumb_section_checkout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
						<p class="head">Checkout</p>
					</div>
					<!--cols-->
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
						<ul>
							<li><a href="/">Home</a></li>
							<li>Checkout</li>
						</ul>
					</div>
					<!--cols-->
				</div>
				<!--row-->
			</div>
			<!--container-->
		</div>
		<!--breadcrumb_area-->
		<div class="Checkout_area">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<c:if test="${user == 'false'}">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="customer_area">
										<i class="fa fa-user"></i> <span>Returning customer?</span> <a
											href="javascript:;" id="login">Click here to login</a>
									</div>
									<!--customer_area-->
									<div class="border_area" id="login-form">
										<p class="text">If you have shopped with us before, please
											enter your details below. If you are a new customer, please
											proceed to the Billing & Shipping section.</p>
										<form id="loginUser" name="loginUser" action="/login">
											<input type="text" class="form-control"
												placeholder="Username Or Email" name="email" id="email">
											<input class="form-control" type="password"
												placeholder="Password" name="loginpassword"
												id="loginpassword">

											<!-- <p class="c_checkbox">
                                            <label>
                                                <input type="checkbox" name="agree">
                                                <i></i>
                                                Remember me
                                            </label>
                                            <a href="javascript:;">Forgot password?</a>
                                        </p> -->
											<!-- <a class="c_btn1 full_width">
                                            <span>Login</span>
                                            <i></i>
											
                                        </a>
										<input type="submit" value="submit" /> -->
											<button class="c_btn1 btn_me" type="submit">
												<span class="addtocart"> Login </span>
											</button>
											<p id="loginMsg"></p>
										</form>
									</div>
									<!--border_area-->
								</div>
								<!--cols-->
							</c:if>

							<c:if
								test="${checkoutForm.couponApplied == null || checkoutForm.couponApplied == ''}">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="customer_area">
										<i class="fa fa-tag"></i> <span>Have a coupon?</span> <a
											href="javascript:;" id="coupon-code">Click here to enter
											your code</a>
									</div>
									<!--customer_area-->
									<div class="border_area" id="coupon-code-form">
										<p class="text">If you have a coupon code, please apply it
											below.</p>
										<form class="apply_coupon">
											<input type="text" class="form-control"
												placeholder="Enter Coupon Code..." id="couponCode" /> <a
												class="c_btn1 coupon_btn" href="javascript:;"
												onclick="checkcoupon();"> <span>Apply Coupon</span> <i></i>
											</a>
											<p style="color: red" id="couponMsg"></p>
										</form>
									</div>
									<!--border_area-->
								</div>
								<!--cols-->
							</c:if>
						</div>
						<!--row-->
						<div class="shopping_icon_lines">
							<i class="fa fa-credit-card"></i>
						</div>
						<!--shopping_icon_lines-->
						<form:form class="billing_details" action="/cart/order"
							modelAttribute="checkoutForm">
							<form:hidden path="couponApplied" />
							<form:hidden path="couponText" />
							<form:hidden path="couponDiscount" />
							<form:hidden path="siteId" value="${siteId}" />
							<form:hidden path="customerId"
								value="${loginresponse.customerId}" />
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<p class="heading">Billing Details</p>
									<form:input path="billingaddress.firstName"
										class="form-control textOnly" placeholder="First name *"
										id="billingFirstName" />
									<form:input path="billingaddress.lastName"
										class="form-control textOnly" placeholder="Last name *" />

									<form:select class="form-control" path="billingaddress.country">
										<option value="0">Select an option...</option>
										<c:forEach var="country" items="${countryList}">
											<option value="${country.key}">${country.value}</option>
										</c:forEach>
									</form:select>
									<form:input path="billingaddress.address" class="form-control"
										placeholder="Address *" />
									<form:input path="billingaddress.address1" class="form-control"
										placeholder="Address line2" />
									<form:input path="billingaddress.city"
										class="form-control textOnly" placeholder="City / Town *" />
									<form:input path="billingaddress.state"
										class="form-control textOnly" placeholder="State / County *" />
									<form:input path="billingaddress.zipCode" class="form-control"
										placeholder="Postcode / ZIP *" />
									<form:input path="billingaddress.contactNum"
										class="form-control numbersOnly" placeholder="Phone *" />
									<form:input path="billingaddress.email" class="form-control "
										placeholder="Email address *" id="billingEmail"
										onblur="registerUserCart()" />
									<c:if test="${user == 'false'}">
										<form:password path="password"
											class="form-control password_show_area"
											placeholder="Password *" id="billingPassword" />
									</c:if>


									<%-- Backup code
										<c:if test="${user == 'false'}">
										<p class="form_checkbox password_show_area">
											<i class="fa fa-check"></i>Create an account?
										</p>
									</c:if>
									<form:password path="password"
										class="form-control password_show" placeholder="Password *" /> --%>

									<p class="form_checkbox different_address_area">
										<form:checkbox path="differentShipping" value="" />
										Ship to a different address?
									</p>
									<div class="different_address">
										<form:input path="shippingaddress.firstName"
											class="form-control textOnly" placeholder="First name *" />
										<form:input path="shippingaddress.lastName"
											class="form-control textOnly" placeholder="Last name *" />

										<form:select class="form-control"
											path="shippingaddress.country">
											<option value="0">Select an option...</option>
											<c:forEach var="country" items="${countryList}">
												<option value="${country.key}">${country.value}</option>
											</c:forEach>
										</form:select>
										<form:input path="shippingaddress.address"
											class="form-control" placeholder="Address *" />
										<form:input path="shippingaddress.address1"
											class="form-control" placeholder="Address line2" />
										<form:input path="shippingaddress.city"
											class="form-control textOnly" placeholder="City / Town *" />
										<form:input path="shippingaddress.state"
											class="form-control textOnly" placeholder="State / County *" />
										<form:input path="shippingaddress.zipCode"
											class="form-control" placeholder="Postcode / ZIP *" />
										<form:input path="shippingaddress.contactNum"
											class="form-control numbersOnly" placeholder="Phone *" />
										<form:input path="shippingaddress.email" class="form-control"
											placeholder="Email address *" />
									</div>
									<!--different_address_area-->
									<p class="heading a_i_margin_top">Additional Information</p>
									<form:textarea path="orderNotes" class="form-control"
										placeholder="Order notes"></form:textarea>
								</div>
								<!--cols-->
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">


									<div class="Payment_method_box">
										<h4 class="pay_mthd_title">Payment Method</h4>
										<form:hidden path="paymentDetail.cardType" id="cardType" />
										<div class="payment_input">
											<form:input path="paymentDetail.cardHolderName"
												name="carnumber" class="pay_input pay_1"
												placeholder="Name On Card *" />
										</div>
										<div class="payment_input card_type_img">
											<form:input path="paymentDetail.cardNumber" name="carnumber"
												id="cardNumber" class="pay_input pay_2"
												placeholder="Enter Card Number *" />
											<span class="card_img"><img
												src="/resources/images/cardtype_img.png"></span>
										</div>

										<div class="payment_input">
											<div class="expire_valid">
												<span class="valid_thru">Valid thru</span> <span>
													<div class="expirey_mm">
														<form:select path="paymentDetail.expMonth" name="month"
															required="" tabindex="4">
															<option value="0">MM</option>
															<option value="01">01</option>
															<option value="02">02</option>
															<option value="03">03</option>
															<option value="04">04</option>
															<option value="05">05</option>
															<option value="06">06</option>
															<option value="07">07</option>
															<option value="08">08</option>
															<option value="09">09</option>
															<option value="10">10</option>
															<option value="11">11</option>
															<option value="12">12</option>
														</form:select>
													</div>
												</span> <span>
													<div class="expirey_yy">
														<form:select path="paymentDetail.expYear" name="year"
															required="" tabindex="4">
															<option value="0">YY</option>
															<option value="20">20</option>
															<option value="21">21</option>
															<option value="22">22</option>
															<option value="23">23</option>
															<option value="24">24</option>
															<option value="25">25</option>
															<option value="26">26</option>
															<option value="27">27</option>
															<option value="28">28</option>
															<option value="29">29</option>
															<option value="30">30</option>
															<option value="31">31</option>
														</form:select>
													</div>

												</span>
											</div>
										</div>
										<div class="payment_input">
											<form:input path="paymentDetail.ccv" name="carnumber"
												class="cvv_input" placeholder="CVV" />
										</div>


									</div>


									<div class="grey_back">
										<p class="heading">Your Orders</p>
										<hr />
										<ul class="your_orders">
											<li><b>Product</b></li>
											<li><b>Total</b></li>
										</ul>
										<hr />
										<div class="product_total">
											<c:forEach var="cart" items="${cartList}" varStatus="loop">
												<c:set var="cartTotal"
													value="${cartTotal+(cart.price*cart.quantity)}" />
												<c:if
													test="${not  fn:contains(cartProductsIds,cart.productId)}">
													<c:set var="cartProductsIds"
														value="${cartProductsIds}${cart.productId}," />
												</c:if>
												<c:if
													test="${not  fn:contains(cartSubSubCatIds,cart.subSubCatId)}">
													<c:set var="cartSubSubCatIds"
														value="${cartSubSubCatIds}${cart.subSubCatId}," />
												</c:if>
												<ul class="your_orders">
													<li>${cart.productName}<b>x ${cart.quantity}</b></li>
													<li><i class="fa fa-${currency}" aria-hidden="true"></i>${cart.price*cart.quantity}</li>
												</ul>
											</c:forEach>
											<input type="hidden" value="${cartProductsIds}"
												id="productIds" /> <input type="hidden"
												value="${cartSubSubCatIds}" id="subSubCatIds" /> <input
												type="hidden" id="totalAmount" value="${cartTotal}" />
										</div>
										<!--product_total-->
										<hr />
										<ul class="your_orders heading">
											<li><b>SubTotal</b></li>
											<li><b><i class="fa fa-${currency}"
													aria-hidden="true"></i>${cartTotal}</b></li>
										</ul>
										<hr />
										<ul class="your_orders">
											<li><b>Shipping</b></li>
											<li>Free Shipping</li>
										</ul>
										<hr />
										<div>
											<ul class="your_orders" id="couponDiv" style="display: none;">
												<li><b>Coupon Discount</b></li>
												<li><b><i class="fa fa-${currency}"
														aria-hidden="true"></i><span id="couponAmount"></span></b></li>
											</ul>
											<hr />
										</div>
										<ul class="your_orders">
											<li><b>Total</b></li>
											<li><b><i class="fa fa-${currency}"
													aria-hidden="true"></i><span id="finalTotalAmount">${cartTotal}</span></b></li>
										</ul>
										<form:hidden path="paymentDetail.paymentMode" />
										<p class="heading a_i_margin_top">Payment Platform</p>

										<ul class="payment_accordion">
											<!-- <li class="active">
												<div class="label" id="paytm">
													<input type="radio" name="paytm"
														onchange="capturePaymentModeCheckBox();" /> <i></i> Paytm
												</div> label
												<p>There are many variations of passages of Lorem Ipsum
													available, but the majority have suffered alteration.</p>
											</li> -->

											<li>
												<div class="label" id="stripe">
													<input type="radio" name="stripe"
														onchange="capturePaymentModeCheckBox();" /> <i></i>
													Stripe
												</div> <!--label-->
												<p>There are many variations of passages of Lorem Ipsum
													available, but the majority have suffered alteration.</p>
											</li>
											<li>
												<div class="label" id="Eway">
													<input type="radio" name="Eway"
														onchange="capturePaymentModeCheckBox();" /> <i></i>Eway
												</div> <!--label-->
												<p>Please send your cheque to Store Name, Store Street,
													Store Town, Store State / County, Store Postcode.</p>
											</li>
											<li>
												<div class="label" id="paypal">
													<input type="radio" name="paypal"
														onchange="capturePaymentModeCheckBox();" /> <i></i>
													Paypal
												</div> <!--label-->
												<p>Pay via PayPal; you can pay with your credit card if
													you don't have a PayPal account.</p>
											</li>
										</ul>
										<!--  <a class="c_btn1">
                                            <span onclick="submitorder()" class="checkoutbutton">Place Order</span>
                                            <i></i>
                                        </a>
										<input type="submit" value="submit" />  -->
										<button class="c_btn1 btn_me" type="submit">
											<span class="addtocart"> Place Order </span>
										</button>
									</div>
									<!--grey_back-->
								</div>
								<!--cols-->
							</div>
							<!--row-->
						</form:form>
					</div>
					<!--cols-->
				</div>
				<!--row-->
			</div>
			<!--container-->
		</div>
		<!--Checkout_area-->
		<!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
		<jsp:include page="footer.jsp" />
	</div>
	<!--main-->
	<a href="#scroll_top" class="scroll_top" id="scroll_top"> <span
		class="fa fa-angle-up"></span>
	</a>
	<script src="/resources/js/js.js"></script>
	<script src="/resources/js/plus-minus.js">
		/* min and max number */
	</script>
	<script src="/resources/js/jquery.validate.min.js"></script>
	<script type="text/javascript">
		function cardcheck(value) {
			$(this).validateCreditCard(function(result) {
				console.log(result);
				return result.valid

			});
		}

		$("#cardNumber").focusout(function() {
			$(this).validateCreditCard(function(result) {
				console.log(result.valid);
				return result.valid

			});
		})

		$('#login').click(function() {
			$('#login-form').slideToggle();
		});
		$('#coupon-code').click(function() {
			$('#coupon-code-form').slideToggle();
		});
		$(
				'div.Checkout_area form.billing_details p.form_checkbox.password_show_area')
				.click(
						function() {
							$(this).toggleClass("active");
							$(
									'div.Checkout_area form.billing_details .form-control.password_show')
									.slideToggle();
						});
		$(
				'div.Checkout_area form.billing_details p.form_checkbox.different_address_area')
				.click(
						function() {
							$(this).toggleClass("active");
							$(
									'div.Checkout_area form.billing_details div.different_address')
									.slideToggle();
						});
		$(
				"div.Checkout_area form.billing_details div.grey_back ul.payment_accordion > li")
				.click(
						function() {
							$(this).addClass("active");
							$(this).siblings().removeClass("active");
							var allLists = $(this).children();
							for (var liCtr = 0; liCtr < allLists.length; liCtr++) {
								switch (allLists[liCtr].id) {
								case 'Eway':
									//alert("razorpay")
									//$('#paymentDetail.paymentMode').val('check-payment');
									/* document.getElementById("paymentDetail.paymentMode").value="check-payment"; */
									document
											.getElementById("paymentDetail.paymentMode").value = "Eway";
									break;
								case 'paypal':
									//alert("paypal")
									//$('#paymentDetail.paymentMode').val('paypal');
									document
											.getElementById("paymentDetail.paymentMode").value = "paypal";
									break;

								/* case 'paytm':
									//alert('paytm')
									document
											.getElementById("paymentDetail.paymentMode").value = "paytm";
									break; */

								case 'stripe':
									//alert('stripe')
									document
											.getElementById("paymentDetail.paymentMode").value = "stripe";
									break;

								}
							}
						});

		$('.textOnly').keyup(function() {
			if (this.value != this.value.replace(/[^a-zA-Z ]+/g, '')) {
				this.value = this.value.replace(/[^a-zA-Z ]+/g, '');
			}
		});

		$('.numbersOnly').keyup(function() {
			if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
				this.value = this.value.replace(/[^0-9\.]/g, '');
			}
		});

		$("#checkoutForm").validate({
			rules : {
				'orderNotes' : {
					required : true
				},
				'billingaddress.firstName' : {
					required : true
				},
				'billingaddress.lastName' : {
					required : true
				},
				'billingaddress.address' : {
					required : true
				},
				'billingaddress.city' : {
					required : true
				},
				'billingaddress.state' : {
					required : true
				},
				'billingaddress.zipCode' : {
					required : true
				},
				'billingaddress.contactNum' : {
					required : true
				},
				'billingaddress.email' : {
					required : true,
					email : true
				},
				'billingaddress.country' : {
					required : true,
					selectcountry : true
				},
				'password' : {
					required : true
				},
				'shippingaddress.firstName' : {
					required : true
				},
				'shippingaddress.lastName' : {
					required : true
				},
				'shippingaddress.address' : {
					required : true
				},
				'shippingaddress.city' : {
					required : true
				},
				'shippingaddress.state' : {
					required : true
				},
				'shippingaddress.zipCode' : {
					required : true
				},
				'shippingaddress.contactNum' : {
					required : true
				},
				'shippingaddress.email' : {
					required : true,
					email : true
				},
				'shippingaddress.country' : {
					required : true,
					selectcountry : true
				},
				'paymentDetail.cardHolderName' : {
					required : false
				},
				'paymentDetail.cardNumber' : {
					required : false,
					cardvalid : true
				},
				'paymentDetail.expMonth' : {
					required : false,
					selectcountry : true
				},
				'paymentDetail.expYear' : {
					required : false,
					selectcountry : true
				},
				'paymentDetail.ccv' : {
					required : false
				}
			},
			errorPlacement : function(error, element) {
			},
			submitHandler : function(form) {
			}
		})

		$.validator.addMethod('selectcountry', function(value) {
			return (value != '0');
		}, "")

		$.validator.addMethod('cardvalid', function(value) {
			return cardcheck(value);
		}, "")

		$("#loginUser").validate(
				{
					rules : {
						email : {
							required : true
						},
						loginpassword : {
							required : true
						}
					},
					errorPlacement : function(error, element) {
					},
					submitHandler : function(form) {
						var data = {
							email : $("#email").val(),
							password : $("#loginpassword").val(),
							siteId : $("#siteId").val()
						}
						$.ajax({
							url : '/cart/checkout/login',
							type : 'post',
							contentType : 'application/json; charset=utf-8',
							data : JSON.stringify(data),
							dataType : "text",
							success : function(response) {
								var jsonResponse = JSON.parse(response);
								console.log(jsonResponse)

								$(".password_show_area").hide();
								$("#customerId").val(jsonResponse.customerId);
								$("#loginMsg").html(
										jsonResponse.responseStatus.message);
							}
						});
					}
				})

		function checkcoupon() {
			var couponCode = $("#couponCode").val();
			if (couponCode == '') {
				$("#couponMsg").text("Please enter coupon code");
				return false;
			}
			var totalAmount = $("#totalAmount").val();
			alert(totalAmount)
			var siteId = $("#siteId").val();
			var customerId = $("#customerId").val();
			var productIds = $("#productIds").val();
			var subSubCatIds = $("#subSubCatIds").val();

			var data = {
				couponCode : $("#couponCode").val(),
				siteId : $("#siteId").val(),
				customerId : $("#customerId").val(),
				totalAmount : $("#totalAmount").val(),
				productId : $("#productIds").val(),
				subSubCategoryId : $("#subSubCatIds").val(),
			}
			console.log(data);
			$.ajax({
				url : '/product/checkcoupon',
				type : 'post',
				contentType : 'application/json; charset=utf-8',
				data : JSON.stringify(data),
				dataType : "text",
				success : function(response) {
					var couponResponse = JSON.parse(response);
					console.log(couponResponse);
					$("#couponMsg").text(couponResponse.message);
					if (couponResponse.responseStatus.code == 1) {
						$("#couponApplied").val("Yes");
						$("#couponDiv").show();
						$("#couponAmount").text(couponResponse.discount);
						$("#finalTotalAmount").text(
								totalAmount - couponResponse.discount);
						$("#couponText").val(couponCode);
						$("#couponDiscount").val(couponResponse.discount);
					}
				}
			});
		}

		$(document)
				.ready(
						function() {
							var couponApplied = $("#couponApplied").val();
							if (couponApplied == 'Yes') {
								$("#couponDiv").show();
								var couponDiscount = $("#couponDiscount").val();
								var totalAmount = $("#totalAmount").val();
								$("#couponAmount").text(couponDiscount);
								$("#finalTotalAmount").text(
										totalAmount - couponDiscount);
							}

							$('#cardNumber')
									.validateCreditCard(
											function(result) {
												console.log(result);
												if (result.card_type != null) {
													$(".card_img")
															.html(
																	"<img src='/resources/images/"+result.card_type.name+".png'>");
													$("#cardType")
															.val(
																	result.card_type.name);
												}
												return result.valid;
											});

						});
	</script>
	<script>
		function registerUserCart() {
			var userForm = {
				email : $('#billingEmail').val(),
				password : $('#billingPassword').val(),
				name : $('#billingFirstName').val(),
				siteId : $("#siteId").val(),
				productId : $("#productIds").val(),
			}
			$.ajax({
				url : "/cart/signup",
				type : "post",
				contentType : 'application/json; charset=utf-8',
				data : JSON.stringify(userForm),
				dataType : "text",
				success : function(response) {
					var singupResponse = JSON.parse(response);
					if (singupResponse == null) {
						console.log("User not present")
					} else {
						$("#billingPassword").hide();
					}
				}
			})
		}
	</script>

	<script>
		$(document).ready(function() {
			// $("#myModal").modal('show');
			$(".age-btn").click(function() {
				$("#myModal").removeClass("show fade");
				$(".modal-backdrop").removeClass("show fade");
				$("#scroll_top").removeClass("modal-open");
				$("html").removeClass("modal-backdrop");
			});
		});
	</script>

</body>
</html>