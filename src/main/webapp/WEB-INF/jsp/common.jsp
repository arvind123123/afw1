<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="stylesheet" href="/resources/css/style.css?61555" type="text/css" />
<link rel="stylesheet" href="/resources/css/style_new.css" type="text/css" />
<link rel="stylesheet" href="/resources/css/responsive.css" type="text/css" />
<!--- owl carousel CSS-->

<link rel="shortcut icon" type="image/x-icon" href="/resources/images/favicon.png" />
<link rel="shortcut icon" type="image/x-icon" href="/resources/images/favicon.ico" />
<!--
<filesmatch ".(html|php|css|js|ico|jpg|jpeg|png|gif|ttf|eot|woff|woff2|svg)$"=""></filesmatch> 
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
-->
<script src="/resources/js/jquery.1.12.4.min.js"></script>