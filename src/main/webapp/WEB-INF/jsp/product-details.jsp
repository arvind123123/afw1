<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.*" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>${productDetail.metaTitle}</title>
    <meta name="description" content="${productDetail.metaDescription}">
    <meta name="keywords" content="${productDetail.metaKeywords}">
    <jsp:include page="common.jsp" />
	<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet">
    <link href="/resources/zoom-slider/jquery.exzoom.css?55" rel="stylesheet" type="text/css"/>

<script src="/resources/zoom-slider/jquery-1.12.4.min.js"></script>

<script src="/resources/zoom-slider/imagesloaded.pkgd.min.js"></script>
<script src="/resources/zoom-slider/jquery.exzoom.js"></script>

<script src="/resources/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$('.zoom_slider').imagesLoaded( function() {
$("#exzoom").exzoom({
autoPlay: false,
});
$("#exzoom").removeClass('hidden')
});
</script>


<style type="text/css">
    .datamainbx_disc{float: left; width: 100%; margin-top: 60px;}
    .disc_details {
    border-left: 5px #ff324d  solid!important;
    text-align: center;
    border-radius: 5px 0 0 5px !important;
}
.datamainbx_disc .disc_details {
    border-radius: 0;
    background: #f4f4f4;
    padding: 10px;
    margin-right: 0px;
    border-right: #ccc solid 1px;
    border-left: 0px;
    text-align: center;
    margin-bottom: 30px;
}
.datamainbx_disc .disc_details a {
    position: relative;
    text-decoration: none;
    font-weight: bold!important;
    font-size: 16px;
    color: #000;
}
.disc_additional {
    border-right: 5px #ff324d  solid!important;
    text-align: center;
    border-radius: 0px 5px 5px 0px !important
}
.datamainbx_disc .disc_additional {
    border-radius: 0;
    background: #f4f4f4;
    padding: 10px;
    margin-right: 0px;
    border-left: #ccc solid 1px !important;
    border-left: 0px;
    text-align: center;
    margin-bottom: 30px;
}
.datamainbx_disc .disc_additional a {
    position: relative;
    text-decoration: none;
    font-weight: bold!important;
    font-size: 16px;
    color: #000;
}

.table_additional_info ul{list-style: none;display: flex;margin-left: 15px;}
.table_additional_info ul .add_list_value{margin-right: 60px;} 
.content_details {
    width: 100%!important;
    line-height: 22px;
    border-right: #ccc solid 1px;
    min-height: 232px!important;
    margin-top: 20px;
    padding: 0 18px;
}
.col {width: 50%;}
.table>tbody>tr>td{padding: 3px; border-bottom: 1px solid #ddd; border-top: none;}
.table .label_key{font-weight: 700;}
.product_review_details{width: 100%; float: left; margin-bottom: 30px;}
.autoOpenPopup {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 100000000;
    display: none;
}
.autoOpenPopup > div {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    background: rgba(0, 0, 0, 0.7);
}
.autoOpenPopup div > .active {
    transform: scaleY(1);
    -webkit-transform: scaleY(1);
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
}
.autoOpenPopup > div > div {
    width: 350px;
    display: flex;
    flex-wrap: wrap;
    background: white;
    transform: scaleY(0);
    -webkit-transform: scaleY(0);
    transform-origin: bottom;
    -webkit-transform-origin: bottom;
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
    border-top: 6px solid #ff324d;
    padding: 20px 20px;
}

.autoOpenPopup > i.close {
    position: absolute;
    top: 0;
    right: 0;
    width: 60px;
    height: 60px;
    color: #fff;
    font-size: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    transition: 0.2s linear;
    -webkit-transition: 0.2s linear;
    -moz-transition: 0.2s linear;
    font-style: normal;
    opacity: 1;
    font-weight: normal;
}
.autoOpenPopup .close {
    float: right;
    font-size: 17px;
    font-weight: 400;
    color: #fff;
    text-shadow: 0 1px 0 #fff;
    opacity: 1;
    position: absolute;
    top: -20px;
    right: -17px;
    background: #ff324d;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    border-radius: 50%;
    -webkit-transition: all 0.5s linear 0s;
    -moz-transition: all 0.5s linear 0s;
    transition: all 0.5s linear 0s;
}
.autoOpenPopup .close:hover {background:#333}
.popup-inner {
    display: block;
    width: 100%;
    float: left;
}
.popup-inner h5 {
    font-size: 25px;
    font-weight: 700;
    color: #ff324d;
    text-align: center;
    margin-top: 0;
    margin-bottom: 15px;
}
.popup-inner p {
    color: #ff324d;
    font-size: 20px;
    text-align: center;
    margin: 0;
}
.popup-inner a.call-no {
    background: #ff324d;
    padding: 7px 8px;
    border-radius: 5px;
    color: #fff;
    font-size: 16px;
    font-weight: 400;
    margin-left: 5px;
    -webkit-transition: all 0.5s linear 0s;
    -moz-transition: all 0.5s linear 0s;
    transition: all 0.5s linear 0s;
}
.popup-inner a.call-no:hover {    background: #333;}
.call-no i {
    border: 1px solid #fff;
    border-radius: 50%;
    width: 24px;
    height: 24px;
    line-height: 24px;
    margin-right: 2px;
}

@media only screen and (max-width: 480px){.table_additional_info{margin-top: 10px;}
form.review_form textarea.form-control{height: 100px;}
form.review_form input.form-control{height: 40px;}
}
</style>


</head>
<body id="scroll_top">
    <div class="add_to_cart_alert_box animated bounceInDown" style="display:none;">
        <p>Item Added To Cart</p>
    </div>


    <div class="main">
        <div class="autoOpenPopup" >
  <div>
    <div class="active">
        <div class="popup-inner">
            <h5>Get upto 70% off</h5>
            <p>Call us Now <a href="tel:357464631" class="call-no"><i class="fa fa-phone" aria-hidden="true"></i> 357464631</a></p>
        </div>
        <i class="close">X</i>
    </div><!---->
     
  </div><!---->
 
</div>
<jsp:include page="/header" />
        <div class="breadcrumb_area breadcrumb_section_conatct">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Product Details</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li>Product Details</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        
        
        
        <c:set var="reviewCount" value="${fn:length(productDetail.reviews)}" />
        
        <div class="main_container">
            <div class="container">
                <div class="row">
                  <!--  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h1>Product Details</h1>
                    </div> -->
                    
                    <form name="addtocart" id="addtocart" action="" method="post">
        <input type="hidden" name="productId" id="productId" value="${productDetail.productId}" />
        <input type="hidden" name="quantity" id="quantity" value="1" />
        <input type="hidden" name="maxQuantity" id="maxQuantity" value="${productDetail.quantity}" />
        <input type="hidden" name="price" id="price" value="${productDetail.sellingPrice}" />
        <input type="hidden" name="productName" id="productName" value="${productDetail.name}" />
        <input type="hidden" name="productImage" id="productImage" value="https://drive.google.com/uc?export=view&id=${productDetail.images[0]}" />
        <input type="hidden" name="subSubCatId" id="subSubCatId" value="${productDetail.subSubCatId}" />
        
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 zoom_slider">
                        <div class="product_details_area">
                            <div class="exzoom hidden" id="exzoom">
                                <div class="exzoom_img_box">
                                    <ul class='exzoom_img_ul'>
                                    <c:forEach items="${productDetail.images}" var="image">
                                      <!--  <li><img src="https://drive.google.com/uc?export=view&id=${image}"/></li>  -->
                                      <li><img src="http://localhost:8090/resources/upload-dir/${image}"/>
                                      <!-- <div class="no-image"><img src="/resources/images/coming-soon.jpg"/></div> --> </li>
                                    </c:forEach>    
                                    </ul>
                                </div><!--exzoom_img_box-->
                                <div class="exzoom_nav"></div><!--exzoom_nav-->
                                <p class="exzoom_btn">
                                    <a href="javascript:void(0);" class="exzoom_prev_btn">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="exzoom_next_btn">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </p>
                            </div><!--exzoom-->
                            <div class="product_details_area_right">
                                <p class="head"><a href="javascript:;">${productDetail.name}</a></p>
                                <ul class="cost_stars price-cost">
                                    <li class="price-sec">
                                    <div class="price-main"> <%-- comment --%>
                                        <div class="costt">
                                            <span class="cost_line"><span>RRP:</span>
                                            <i class="fa fa-usd" aria-hidden="true"></i><div class="prc"> ${productDetail.price}</div>
                                            </span>
                                            <span class="cost"><i class="fa fa-usd" aria-hidden="true"></i>${productDetail.sellingPrice}</span>
                                            <span class="cost">
                                            <%-- <i class="fa fa-usd" aria-hidden="true"></i> ${productDetail.price} --%>
                                            </span>
                                        </div>
                                        <div class="total-quantity"><span>12 bottles - for $</span><div class="pack-price" id="pack-price">$552.00</div></div>
                                        
                                    </div>
                                      <!--  <span class="cost_off">35% Off</span> -->

                                   <div class="save-price" style="display: none;">
                                        <div class="dd">Save $<div class="save-price" id="saving">Save $218</div></div>
                                    </div> 
                                    </li>
                                    <li class="rating">
                                    <c:forEach  begin="1" end="${productDetail.rating}">
                                        <i class="fa fa-star"></i>
                                      </c:forEach>  
                                      <c:forEach  begin="${productDetail.rating+1}" end="4">
                                        <i class="fa fa-star"></i>
                                        </c:forEach> 
                                        <i class="fa fa-star-o"></i>
                                        <span id="reviewcount">(${reviewCount})</span>
                                    </li>
                                    <!-- =================Commented-by-dilshad-for-second-phase=================== -->
                                </ul><!--cost_stars-->
                                <p class="text">${productDetail.shortDescription}</p>
							<ul class="icon_list detail-availabel">
                               <li class="available"><i class="fa fa-check" aria-hidden="true"></i>
                                  <span>Availability:</span>In stock</li>
                               <li class="stock"><i class="fa fa-tag" aria-hidden="true"></i>
                                   Over 166 sold</li>
							</ul>
                               
                                
                                                                
                                 <c:forEach items="${productDetail.features}" var="feature">
                                 <c:if test="${feature.name == 'Size'}">
                                <p class="sizes_radio">
                                    <span>${feature.name}</span>
                                    <c:forEach items="${feature.values}" var="value">
                                    <label>
                                        <input type="radio" name="${feature.name}" value="${value}">
                                        <i>${value}</i>
                                    </label>
                                   </c:forEach>
                                </p>
                                </c:if>
                                <c:if test="${feature.name == 'Color'}">
                                 <p class="radio_buttons">
                                    <span>${feature.name}</span>
                                    <c:forEach items="${feature.values}" var="value">
                                    <label>
                                        <input type="radio" name="${feature.name}" value="${value}">
                                        <i class="back-${fn:toLowerCase(value)}"></i>
                                    </label>
                                     </c:forEach>                                   
                                </p>
                                </c:if>
                                </c:forEach>  
                                <div class="min_and_max">
                                    <input id="demo0" type="text" readonly="true" value="01" name="demo0" data-bts-min="1" data-bts-max="${productDetail.quantity}" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-success" data-bts-button-up-class="btn btn-success">
                                   <!--  <a class="c_btn1" href="javascript:;">
                                        <span class="addtocart"><i class="fa fa-shopping-cart"></i> Add to cart</span>
                                        <i></i>
                                    </a> -->
                                    <!-- <input type="submit" value="submit"> -->
                                    <button class="c_btn1 btn_me" type="submit" id="cartButton">
                                        <span class="addtocart"><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                    </button>

                                    <%-- <a class="glyphicon glyphicon-transfer icons" href="/product/compare/add/${productDetail.productId}"></a> --%>
                                    <a class="fa fa-heart icons" href="/product/addtowishlist/${productDetail.productId}"></a>
                                </div><!--min_and_max-->
                                <p class="category"><span>SKU:</span> ${productDetail.code}</p>
                                <p class="category"><span>Category:</span> ${productDetail.category}</p>
                                <p class="category"><span>Tags:</span> ${productDetail.tags}</p>
<!--                                 <p class="category">
                                    <span>Share:</span>
                                    
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.thelabelbar.com/product/detail/${productDetail.productId}" target="_blank" class="fa fa-facebook"></a>
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.thelabelbar.com/product/detail/${productDetail.productId}" target="_blank" class="fa fa-linkedin"></a>
                                    <a href="https://twitter.com/share?url=https://www.thelabelbar.com/product/detail/${productDetail.productId}" class="fa fa-twitter" data-show-count="false"></a>
                                </p> -->
                                   
                            </div><!--product_details_area_right-->
                        </div><!--product_details_area-->
                    </div><!--cols-->
                    </form>




                    <div class="datamainbx_disc">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="Description_details">
                                      <div class="disc_details">
                                        <a href="javascript:;">Description</a>
                                      </div>
                                      <div class="content_details">${productDetail.description}</div>
                                      </div><!--panel-body-->
                                    </div><!--panel-collapse-->
                    
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="">
                                      <div class="disc_value">
                                          <div class="table_additional_info">
                                             <div class="disc_additional">
                                        <a href="javascript:;">Additional</a>
                                      </div>
                                      <div class="content_details">
                                          <c:forEach items="${productDetail.additionalList}" var="add">
                                              <!-- <ul>
                                                <li class="add_list_value"><strong>${add.key}</strong></li>
                                                <li>${add.value}</li>
                                              </ul> -->
                                              <table class="table">
                                                <tbody>
                                                  <tr>
                                                    <td class="col label_key" scope="row">${add.key}</td>
                                                    <td class="col data_value">${add.value}</td>
                                                  </tr>
                                                </tbody>
                                              </table>

                                           </c:forEach>   
                                           </div>
                                          </div>
                                      </div>
                                    </div>
                    </div>


                    </div>


                    <div class="col-xs-12"><hr></div>

                    
                </div><!--row-->
            </div><!--container-->
        </div><!--main_container-->
        <hr />

        <c:if test="${fn:length(productDetail.similarProducts) > 0}">
        <div class="featured_products similar_detail">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Similar Products</h2>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 featured_products_slider">
            <c:forEach items="${productDetail.similarProducts}" var="product">
                <div class="item">
                    <div class="product_area">
                        <div class="image_area">
                            <img src="https://drive.google.com/uc?export=view&id=${product.imageUrl}" />
                            <span class="new">New</span>
                            <p>
                                <a href="/${smimilarProductMap.get(product.productId)}" class="fa fa-shopping-cart"></a>
                                    <%-- <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a> --%>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                            </p>
                        </div><!--image_area-->
                        <div class="head_cost_star_area"> 
                            <p class="head">
                               <a href="/${smimilarProductMap.get(product.productId)}">${product.name}</a>
                            </p>
                             <p class="cost">
                                  	<span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                                  <!--  <span>35% Off</span> -->
                                </p>
                            <p class="stars">
                                <c:forEach  begin="1" end="${product.rating}">
                                        <i class="fa fa-star"></i>
                                      </c:forEach>  
                                      <c:forEach  begin="${product.rating+1}" end="4">
                                        <i class="fa fa-star"></i>
                                        </c:forEach> 
                                         <i class="fa fa-star-o"></i>
                                <%-- <span>(${product.ratingCount})</span> --%>
                                <span><%="("+ (int) (Math.random() * 1000) +")"%>  </span>
                            </p>
                          <!--  <p class="radio_buttons">
                                <label>
                                    <input type="radio" name="radio" checked />
                                    <i class="back-brown"></i>
                                </label>
                                <label>
                                    <input type="radio" name="radio" />
                                    <i class="back-grey"></i>
                                </label>
                                <label>
                                    <input type="radio" name="radio" />
                                    <i class="back-red"></i>
                                </label>
                            </p> -->
                        </div><!--head_cost_star_area-->
                    </div><!--product_area-->
                </div><!--item-->
                </c:forEach>
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--featured_products-->        
</c:if>


<div class="product_review_details">
    <div class="container">
                                      <div class="row">
                                        <div class="col-xs-12 col-lg-12">
                                          <p class="head">${reviewCount} Review For <span>${productDetail.name}</span></p>
                                          <c:forEach items="${productDetail.reviews}" var="review">
                                          <ul class="reviews_area">
                                          <!--  <img src="images/reviews-1.jpg" />  -->
                                            <li>
                                                <p class="name">${review.name}</p>
                                                <p class="date_star">
                                                    <span> <fmt:parseDate value = "${review.date}" var="datee" pattern = "yyyy-MM-dd" />
                                                    <fmt:formatDate value="${datee}" pattern = "MMM dd, yyyy" />
                                                    </span>
                                                    <c:set var="startRating" value="${review.rating}" />
                                                    <c:set var="half" value="false" />
                                                    <c:if test="${fn:contains(review.rating,'.')}">
                                                    <c:set var="half" value="true" />
                                                    <c:set var="startRating" value="${fn:split(review.rating,'.')[0]}" />
                                                    </c:if>
                                                    <span>
                                                    <c:forEach begin="1" end="${startRating}">
                                                        <i class="fa fa-star"></i>
                                                    </c:forEach>
                                                    <c:if test="${half == 'true'}">
                                                    <i class="fa fa-star-half-o"></i>
                                                    <c:set var="startRating" value="${startRating+1}" />
                                                    </c:if>     
                                                    <c:forEach end="5" begin="${startRating+1}">
                                                        <i class="fa fa-star-o"></i>
                                                    </c:forEach>        
                                                        
                                                    </span>
                                                </p>
                                                <p class="text">${review.reviewText}</p>  
                                            </li>
                                          </ul>
                                          </c:forEach>
                                          <p class="head">Add a review</p>
                                          <form class="review_form" name="reviewForm" id="reviewForm" action="" method="post">
                                            <fieldset class="star_rating">
                                                <input type="radio" id="star5" name="rating" value="5" />
                                                <label class = "full" for="star5" title=""></label>

                                                <input type="radio" id="star4half" name="rating" value="4.5" />
                                                <label class="half" for="star4half" title=""></label>

                                                <input type="radio" id="star4" name="rating" value="4" />
                                                <label class = "full" for="star4" title=""></label>

                                                <input type="radio" id="star3half" name="rating" value="3.5" />
                                                <label class="half" for="star3half" title=""></label>

                                                <input type="radio" id="star3" name="rating" value="3" />
                                                <label class = "full" for="star3" title=""></label>

                                                <input type="radio" id="star2half" name="rating" value="2.5" />
                                                <label class="half" for="star2half" title=""></label>

                                                <input type="radio" id="star2" name="rating" value="2" />
                                                <label class = "full" for="star2" title=""></label>

                                                <input type="radio" id="star1half" name="rating" value="1.5" />
                                                <label class="half" for="star1half" title=""></label>

                                                <input type="radio" id="star1" name="rating" value="1" checked="checked"/>
                                                <label class = "full" for="star1" title=""></label>

                                                <input type="radio" id="starhalf" name="rating" value="half" />
                                                <label class="half" for="starhalf" title=""></label>
                                            </fieldset>
                                            <textarea class="form-control" name="reviewText" id="reviewText" placeholder="Your review*"></textarea>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <input type="text" class="form-control textOnly" name="name" id="name" placeholder="Enter Name*" />
                                                </div><!--cols-->
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email*" />
                                                </div><!--cols-->
                                            </div><!--row-->
                                          <!--  <a class="c_btn1" href="javascript:void(0)">
                                                <span>Submit Review</span>
                                                <i></i>
                                            </a> -->
                                             <button class="c_btn1 btn_me" type="submit">
                                                <span class="addtocart">Submit Review</span>
                                            </button>
                                            
                                          </form>
                                          <span id="reviewSuccess"></span>
                                      </div><!--col-->
                                      </div><!--row-->
                                    </div><!--container-->
</div>

<!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />        
        </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>

<script src="/resources/js/js.js"></script>
<script>
function gotocart(){
    window.location.href= "/product/viewshoppingcart";
}
$(document).ready(function(){
    $('.textOnly').keyup(function () {
                  if (this.value != this.value.replace(/[^a-zA-Z ]+/g, '')) {
                       this.value = this.value.replace(/[^a-zA-Z ]+/g, '');
                    }
            });
            
    $("form[name='addtocart']").validate({
         rules:{
            Color:{
                required : true
            },
            
             Size: {
              required: true
             }
         },
          errorPlacement: function(error, element) {   },
           errorClass : "bob",
                highlight: function(element) {
                    $(element).parent().addClass("error");
                },
                unhighlight: function(element) {
                    $(element).parent().removeClass("error");
                },
           submitHandler: function(form) {
                alert("submit");
                var data = {
                                    productId: $("#productId").val(),
                                    quantity: $("#demo0").val(),
                                    price: $("#price").val()*12,
                                    productName: $("#productName").val(),
                                    productImage: $("#productImage").val(),
                                    maxQuantity: $("#maxQuantity").val(),
                                    size: $("input[name='Size']:checked").val(),
                                    color: $("input[name='Color']:checked").val(),
                                    subSubCatId: $("#subSubCatId").val(),
                                }
                            $.ajax({
                                url: '/product/addtocart',
                                type: 'post',
                                contentType: 'application/json; charset=utf-8',
                                data: JSON.stringify(data),
                                dataType: "html",
                                success: function(response) {
                                    //console.log(response)
                                    $("#cartPopup").html(response);
                                    $(".add_to_cart_alert_box").show().delay(5000).fadeOut();
                                    $("#cartButton").attr('onclick', 'gotocart();');
                                    $("#cartButton").attr('type', 'button');
                                    $(".addtocart").html("<i class='fa fa-shopping-cart'></i> Go to Cart");
                                }
                            });
             
          }
     })
     
     
    $("form[name='reviewForm']").validate({
         rules:{
            rating:{
                required : true
            },
            
             name: {
              required: true
             },
            reviewText:{
                required : true
            },
             email: {
              required: true,
              email:true
             }
             
         },
          errorPlacement: function(error, element) {   },
           submitHandler: function(form) {
                
                var data = {
                            name : $("#name").val(),
                            reviewText : $("#reviewText").val(),
                            rating : $('input[name=rating]:checked').val(),
                            email : $("#email").val(),
                            productId: $("#productId").val()                            
                                }
                            $.ajax({
                                url: '/product/addreview',
                                type: 'post',
                                contentType: 'application/json; charset=utf-8',
                                data: JSON.stringify(data),
                                dataType: "text",
                                success: function(response) {
                                    console.log(response)
                                    $("#reviewSuccess").html(response);
                                }
                            });
             
          }
     })     

     
})
</script>
<script src="/resources/js/plus-minus.js">
/* min and max number */
    
</script>
<script>

var reviewcount=Math.floor(Math.random() * 300) + 1;
$('#reviewcount').html(' ('+reviewcount+')');

    var price = '${productDetail.price}'.trim();
    var sellingPrice = '${productDetail.sellingPrice}'.trim();
    var packamt = sellingPrice*12;
    var extAmount = price*12;
    var saving = extAmount - packamt;
    document.getElementById("saving").innerHTML = saving;
    document.getElementById("pack-price").innerHTML = packamt;
</script>
<script>
    $("input[name='demo0']").TouchSpin();
         
</script>
<script>
        window.onload = function () {
              setTimeout(function(){
                $(".autoOpenPopup").show();
              },500);
              setTimeout(function(){
                $(".autoOpenPopup>div>div").addClass('active')
              },500);

            //}
          }
          $(document).ready(function(){
            $(".autoOpenPopup i.close").click(function(){
              $(".autoOpenPopup>div>div").removeClass('active');
              setTimeout(function(){
                $(".autoOpenPopup").hide();
              },200);
            })
          })
     </script>
</body>
</html>