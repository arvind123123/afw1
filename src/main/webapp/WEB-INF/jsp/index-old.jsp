<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    
<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>

<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
<script src="/resources/js/parallax.js" ></script>
<!-- <script src="/resources/js/scripts.js"></script> -->



</head>
<body id="scroll_top">
    <div class="main">
        <jsp:include page="header.jsp" />    

<div class="banner">
            <div class="item">
                <div class="banner_image banner-1">
                    <div class="banner_text">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p class="get_upto">Get up to 50% off Today Only!</p>
                                    <p class="head">Woman Fashion</p>
                                    <p class="show_now">
                                        <a class="c_btn1" href="javascript:;">
                                            <span>Show Now</span>
                                            <i></i>
                                        </a>
                                    </p>
                                </div><!--cols-->
                            </div><!--row-->
                        </div><!--container-->
                    </div><!--banner_text-->
                </div><!--banner_image-->
            </div><!--item-->
            <div class="item">
                <div class="banner_image banner-2">
                    <div class="banner_text">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p class="get_upto">50% off in all products</p>
                                    <p class="head">Man Fashion</p>
                                    <p class="show_now">
                                        <a class="c_btn1" href="javascript:;">
                                            <span>Show Now</span>
                                            <i></i>
                                        </a>
                                    </p>
                                </div><!--cols-->
                            </div><!--row-->
                        </div><!--container-->
                    </div><!--banner_text-->
                </div><!--banner_image-->
            </div><!--item-->
            <div class="item">
                <div class="banner_image banner-3">
                    <div class="banner_text">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p class="get_upto">Taking your Viewing Experience to Next Level</p>
                                    <p class="head">Summer Fashion</p>
                                    <p class="show_now">
                                        <a class="c_btn1" href="javascript:;">
                                            <span>Show Now</span>
                                            <i></i>
                                        </a>
                                    </p>
                                </div><!--cols-->
                            </div><!--row-->
                        </div><!--container-->
                    </div><!--banner_text-->
                </div><!--banner_image-->
            </div><!--item-->
        </div><!--banner-->
        <!-- new category -->

        <style type="text/css">
        div.section{width: 100%;
    float: left;}
            .cat_overlap {
    position: relative;
    background-color: #fff;
    box-shadow: 0 0 10px rgb(0 0 0 / 20%);
    padding: 30px;
    z-index: 1;
    margin-top: -70px;
}

.radius_all_5, .radius_all_5:before, .radius_all_5:after {
    border-radius: 5px;
}
        </style>
        <!-- START SECTION CATEGORIES -->
<div class="section pt-0 small_pb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cat_overlap radius_all_5">
                    <div class="row align-items-center">
                        <div class="col-lg-3 col-md-4">
                            <div class="text-md-left">
                                <h4>Top Categories</h4>
                                <p class="mb-2">There are many variations of passages of Lorem</p>
                                <a href="#" class="btn btn-line-fill btn-sm">View All</a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-8">
                            <div class="row">
                                <div class="item col-lg-3">
                                    <div class="categories_box">
                                        <a href="#">
                                            <i class="fa fa-user"></i>
                                            <span>Fashion</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="item col-lg-3">
                                    <div class="categories_box">
                                        <a href="#">
                                            <i class="fa fa-heartbeat"></i>
                                            <span>Health</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="item col-lg-3">
                                    <div class="categories_box">
                                        <a href="#">
                                            <i class="  fa fa-rocket"></i>
                                            <span>Personal Care</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="item col-lg-3">
                                    <div class="categories_box">
                                        <a href="#">
                                            <i class="fa fa-ship"></i>
                                            <span>Bags</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION CATEGORIES -->
        <!--end of new category -->



        <!-- <div class="ads">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <a href="javascript:;" class="main_ads">
                            <div class="image_area">
                                <img src="/resources/images/ads_img1.jpg" />
                            </div>
                            <div class="text_area">
                                <p class="text">Super Sale</p>
                                <p class="head">New Collection</p>
                                <p class="shop_now"><span>Shop Now</span></p>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <a href="javascript:;" class="main_ads">
                            <div class="image_area">
                                <img src="/resources/images/ads_img2.jpg" />
                            </div>
                            <div class="text_area">
                                <p class="head">New Season</p>
                                <p class="sub_head">Sale 40% Off</p>
                                <p class="shop_now"><span>Shop Now</span></p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div> -->



        <div class="products">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h1>Exclusive Products</h1>
                    </div><!--cols-->
                </div><!--row-->
                <div class="row">
                <c:forEach items="${exclusiveProducts}" var="product">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="product_area">
                            <div class="image_area">
                                <img src="https://drive.google.com/uc?export=view&id=${product.imageUrl}" />
                                <span class="sale">Sale</span>
                                <p>
                                    <a href="/product/detail/${product.productId}" class="fa fa-shopping-cart"></a>
                                    <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                                </p>
                            </div><!--image_area-->
                            <div class="head_cost_star_area">
                                <p class="head">
                                    <a href="/product/detail/${product.productId}">${product.name}</a>
                                </p>
                                <p class="cost">
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                                   <!-- <span>35% Off</span> -->
                                </p>
                                
                                <p class="stars">
                                <c:forEach  begin="1" end="${product.rating}">
                                        <i class="fa fa-star"></i>
                                      </c:forEach>  
                                      <c:forEach  begin="${product.rating+1}" end="5">
                                        <i class="fa fa-star-o"></i>
                                        </c:forEach> 
                                <span>(${product.ratingCount})</span>
                            </p>
                            <!--    <p class="radio_buttons">
                                    <label>
                                        <input type="radio" name="radio" checked />
                                        <i class="back-brown"></i>
                                    </label>
                                    <label>
                                        <input type="radio" name="radio" />
                                        <i class="back-grey"></i>
                                    </label>
                                    <label>
                                        <input type="radio" name="radio" />
                                        <i class="back-red"></i>
                                    </label>
                                </p> -->
                            </div><!--head_cost_star_area-->
                        </div><!--product_area-->
                    </div><!--cols-->
                   </c:forEach>
                </div><!--row-->
            </div><!--container-->
        </div><!--products-->

        <!-- new section -->
<section class="section_new bg_light_blue2">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-lg-12">
                <div class="">
                    <div class="">
                        <div class="wpb_wrapper">
                            <div class="ts-product-category-banner-wrapper">
                                <div class="category-column">
                                    <div class="category-item active" data-index="0">
                                        <div class="icon has-hover-icon">
                                            <img src="/resources/images/glass-default.png" data-src="/resources/images/glass-default.png" alt="Icon">
                                            <img src="/resources/images/glass.png" data-src="/resources/images/glass.png" alt="Icon">              </div>
                                            <div class="category-name">
                                                <h3>Glasses</h3>
                                            </div>
                                        </div>
                                        <div class="category-item " data-index="1">
                                            <div class="icon has-hover-icon">
                                                <img src="/resources/images/bag-default.png" data-src="/resources/images/bag-default.png" alt="Icon"><img src="/resources/images/bag.png" data-src="/resources/images/bag.png" alt="Icon">              
                                            </div>
                                            <div class="category-name">
                                                <h3>Women Bags</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="banner_new_beg">
                                        <a href="#" data-index="0">
                                            <img src="/resources/images/category-glasses.jpg" data-src="/resources/images/category-glasses.jpg" alt="Banner">
                                        </a><a href="#" data-index="1" style="display: none">
                                            <img src="/resources/images/wm-bags-cat.jpg" data-src="/resources/images/wm-bags-cat.jpg" alt="Banner"></a>
                                        </div>
                                        <div class="category-column">
                                            <div class="category-item " data-index="2">
                                                <div class="icon has-hover-icon">
                                                    <img src="/resources/images/earing-default.png" data-src="/resources/images/earing-default.png" alt="Icon"><img src="/resources/images/earing.png" data-src="/resources/images/earing.png" alt="Icon">              
                                                </div>
                                                <div class="category-name">
                                                    <h3>Accessories</h3>
                                                </div>
                                            </div>
                                            <div class="category-item " data-index="3">
                                                <div class="icon has-hover-icon">
                                                    <img src="/resources/images/belt-default.png" data-src="/resources/images/belt-default.png" alt="Icon">
                                                    <img src="/resources/images/belt.png" data-src="/resources/images/belt.png" alt="Icon">             
                                                </div>
                                                <div class="category-name">
                                                    <h3>Belts</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </section>
        <!-- end of new section -->




        <!-- <div class="summer_collection">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <img src="/resources/images/tranding_img.png" />
                        <div class="content_area">
                            <p class="text">New season trends!</p>
                            <p class="head">Best Summer Collection</p>
                            <p class="sub_head">Sale Get up to 50% Off</p>
                            <p>
                                <a class="c_btn1" href="javascript:;">
                                    <span>Show Now</span>
                                    <i></i>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        
        <div class="featured_products">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Featured Products</h2>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 featured_products_slider">
            <c:forEach items="${featuredProducts}" var="product">
                <div class="item">
                    <div class="product_area">
                        <div class="image_area">
                            <img src="https://drive.google.com/uc?export=view&id=${product.imageUrl}" />
                            <span class="new">New</span>
                            <p>
                                <a href="/product/detail/${product.productId}" class="fa fa-shopping-cart"></a>
                                    <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                            </p>
                        </div><!--image_area-->
                        <div class="head_cost_star_area">
                            <p class="head">
                                    <a href="/product/detail/${product.productId}">${product.name}</a>
                                </p>
                                <p class="cost">
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                                  <!--  <span>35% Off</span> -->
                                </p>
                            <p class="stars">
                                <c:forEach  begin="1" end="${product.rating}">
                                        <i class="fa fa-star"></i>
                                      </c:forEach>  
                                      <c:forEach  begin="${product.rating+1}" end="5">
                                        <i class="fa fa-star-o"></i>
                                        </c:forEach> 
                                <span>(${product.ratingCount})</span>
                            </p>
                        <!--    <p class="radio_buttons">
                                <label>
                                    <input type="radio" name="radio" checked />
                                    <i class="back-brown"></i>
                                </label>
                                <label>
                                    <input type="radio" name="radio" />
                                    <i class="back-grey"></i>
                                </label>
                                <label>
                                    <input type="radio" name="radio" />
                                    <i class="back-red"></i>
                                </label>
                            </p> -->
                        </div><!--head_cost_star_area-->
                    </div><!--product_area-->
                </div><!--item-->
                </c:forEach>
                
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--featured_products-->        
<div class="testinomials">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                       
                <h2 class="text-center">Our Client Say!</h2>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 testinomials_slider">
                <div class="item">
                    <div class="testimonials_area">
                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi nam natus perferendis possimus quasi sint sit tempora voluptatem.</p>
                        <p class="img_name">
                            <img src="/resources/images/user_img1.jpg" />
                            <span>
                                <b>Lissa Castro</b>
                                <i>Designer</i>
                            </span>
                        </p>
                    </div><!--testimonials_area-->
                </div><!--item-->
                <div class="item">
                    <div class="testimonials_area">
                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi nam natus perferendis possimus quasi sint sit tempora voluptatem.</p>
                        <p class="img_name">
                            <img src="/resources/images/user_img2.jpg" />
                            <span>
                                <b>Alden Smith</b>
                                <i>Designer</i>
                            </span>
                        </p>
                    </div><!--testimonials_area-->
                </div><!--item-->
                <div class="item">
                    <div class="testimonials_area">
                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi nam natus perferendis possimus quasi sint sit tempora voluptatem.</p>
                        <p class="img_name">
                            <img src="/resources/images/user_img3.jpg" />
                            <span>
                                <b>Daisy Lana</b>
                                <i>Designer</i>
                            </span>
                        </p>
                    </div><!--testimonials_area-->
                </div><!--item-->
                <div class="item">
                    <div class="testimonials_area">
                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi nam natus perferendis possimus quasi sint sit tempora voluptatem.</p>
                        <p class="img_name">
                            <img src="/resources/images/user_img4.jpg" />
                            <span>
                                <b>John Becker</b>
                                <i>Designer</i>
                            </span>
                        </p>
                    </div><!--testimonials_area-->
                </div><!--item-->
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--testinomials-->        
<div class="why_us">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="why_us_area">
                    <p><i class="fa fa-truck"></i></p>
                    <p class="head">Free Delivery</p>
                    <p class="text">If you are going to use of Lorem, you need to be sure there anything</p>
                </div><!--why_us_area-->
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="why_us_area border">
                    <p><i class="fa fa-undo"></i></p>
                    <p class="head">30 Day Return</p>
                    <p class="text">If you are going to use of Lorem, you need to be sure there anything</p>
                </div><!--why_us_area-->
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="why_us_area">
                    <p><i class="glyphicon glyphicon-headphones"></i></p>
                    <p class="head">27/4 Support</p>
                    <p class="text">If you are going to use of Lorem, you need to be sure there anything</p>
                </div><!--why_us_area-->
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--why_us-->        
<!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />
            </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>

<script src="/resources/js/js.js"></script>
<script>


function split(t) {
    return t.split(/,\s*/);
}

function extractLast(t) 
{
    return split(t).pop();
}
function monkeyPatchAutocomplete() {
    var oldFn = $.ui.autocomplete.prototype._renderItem;
    $.ui.autocomplete.prototype._renderItem = function(ul, item) {
       // var re = new RegExp("^" + this.term, "i");
      //  var t = item.label.replace(re, "<span>" + this.term + "</span>");
        return $("<li></li>").data("item.autocomplete", item).append("<a>" + this.term + "</a>").appendTo(ul)
    }
}

function monkeyPatchAutocomplete1(){
    var oldFn=$.ui.autocomplete.prototype._renderItem;
    $.ui.autocomplete.prototype._renderItem=function(ul,item){
        //var t=item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+$.ui.autocomplete.escapeRegex(this.term)+")(?![^<>]*>)(?![^&;]+;)","gi"),"<strong class='highlight'>$1</strong>");
        return $("<li></li>").data("item.autocomplete",item).append("<a>"+item.label+"</a>").appendTo(ul)}
        }

        
  
//monkeyPatchAutocomplete1();
//var t = !0, e = !0;
    $("#searchbox").autocomplete({
        source:function(t, e) {
            $.getJSON("/get_search_list", {
                term:extractLast(t.term)
            }, e);
        },
        search:function() {
            var t = extractLast(this.value);
            return t.length < 1 ? !1 :void 0;
        },
        focus:function() {
          
        },
        select:function(e, i) {
            console.log("select");
            console.log(i.item.value);
            var selected = i.item.value;
            selected = selected.toLowerCase().replace(/\s/g, '-');
            window.location.href = "/searchproduct?item="+selected;
            
        }
    })
    /*.data("ui-autocomplete")._renderItem = function (ul, item) {
         return $("<li></li>")
             .data("item.autocomplete", item)
             .append("<a>" + item.label + "</a>")
             .appendTo(ul);
     };*/
</script>


</body>
</html>