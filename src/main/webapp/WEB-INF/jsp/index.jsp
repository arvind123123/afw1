<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Buy Wine Online | Finest Australian Wines | Allfinewines</title>
    <meta name="description" content="Buy Finest Australian Wines from a wide selection of varieties that includes red, white and sparkling wines. Checkout our online wine store and get it delivered to your friends & family in Australia.">
    <jsp:include page="common.jsp" />
<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/resources/js/jquery-ui.1.10.4.min.js" ></script>
<script src="/resources/js/parallax.js" ></script>
<link href="/resources/zoom-slider/jquery.exzoom.css" rel="stylesheet" type="text/css"/>
</head>
<body id="scroll_top">





 <div id="myModal" class="modal fade show">
    <div class="modal-dialog">
        <div class="modal-content">
			<div class="modal-body">
				<h3>YOU MUST BE 18 YEARS OF AGE TO ENTER THIS SITE</h3>
				<div class="pop-btn">
					<button type="button" class="close" data-dismiss="modal">I AM OVER 18</button>
					<a href="https://www.google.com/" class="age-btn">I'M NOT 18</a>
				</div>
            </div>
        </div>
    </div>
</div>





<div class="main_new">
    <jsp:include page="/header" />
    <div class="banner">
        <div class="item">
            <img class="main_img1" src="/resources/images/christmas-banner.jpg" width="1400" height="550" alt="responsive image" />
        </div>
        <div class="item">
            <img class="main_img1" src="/resources/images/banner_background1.jpg" width="1400" height="550" alt="responsive image"/> 
            <div class="banner_image banner-1">
                <div class="banner_img">
                    <div class="container">
                        <img class="main_img1" src="/resources/images/banner4.png" width="350" height="290" alt="responsive image"/>
                        <div class="main_img">
                            <img src="/resources/images/banner1.png" width="160" height="550" alt="responsive image"/>
                        </div>
                    </div>
                </div>
                <div class="banner_text">
                    <div class="container">
                        <div class="main_text">
                            <span class="special">Special Product</span>
                            <p class="head">Vintage Wine<br /> Reser 1985</p>
                            <p class="get_upto">But I must explain to you how all this mistaken idea of denouncing pleasure</p>
                            <p class="buttons">
                                <a href="javascript:;" class="btn-1"><span>Shop Now</span></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <img class="main_img1" src="/resources/images/banner-1.jpg" width="1400" height="550" alt="responsive image"/>
        </div>
        <div class="item">
            <img class="main_img1" src="/resources/images/banner-2.jpg" width="1400" height="550" alt="responsive image"/>
        </div>
        <div class="item">
            <img class="main_img1" src="/resources/images/bvanner-4.jpg" width="1400" height="550" alt="responsive image"/>
        </div>
    </div>
    <div class="n_section img_links">
        <div class="container">
            <div class="main_title">
                <p class="subtitle">Wine</p>
                <h2 class="head">Our Products</h2>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">                       
                    <div class="img_link">
                        <img src="/resources/images/red-wine.png" width="350" height="350" alt="responsive image"/>
                        <div class="anchor-btn"><a href="/red-wine"><span>Red Wine</span></a></div>
                    </div><!--img_link-->
                </div><!--cols-->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">                       
                    <div class="img_link">
                        <img src="/resources/images/whitewine1.jpg" width="350" height="350" alt="responsive image"/>
                        <div class="anchor-btn"><a href="/white-wine"><span>White Wine</span></a></div>
                    </div><!--img_link-->
                </div><!--cols-->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">                       
                    <div class="img_link">
                        <img src="/resources/images/mixed-wine.png" width="350" height="350" alt="responsive image"/>
                        <div class="anchor-btn"><a href="/mixed-cases"><span>Mixed Wine</span></a></div>
                    </div><!--img_link-->
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </div><!--n_section-->
    <div class="logos_area">
        <div class="container">
            <div class="main_title">
                <p class="subtitle">brands & partners</p>
                <h2 class="head">Our Favorite Partner</h2>
            </div><!--main_title-->
            <ul class="logos">
                <li>
                    <a href="javascript:;"><img src="/resources/images/logo-1.jpg" width="152" height="58" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/logo-2.jpg" width="152" height="58" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/logo-3.jpg" width="152" height="58" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/logo-4.jpg" width="152" height="58" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/logo-5.jpg" width="152" height="58" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/logo-6.jpg" width="152" height="58" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/logo-7.jpg" width="152" height="58" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/logo-8.jpg" width="152" height="58" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/logo-9.jpg" width="152" height="58" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/zilzie.jpg" width="152" height="66" alt="responsive image"/></a>
                </li>
                <li>
                    <a href="javascript:;"><img src="/resources/images/curtis-logo.jpg" width="152" height="66" alt="responsive image"/></a>
                </li>
            </ul>
        </div><!--container-->
    </div><!--logos_area-->
<div class="featured_products featured">
    <div class="container">
        <div class="main_title">
                <p class="subtitle">our products</p>
                <h2 class="head">Featured Wines</h2>
            </div><!--main_title-->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 featured_products_slider">
            <c:forEach items="${featuredProducts}" var="product">
                <div class="item">
                    <div class="product_area">
                        <div class="image_area">
                            <img src="http://localhost:8090/resources/upload-dir/${product.imageUrl}" width="255" height="250" alt="responsive image"/>
                            <span class="new">New</span>
                            <p>
                            <a href="/${categoriesMap.get(product.catId)}/${subcatMap.get(product.subCatId)}/${featuredUrlMap.get(product.productId)}" class="fa fa-shopping-cart" id="cartButton"></a>
                                   <%--  <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a> --%>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                            </p>
                        </div><!--image_area-->
                        <div class="head_cost_star_area">
                            <p class="head">
                                    <a href="/${categoriesMap.get(product.catId)}/${subcatMap.get(product.subCatId)}/${featuredUrlMap.get(product.productId)}">${product.name}</a>
                                </p>
                                <p class="cost">

                                   
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span> 
                                  <%--  <span>35% Off</span> --%>
                                  <!-- <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span> -->
                                </p>
                        </div><!--head_cost_star_area-->
                    </div><!--product_area-->
                </div><!--item-->
                </c:forEach>
                
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--featured_products-->
<!-- end of products slider -->
    <div class="produce_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6">
                    <img src="/resources/images/produce_area-2.png" class="img-responsive" width="540" height="835" alt="responsive image"/>
                </div><!--cols-->
                <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6">
                    <div class="produce_area_box">
                        <div class="main_title">
                            <p class="subtitle">Discover the process</p>
                            <h2 class="head">How do we produce wine?</h2>
                        </div><!--main_title-->
                        <ul class="produce_area_area">
                            <li>
                                <img src="/resources/images/produce_area-3.png" alt="responsive image"/>
                                <p class="head">Harvesting</p>
                                <p class="text">But I must explain to you how all this mistaken idea of denouncing pleasure and pra was bor.</p>
                            </li>
                            <li>
                                <img src="/resources/images/produce_area-4.png" alt="responsive image"/>
                                <p class="head">Crushing & Pressing</p>
                                <p class="text">But I must explain to you how all this mistaken idea of denouncing pleasure and pra was bor.</p>
                            </li>
                            <li>
                                <img src="/resources/images/produce_area-5.png" alt="responsive image"/>
                                <p class="head">Fermentation</p>
                                <p class="text">But I must explain to you how all this mistaken idea of denouncing pleasure and pra was bor.</p>
                            </li>
                            <li>
                                <img src="/resources/images/produce_area-6.png" alt="responsive image"/>
                                <p class="head">Inspection & Bottling</p>
                                <p class="text">But I must explain to you how all this mistaken idea of denouncing pleasure and pra was bor.</p>
                            </li>
                        </ul>
                    </div><!--produce_area_box-->
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </div><!--produce_area-->
<!-- sale product -->
<div class="sale_products featured_products featured">
    <div class="container">
        <div class="main_title">
                <p class="subtitle">our products</p>
                <h2 class="head">Sale Product</h2>
            </div><!--main_title-->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sale_products_slider">
            <c:forEach items="${featuredProducts}" var="product">
                <div class="item">
                    <div class="product_area">
                        <div class="image_area">
                            <img src="https://drive.google.com/uc?export=view&id=${product.imageUrl}" width="255" height="250" alt="responsive image"/>
                            <span class="new">New</span>
                            <p>
                                <a href="/${categoriesMap.get(product.catId)}/${subcatMap.get(product.subCatId)}/${featuredUrlMap.get(product.productId)}" class="fa fa-shopping-cart" id="cartButton"></a>
                                   <%--  <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a> --%>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                            </p>
                        </div><!--image_area-->
                        <div class="head_cost_star_area">
                            <p class="head">
                                     <a href="/${categoriesMap.get(product.catId)}/${subcatMap.get(product.subCatId)}/${featuredUrlMap.get(product.productId)}">${product.name}</a>
                                </p>
                                <p class="cost">

                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                                    <!-- <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span> -->
                                </p>

                        </div><!--head_cost_star_area-->
                    </div><!--product_area-->
                </div><!--item-->
                </c:forEach>
                
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--featured_products-->
<!-- end of sale product -->
<!-- best saller -->
<div class="featured_products best_seller featured">
    <div class="container">
        <div class="main_title">
                <p class="subtitle">our products</p>
                <h2 class="head">Best Seller</h2>
            </div><!--main_title-->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 best_products_slider">
            <c:forEach items="${featuredProducts}" var="product">
                <div class="item">
                    <div class="product_area">
                        <div class="image_area">
                            <img src="https://drive.google.com/uc?export=view&id=${product.imageUrl}" width="255" height="250" alt="responsive image"/>
                            <span class="new">New</span>
                            <p>
                                <a href="/${categoriesMap.get(product.catId)}/${subcatMap.get(product.subCatId)}/${featuredUrlMap.get(product.productId)}" class="fa fa-shopping-cart" id="cartButton"></a>
                                    <%-- <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a> --%>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                            </p>
                        </div><!--image_area-->
                        <div class="head_cost_star_area">
                            <p class="head">
                                     <a href="/${categoriesMap.get(product.catId)}/${subcatMap.get(product.subCatId)}/${featuredUrlMap.get(product.productId)}">${product.name}</a>
                                </p>
                                <p class="cost">

                                     <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                                   <!--  <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span> -->
                                  <!--  <span>35% Off</span> -->
                                </p>
                        </div><!--head_cost_star_area-->
                    </div><!--product_area-->
                </div><!--item-->
                </c:forEach>
                
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--featured_products-->
<!-- end of best saller -->
<!-- Top Rating -->
<div class="featured_products featured">
    <div class="container">
        <div class="main_title">
                <p class="subtitle">our products</p>
                <h2 class="head">Top Rating</h2>
            </div><!--main_title-->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 top_rating_slider">
            <c:forEach items="${featuredProducts}" var="product">
                <div class="item">
                    <div class="product_area">
                        <div class="image_area">
                            <img src="https://drive.google.com/uc?export=view&id=${product.imageUrl}" width="255" height="250" alt="responsive image"/>
                            <span class="new">New</span>
                            <p>
                                <a href="/${categoriesMap.get(product.catId)}/${subcatMap.get(product.subCatId)}/${featuredUrlMap.get(product.productId)}" class="fa fa-shopping-cart" id="cartButton"></a>
                                    <%-- <a href="/product/compare/add/${product.productId}" class="glyphicon glyphicon-transfer"></a> --%>
                                    <a href="/product/addtowishlist/${product.productId}" class="fa fa-heart"></a>
                            </p>
                        </div><!--image_area-->
                        <div class="head_cost_star_area">
                            <p class="head">
                                     <a href="/${categoriesMap.get(product.catId)}/${subcatMap.get(product.subCatId)}/${featuredUrlMap.get(product.productId)}">${product.name}</a>
                                </p>
                                <p class="cost">

                                      <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.sellingPrice}</span>
                                    <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                                    <!-- <span><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span> -->
                                  <!--  <span>35% Off</span> -->
                                </p>
                        </div><!--head_cost_star_area-->
                    </div><!--product_area-->
                </div><!--item-->
                </c:forEach>
                
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--featured_products-->
<!-- end of Top Rating -->
     <!-- END OF our team -->
    <div class="testinomials">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 testinomials_slider">
                    <div class="item">
                        <div class="testimonials_area">
                            <p class="text">Fantastic!!!   The quickest delivery I ever got . They delivered my dozen yesterday- exactly on the same date when they promised to do. Placing online order was also very easy and the package was delivered in good condition.</p>
                            <p class="img_name">Lissa Castro - Designer</p>
                        </div><!--testimonials_area-->
                    </div><!--item-->
                    <div class="item">
                        <div class="testimonials_area">
                            <p class="text">This is the best wine shopping portal I ever saw. I booked my wines within minutes. They have quite a huge collection and the prompt reply and smooth process of booking really surprised me.</p>
                            <p class="img_name">Alden Smith - Designer</p>
                        </div><!--testimonials_area-->
                    </div><!--item-->
                    <div class="item">
                        <div class="testimonials_area">
                            <p class="text">One of my friends referred this website. I am really glad he did. My choice of wine displayed 'out of stock' on other websites. But I found a lot of stock here.  Thanks for delivering it on time.</p>
                            <p class="img_name">Daisy Lana - Designer</p>
                        </div><!--testimonials_area-->
                    </div><!--item-->
                    <div class="item">
                        <div class="testimonials_area">
                            <p class="text">It has been more than 4 years since I have been purchasing wine through websites but never have I got my choice of wine on the date it displayed at the time of booking. I got very reliable service here.</p>
                            <p class="img_name">John Becker - Designer</p>
                        </div><!--testimonials_area-->
                    </div><!--item-->
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </div><!--testinomials-->

    <jsp:include page="footer.jsp" />
</div><!--main_new-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>

<div class="main_container zoom_slider_popup" id="featured_search_popup" style="display: none;">
    <div class="zoom_slider">
        <div class="product_details_area">
            <div class="exzoom hidden" id="exzoom">
                <div class="exzoom_img_box">
                    <ul class='exzoom_img_ul'>
                        <li><img src="/resources/zoom-slider/1.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/2.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/3.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/4.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/5.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/6.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/7.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/8.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/1.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/2.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/3.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/4.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/5.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/6.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/7.jpg" alt="responsive image"/></li>
                        <li><img src="/resources/zoom-slider/8.jpg" alt="responsive image"/></li>
                    </ul>
                </div><!--exzoom_img_box-->
                <div class="exzoom_nav"></div><!--exzoom_nav-->
                <p class="exzoom_btn">
                    <a href="javascript:void(0);" class="exzoom_prev_btn">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a href="javascript:void(0);" class="exzoom_next_btn">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </p>
            </div><!--exzoom-->
            <div class="product_details_area_right">
                <p class="head"><a href="javascript:;">Blue Dress For Woman</a></p>
                <ul class="cost_stars">
                    <li>
                        <span class="cost">$45.00</span>
                        <span class="cost_line">$55.25</span>
                        <span class="cost_off">35% Off</span>
                    </li>
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <i class="fa fa-star-o"></i>
                        <span>(21)</span>
                    </li>
                </ul><!--cost_stars-->
                <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                <ul class="icon_list">
                    <li><i class="fa fa-shield"></i> 1 Year AL Jazeera Brand Warranty</li>
                    <li><i class="fa fa-undo"></i> 30 Day Return Policy</li>
                    <li><i class="fa fa-money"></i> Cash on Delivery available</li>
                </ul>
                <p class="radio_buttons">
                    <span>Color</span>
                    <label>
                        <input type="radio" name="radio" checked="">
                        <i class="back-brown"></i>
                    </label>
                    <label>
                        <input type="radio" name="radio">
                        <i class="back-grey"></i>
                    </label>
                    <label>
                        <input type="radio" name="radio">
                        <i class="back-red"></i>
                    </label>
                </p>
                <p class="sizes_radio">
                    <span>Sizes</span>
                    <label>
                        <input type="radio" name="size">
                        <i>XS</i>
                    </label>
                    <label>
                        <input type="radio" name="size">
                        <i>S</i>
                    </label>
                    <label>
                        <input type="radio" name="size">
                        <i>M</i>
                    </label>
                    <label>
                        <input type="radio" name="size">
                        <i>L</i>
                    </label>
                    <label>
                        <input type="radio" name="size">
                        <i>XL</i>
                    </label>
                    <label>
                        <input type="radio" name="size">
                        <i>2XL</i>
                    </label>
                    <label>
                        <input type="radio" name="size">
                        <i>3XL</i>
                    </label>
                </p>
                <div class="min_and_max">
                    <input id="demo0" type="text" value="01" name="demo0" data-bts-min="1" data-bts-max="100" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-success" data-bts-button-up-class="btn btn-success">
                    <a class="c_btn1" href="javascript:;">
                        <span><i class="fa fa-shopping-cart"></i> Add to cart</span>
                        <i></i>
                    </a>
                    <a class="glyphicon glyphicon-transfer icons" href="compare.php"></a>
                    <a class="fa fa-heart icons" href="javascript:;"></a>
                </div><!--min_and_max-->
                <p class="category"><span>SKU:</span> BE45VGRT</p>
                <p class="category"><span>Category:</span> Clothing</p>
                <p class="category"><span>Tags:</span> Cloth, Printed</p>
                <p class="category">
                    <span>Share:</span>
                    <a href="javascript:;" target="_blank" class="fa fa-facebook"></a>
                    <a href="javascript:;" target="_blank" class="fa fa-twitter"></a>
                    <a href="javascript:;" target="_blank" class="fa fa-pinterest"></a>
                    <a href="javascript:;" target="_blank" class="fa fa-youtube"></a>
                    <a href="javascript:;" target="_blank" class="fa fa-instagram"></a></p>
            </div><!--product_details_area_right-->
        </div><!--product_details_area-->
        <i class="fa fa-times"></i>
        <div class="clear"></div>
    </div><!--zoom_slider-->
</div><!--main_container-->

<div class="form-popup" id="info">
</div>
<script src="/resources/js/js.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script>
  $(document).ready(function(){
    $("#myModal").modal('show');
    $("button.close").click(function(){
        $("#myModal").removeClass("show fade");
        $(".modal-backdrop").removeClass("show fade");
        $("#scroll_top").removeClass("modal-open");
        $("html").removeClass("modal-backdrop");
  });
  });
</script>

<script src="/resources/js/plus-minus.js">
 
</script>
<script>
    $("input[name='demo0']").TouchSpin();
</script>
<script>
    $(document).ready(function(){
$(window).scroll(function() {
    if ($(this).scrollTop() > 1000) {
        $('#info').fadeIn();
    } else {
        $('#info').fadeOut();
    }
}).trigger('scroll');
    });
</script>
<script>
    $(document).ready(function(){
    $(".cross").click(function(){
    $(".form-popup").toggleClass("pop");
    });
    });
</script>
</body>
</html>
