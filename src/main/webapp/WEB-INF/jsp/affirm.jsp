<!DOCTYPE html>
<html lang="en">
  <title>All Fine Wines</title>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
 /**************************************************************\
          Step 1: Set up Affirm.js
\**************************************************************/
var _affirm_config = {
  public_api_key: "DX5GOIGQ1CYD7Y72" /* sandbox public api key */,
//    public_api_key: "J03H2DJH5CAVUN2I" /* production public api key */,
  script: "https://cdn1-sandbox.affirm.com/js/v2/affirm.js"
};

/**************************************************************\
          Step 2: Initialize Affirm
\**************************************************************/
(function(m, g, n, d, a, e, h, c) {
  var b = m[n] || {},
    k = document.createElement(e),
    p = document.getElementsByTagName(e)[0],
    l = function(a, b, c) {
      return function() {
        a[b]._.push([c, arguments]);
      };
    };
  b[d] = l(b, d, "set");
  var f = b[d];
  b[a] = {};
  b[a]._ = [];
  f._ = [];
  b._ = [];
  b[a][h] = l(b, a, h);
  b[c] = function() {
    b._.push([h, arguments]);
  };
  a = 0;
  for (
    c = "set add save post open empty reset on off trigger ready setProduct".split(
      " "
    );
    a < c.length;
    a++
  )
    f[c[a]] = l(b, d, c[a]);
  a = 0;
  for (c = ["get", "token", "url", "items"]; a < c.length; a++)
    f[c[a]] = function() {};
  k.async = !0;
  k.src = g[e];
  p.parentNode.insertBefore(k, p);
  delete g[e];
  f(g);
  m[n] = b;
})(
  window,
  _affirm_config,
  "affirm",
  "checkout",
  "ui",
  "script",
  "ready",
  "jsReady"
);

/**************************************************************\
          Step 3: Render Affirm Checkout
\**************************************************************/


var checkout_time = 20;

// include expire_time() in your checkout object for the expiration timestamp
var expire_time = function(){
    var current_time = Date.now();
    var new_time = current_time + (checkout_time*60*1000);
    var timestamp = new Date(new_time);
    var iso_timestamp = timestamp.toISOString().split('.')[0]+"Z";
    return iso_timestamp
}


affirm.checkout({
  merchant: {
    user_cancel_url: "https://thelabelbar.com/",
    user_confirmation_url: "https://thelabelbar.com/",
    user_confirmation_url_action: "GET",
    merchant: "Careerera"
  },

  shipping: {
    name: {
      first: "Rakesh Yadav",    
      last: "Rakesh Yadav"
    },
    address: {
      line1: "2 -Industrial Park Drive",   
   line2: "sector 55",    
      city: "Waldorf",
      state: "MD",
      country: "United States",
      zipcode: "20602"
    },
    email: "Rakesh.w2c@gmail.com",
    phone_number: "4155555555"
  },
  billing: {
    name: {
      first: "Rakesh Yadav",
      last: "Rakesh Yadav"
    },
    address: {
      line1: "2 -Industrial Park Drive",
      line2: "sector 55",
      city: "Waldorf",
      state: "MD",
      country: "United States",
      zipcode: "20602"
    },
    email: "Rakesh.w2c@gmail.com",
    phone_number: "4155555555"
  },
  metadata: {
    mode: "modal"
  },
  items: [
    {
      sku: "Spanish Language Training",
      display_name: "Careerera",
      unit_price: 59900, // 50 = 50*100 cent
      qty: 1
    }
  ],
  total: 59900,
   checkout_expiration : expire_time()
});


</script>

</head>
<body>
<input type="button" id="submit-form" value="Pay With Affirm" />



<script>

/**************************************************************\
              Step 4: Handle callbacks
\**************************************************************/
$("#submit-form").click(function() {
  affirm.checkout.open({
    onFail: function(e) {
	alert("Affirm Error page callled...");
  window.location.href= "https://thelabelbar.com";
      console.log(e);
    },
    onSuccess: function(a) {
		alert("Affirm Success page callled...");
       console.log(a);
    }
  });
});

</script>

</body>
</html>