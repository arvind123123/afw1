<div class="footer">
    <div class="container">
        <div class="row">
            <div class="subscribe">
                <p class="footer_logo" style="display:flex;justify-content:center;">
                    <a href="/"><img src="/resources/images/logo_footer.png" /></a>
                </p>
                <div class="col-xs-12 col-sm-7 newsletters_">
                    <div>
                        <h3 class="heading">Subscribe Our Newsletter</h3>
                        <form>
                            <input type="text" class="form-control" placeholder="Enter Email Address">
                            <a class="c_btn2" href="javascript:;">
                                <span>Subscribe</span>
                            </a>
                        </form>
                    </div>
                </div><!--cols-->
                <div class="col-xs-12 col-sm-5">
                    <p class="text">If you are going to use of Lorem Ipsum need to be sure there isn't hidden of text</p>
                    <p class="social_icons">
                        <a href="javascript:;" target="_blank" class="fa fa-facebook"></a>
                        <a href="javascript:;" target="_blank" class="fa fa-twitter"></a>
                        <a href="javascript:;" target="_blank" class="fa fa-pinterest"></a>
                        <a href="javascript:;" target="_blank" class="fa fa-youtube"></a>
                        <a href="javascript:;" target="_blank" class="fa fa-instagram"></a>
                    </p>
                </div>
            </div><!--subscribe-->
            
            <div class="col-xs-12 col-md-3">
                <p class="head">Useful Links</p>
                <ul class="footer_links">
                    <li><a href="/aboutus">About Us</a></li>
                    <li><a href="/faqs">FAQ</a></li>
                    <li><a href="/terms-conditions">Terms & Conditions</a></li>
                    <li><a href="javascript:;">Affiliates</a></li>
                    <li><a href="/contactus">Contact</a></li>
                </ul>
            </div><!--cols-->
            <div class="col-xs-12 col-md-3">
                <p class="head">Category</p>
                <ul class="footer_links">
                    <li><a href="javascript:;">Men</a></li>
                    <li><a href="javascript:;">Woman</a></li>
                    <li><a href="javascript:;">Kids</a></li>
                    <li><a href="javascript:;">Best Saller</a></li>
                    <li><a href="javascript:;">New Arrivals</a></li>
                </ul>
            </div><!--cols-->
            <div class="col-xs-12 col-md-3">
                <p class="head">My Account</p>
                <ul class="footer_links">
                    <li><a href="javascript:;">My Account</a></li>
                    <li><a href="javascript:;">Discount</a></li>
                    <li><a href="javascript:;">Returns</a></li>
                    <li><a href="javascript:;">Orders History</a></li>
                    <li><a href="javascript:;">Order Tracking</a></li>
                </ul>
            </div><!--cols-->
            <div class="col-xs-12 col-md-3">
                <p class="head">Contact Info</p>
                <p class="contact_info">
                    <i class="fa fa-map-marker"></i>
                    123 Street, Old Trafford, New South London, UK.
                </p>
                <p class="contact_info">
                    <i class="fa fa-envelope"></i>
                    <a href="javascript:;" target="_blank">+91-000-000-0000</a>
                </p>
                <p class="contact_info">
                    <i class="fa fa-phone"></i>
                    <a href="javascript:;" target="_blank">demo@demo.com</a>
                </p>
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--footer-->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <p class="copyrifootertxt" style="min-height:32px;display:flex;align-items:center;">&copy; 2020 All Rights Reserved by Bestwebcreator.</p>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <p class="right_area">
                    <img src="/resources/images/visa.png" />
                    <img src="/resources/images/discover.png" />
                    <img src="/resources/images/master_card.png" />
                    <img src="/resources/images/paypal.png" />
                    <img src="/resources/images/american_express.png" />
                </p>
            </div><!--cols-->
        </div><!--row-->
    </div><!--container-->
</div><!--copyright-->