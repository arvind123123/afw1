<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />
    </head>
<body id="scroll_top">
    <div class="main">
<jsp:include page="header.jsp" />      
       <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">Wishlist</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li>Wishlist</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="wishlist">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="main_wishlist_area_head">
                            <li>Product</li>
                            <li>Price</li>
                            <li>Stock Status</li>
                            <li>Remove</li>
                        </ul>
						<c:forEach items="${wishlist}" var="product">
                        <ul class="main_wishlist_area">
                            <li>
                                <img src="${product.image}" class="product_img" />
                            </li>
                            <li>
                                <a href="javascript:;">${product.name}</a>
                            </li>
                            <li>
                                <span class="cost"><i class="fa fa-${currency}" aria-hidden="true"></i>${product.price}</span>
                            </li>
                            <li>
                                <span class="in_stock">${product.status}</span>
                            </li>
							<c:if test="${product.status == 'In Stock'}">
                            <li>
                                <a class="c_btn1" href="/product/detail/${product.productId}">
                                    <span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                    <i></i>
                                </a>
                            </li>
							</c:if>
							<c:if test="${product.status != 'In Stock'}">
                            <li>
                                <a class="c_btn1" href="javascript:;">
                                    <span><i class="fa fa-shopping-cart"></i> Unavailable</span>
                                    <i></i>
                                </a>
                            </li>
							</c:if>
                            <li><a href="/product/removewishlist/${product.productId}" class="fa fa-close"></a></li>
                        </ul>
                       </c:forEach> 
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--wishlist-->
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />       
	   </div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>