<!DOCTYPE html>
<html lang="en">
<head>
    <title>All Fine Wines</title>
    <jsp:include page="common.jsp" />

    </head>
<body id="scroll_top">
       
<div class="main">
<jsp:include page="header.jsp" />         
        <div class="breadcrumb_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <p class="head">404</p>
                    </div><!--cols-->
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>404</li>
                        </ul>
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--breadcrumb_area-->
        <div class="four_zero_four_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="main_404">
                            <h1 class="heading">404</h1>
                            <p class="sub_heading">oops! The page you requested was not found!</p>
                            <p class="text">The page you are looking for was moved, removed, renamed or might never existed.</p>
                            <form>
                                <div class="input_area">
                                    <input type="text" placeholder="Search" class="form-control">
                                    <!-- <a href="javascript:;" class="fa fa-search"></a> -->
                                </div><!--input_area-->
                                <a class="c_btn1" href="./">
                                    <span>Back To Home <i fa fa-home></i></span>
                                </a>
                            </form>
                            <div class="clearfix"></div>
                        </div><!--main_404-->
                    </div><!--cols-->
                </div><!--row-->
            </div><!--container-->
        </div><!--four_zero_four_area-->
        <!-- <div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="heading">Subscribe Our Newsletter</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form>
                    <input type="text" class="form-control" placeholder="Enter Email Address">
                    <a class="c_btn2" href="javascript:;">
                        <span>Subscribe</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div> -->
<jsp:include page="footer.jsp" />           
		</div><!--main-->
<a href="#scroll_top" class="scroll_top" id="scroll_top">
    <span class="fa fa-angle-up"></span>
</a>
<script src="/resources/js/js.js"></script>
</body>
</html>