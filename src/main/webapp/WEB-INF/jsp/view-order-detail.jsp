<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="order_dtl_position">
    <div class="order_detail_popup_inner">
    <h3 class="order_detail_title">Order Details</h3>
    <a class="order_close">X</a>
    <div class="show_tbl">
    <table class="table table_show_pop table-bordered">
    <thead>
      <tr>
        <th>Customer Name</th>
        <th>Customer Email</th>
        <th>Customer Contact</th>
        <th>Payment Mode</th>
       
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>${orderDetails.customerName}</td>
        <td>${orderDetails.customerEmail}</td>
        <td>${orderDetails.customerContact}</td>
        <td>${orderDetails.paymentMode}</td>
       
      </tr>
     
     
      
    </tbody>
  </table>
  <h4>Product Details</h4>
  <table class="table table_show_pop table-bordered">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Product Quantity</th>
        <th>Product Price</th>
       
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>${orderDetails.productDetail.productName}</td>
        <td>${orderDetails.productDetail.quantity}</td>
        <td>${orderDetails.productDetail.price}</td>
       
      </tr>
     
     
      
    </tbody>
  </table>
  <h4>Shipping Details</h4>
  <table class="table table_show_pop table-bordered">
    <thead>
      <tr>
        <th>Shipping Address</th>
        <th>Shipping City</th>
        <th>Shipping State</th>
        <th>Shipping Zip</th>
       
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>${orderDetails.shippingAddress.address}</td>
        <td>${orderDetails.shippingAddress.city}</td>
        <td>${orderDetails.shippingAddress.state}</td>
        <td>${orderDetails.shippingAddress.zipCode}</td>
       
      </tr>
     
     
      
    </tbody>
  </table>
  
  <h4>Payment Details</h4>
  <table class="table table_show_pop table-bordered">
    <thead>
      <tr>
        <th>Card Name</th>
        <th>Card Number</th>
        <th>Card Month</th>
        <th>Card Year</th>
        <th>Card CCV</th>
       
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>${orderDetails.paymentDetail.cardHolderName}</td>
        <td>${orderDetails.paymentDetail.cardNumber}</td>
        <td>${orderDetails.paymentDetail.expMonth}</td>
		<td>${orderDetails.paymentDetail.expYear}</td>
		<td>${orderDetails.paymentDetail.ccv}</td>
       
      </tr>
     
     
      
    </tbody>
  </table>
  
   <h4>Order History</h4>
  <table class="table table_show_pop table-bordered">
    <thead>
      <tr>
        <th>status</th>
        <th>Comment</th>
        <th>Date</th>
        
      </tr>
    </thead>
    <tbody>
    
      <c:forEach items="${orderHistory}" var="history">
	  <tr>
        <td>${history.status}</td>
        <td>${history.comment}</td>
        <td>${history.date}</td>
		
      </tr>
     </c:forEach>
     
     
      
    </tbody>
  </table>
  
     </div>


 </div></div>