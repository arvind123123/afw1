<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="header">
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 left_area">
                    <div class="select_icon">
                        <i class="fa fa-flag left_top"></i>
                        <select class="flag">
                            <option selected>United States</option>
                            <option>France</option>
                            <option>English</option>
                        </select>
                    </div><!--select_icon-->
                    <div class="select_icon ">
                        <i class="fa fa-money left_top"></i>
                        <select class="flag">
                            <option selected>USD</option>
                            <option>EUR</option>
                            <option>GBR</option>
                        </select>
                    </div><!--select_icon-->
                    <a href="javascript:;" target="_blank">
                        <i class="glyphicon glyphicon-phone"></i>
                        <span>000-000-0000</span>
                    </a>
                </div><!--cols-->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right_area">
                    <a href="/product/compare"><i class="glyphicon glyphicon-transfer"></i> Compare</a>
                    <a href="/wishlist"><i class="fa fa-heart"></i> Wishlist</a>
					<c:if test="${loginresponse != null}">
					<a href="/myorders"><i class="glyphicon glyphicon-user"></i> ${loginresponse.name}</a>
					</c:if>
					<c:if test="${loginresponse == null}">
					<a href="/login"><i class="glyphicon glyphicon-user"></i> Login</a>
					</c:if>
                    
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </div><!--header_top-->
    <div class="header_bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="/" class="logo"><img src="/resources/images/logo.png" /></a>
                    <ul class="right_area">
                        <li>
                            <a href="javascript:;" class="search"><i class="fa fa-search"></i></a>
                        </li>
                        <li id="cartPopup">
                            
                        </li>
                        <li>
                            <a href="javascript:;" class="menu">
                                <div></div>
                                <div></div>
                                <div></div>
                            </a>
                        </li>
                    </ul><!--right_area-->
                    <ul class="navigation_bar">
                        <li><a href="/">Home</a></li>
                        <li>
                            <a href="javascript:;">Pages <i class="fa fa-angle-down"></i></a>
                            <ul>
                                <li><a href="login.html">Login</a></li>
                                <li><a href="signup.html">Register</a></li>
                                <li><a href="products.html">Products</a></li>
                                <li><a href="product-details.html">Product Details</a></li>
                                <li><a href="compare.html">Compare</a></li>
                                <li><a href="wishlist.html">Wishlist</a></li>
                                <li><a href="shopping-cart.html">Shopping Cart</a></li>
                                <li><a href="checkout.html">Checkout</a></li>
                                <li><a href="order-complete.html">Order Complete</a></li>
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="faq.html">Faq</a></li>
                                <li><a href="terms-conditions.html">Terms & Conditions</a></li>
                                <li><a href="404.html">404</a></li>
                                <li><a href="contact-us.html">Contact Us</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;">Products <i class="fa fa-angle-down"></i></a>
                            <ul>
							<c:forEach items="${categories}" var="category">
                                <li><a href="javascript:;">${category.name}</a></li>
                             </c:forEach>   
                            </ul>
                        </li>
                        <li><a href="javascript:;">Shop <i class="fa fa-angle-down"></i></a>
                            <ul>
                                <li><a href="javascript:;">Lorem Ipsum</a></li>
                                <li><a href="javascript:;">Lorem Ipsum</a></li>
                                <li><a href="javascript:;">Lorem Ipsum</a></li>
                                <li><a href="javascript:;">Lorem Ipsum</a></li>
                                <li><a href="javascript:;">Lorem Ipsum</a></li>
                            </ul>
                        </li>
                        <li><a href="/contactus">Contact Us</a></li>
                    </ul>
                </div><!--cols-->
            </div><!--row-->
        </div><!--container-->
    </div><!--header_bottom-->
</div><!--header--> 

<script>
$(document).ready(function(){
					$.ajax({
								url: '/product/getcartpopup',
								type: 'get',
								contentType: 'application/json; charset=utf-8',
								dataType: "html",
								success: function(response) {
									//console.log(response)
									$("#cartPopup").html(response);
								}
							});
							function removeProductCart(productId){
							$.ajax({
								url: '/product/removecart/'+productId,
								type: 'get',
								contentType: 'application/json; charset=utf-8',
								dataType: "html",
								success: function(response) {
									//console.log(response)
									$("#cartPopup").html(response);
								}
							});
							}
})
</script>