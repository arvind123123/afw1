var productApp = angular.module("productApp", ['infinite-scroll']);

productApp.factory("getSearchedProducts", function($http) {
	return{
		getProductResponse : function(item){
			return $http({
				url: "/getProductResponse/"+item,
				method: "GET",
				headers: {
		            'Content-Type': '*'
		        }
			}).then(function(response){
				return response.data;
			})
		}
	}
})

productApp.directive("productLoader", function() {
	return{
		//template: "result is loading ...",
		link: function($scope,element){
			$scope.$watch("productLoader",function(val){
				if(val){
					$(element).show();
				}
				else{
					$(element).hide();
				}
			})
				
			
		}
	}
})

productApp.controller("productController", function($scope,$timeout,$http,$filter,$window,getSearchedProducts,$timeout) {
	$scope.item = $("#item").val();
	$scope.productList = [];
	$scope.productDisplay = 9;
	$scope.brandList = [];
	$scope.selectedBrands = [];
	$scope.categoryList = [];
	$scope.sizeList = [];
	$scope.category = "";
	$scope.size = "";
	$scope.sliderMinFare = 0;
	$scope.sliderMaxFare = 0;
	$scope.minFare = 0;
	$scope.maxFare = 0;
	$scope.currency = $("#currency").val();
	$scope.results = getSearchedProducts.getProductResponse($scope.item);
	$scope.results.then(function(data){
		//console.log(data);
		$scope.productList = data;
		
		angular.forEach($scope.productList,function(item,index){
			if(index == 0){
				$scope.sliderMinFare = item.price;
				$scope.sliderMaxFare = item.price;
				$scope.minFare = item.price;
				$scope.maxFare = item.price;
			}
			if($scope.sliderMinFare > item.price){
				$scope.sliderMinFare = item.price;
				$scope.minFare = item.price;
			}
			if($scope.sliderMaxFare < item.price){
				$scope.sliderMaxFare = item.price;
				$scope.maxFare = item.price;
			}
			if($scope.brandList.indexOf(item.brand) == -1){
			$scope.brandList.push(item.brand);
			}
			if($scope.categoryList.length == 0){
				
					var categoryObj = {"name":item.category,"count":$scope.productList.filter((obj) => obj.category === item.category).length};
					$scope.categoryList.push(categoryObj);
				}
			angular.forEach($scope.categoryList,function(category){
				
				if(category.name != item.category){
					var categoryObj = {"name":item.category,"count":$scope.productList.filter((obj) => obj.category === item.category).length};
					$scope.categoryList.push(categoryObj);
				}
			})
			
				angular.forEach(item.sizes.split(','),function(size){
					if($scope.sizeList.indexOf(size) == -1){
					$scope.sizeList.push(size);
					}
				})
			
			
		})
			// $timeout(function() {
			$("#slider-range").slider({
               range:true,
               min: Math.floor($scope.minFare),
               max: Math.ceil($scope.maxFare),
               values: [$scope.sliderMinFare, $scope.sliderMaxFare],
               slide: function( event, ui ) {
        		  $("#amount").html("<i class='fa fa-"+$scope.currency+"'></i>"+Math.floor(ui.values[0]) + " - <i class='fa fa-"+$scope.currency+"'></i>" + Math.ceil(ui.values[1]));
				  $scope.$apply(function() {
                        $scope.sliderMinFare = ui.values[0];
						$scope.sliderMaxFare = ui.values[1];
					
                    });
               }
            });
	
			$("#amount").html("<i class='fa fa-"+$scope.currency+"'></i>"+Math.floor($scope.sliderMinFare) +" - <i class='fa fa-"+$scope.currency+"'></i>"+Math.ceil($scope.sliderMaxFare));
			//// }, 1000);
			$('html, body').animate({scrollTop: '10px'}, 500);
			
	})


function noFilter(filterObj) {
	        for (var key in filterObj) {
	      
	            if (filterObj[key]) {
	                return false;
	            }
	        }
	        return true;
	    }
		
	$scope.loadMore = function(){
		
		if ($scope.productDisplay + 6 < $scope.productList.length) {
            $scope.productDisplay += 6;
        } else {
            $scope.productDisplay = $scope.productList.length;
        }
	}
	
		
	$scope.brandFilter = function(product){
		
		return $scope.selectedBrands[product.brand] || noFilter($scope.selectedBrands);
	}
	
	$scope.selectedCategory = function(category){
		$scope.category = category;
		
	}
	$scope.onclickSize =  function(size){
		$scope.size = size;
		
	}
	$scope.categoryFilter = function(product){
		
		if(product.category == $scope.category){
			return product;
		}
		else if($scope.category == ''){
			return product;
		}
	}
	
	$scope.sizeFilter = function(product){
		if($scope.size != ''){
			var matched = false;
		angular.forEach(product.sizes.split(','),function(size){
			if(size == $scope.size){
				matched = true;
			
			}
		})
			if(matched)
			return product;
		}
		else if($scope.size == ''){
			return product;
		}
	}
			
	$scope.priceRange = function(item){
		
		return item.price >= $scope.sliderMinFare && item.price <= $scope.sliderMaxFare;
	}
	
	})