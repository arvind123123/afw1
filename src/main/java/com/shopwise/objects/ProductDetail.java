package com.shopwise.objects;

import java.util.HashMap;
import java.util.List;

public class ProductDetail {

	private String name;
	private double price;
	private double sellingPrice;
	private List<String> images;
	private String description;
	private String tags;
	private String code;
	private String category;
	private String brandName;
	private int quantity;
	private int productId;
	private List<ProductReview> reviews;
	private int rating;
	private String shortDescription;
	private String cashDelivery;
	private int returndays;
	private List<ProductFeatures> features;
	private HashMap<String, String> additionalList;
	private List<Products> similarProducts;
	private int subSubCatId;
	private String metaTitle;
	private String metaKeywords;
	private String metaDescription;
	

	public HashMap<String, String> getAdditionalList() {
		return additionalList;
	}

	public void setAdditionalList(HashMap<String, String> additionalList) {
		this.additionalList = additionalList;
	}

	public String getCashDelivery() {
		return cashDelivery;
	}

	public void setCashDelivery(String cashDelivery) {
		this.cashDelivery = cashDelivery;
	}

	public int getReturndays() {
		return returndays;
	}

	public void setReturndays(int returndays) {
		this.returndays = returndays;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public List<ProductFeatures> getFeatures() {
		return features;
	}

	public void setFeatures(List<ProductFeatures> features) {
		this.features = features;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public List<ProductReview> getReviews() {
		return reviews;
	}

	public void setReviews(List<ProductReview> reviews) {
		this.reviews = reviews;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public List<Products> getSimilarProducts() {
		return similarProducts;
	}

	public void setSimilarProducts(List<Products> similarProducts) {
		this.similarProducts = similarProducts;
	}

	public int getSubSubCatId() {
		return subSubCatId;
	}

	public void setSubSubCatId(int subSubCatId) {
		this.subSubCatId = subSubCatId;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public String getMetaKeywords() {
		return metaKeywords;
	}

	public void setMetaKeywords(String metaKeywords) {
		this.metaKeywords = metaKeywords;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}
}
