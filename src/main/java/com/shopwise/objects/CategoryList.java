package com.shopwise.objects;

import java.util.List;

public class CategoryList {

	private int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private String name;
	private List<SubCategoryList> subCatList;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<SubCategoryList> getSubCatList() {
		return subCatList;
	}
	public void setSubCatList(List<SubCategoryList> subCatList) {
		this.subCatList = subCatList;
	}
	
	
}
