package com.shopwise.objects;

public class Products {
	
	private String name;
	private double price;
	private double sellingPrice;
	private String imageUrl;
	private int productId;
	private String brand;
	private String category;
	private String sizes;
	private int ratingCount;
	private int rating;
	private int subCatId;
	private int catId;
	
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public int getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(int subCatId) {
		this.subCatId = subCatId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getCategory() {
		return category;
	}
	@Override
	public String toString() {
		return "Products [name=" + name + ", price=" + price + ", sellingPrice=" + sellingPrice + ", imageUrl="
				+ imageUrl + ", productId=" + productId + ", brand=" + brand + ", category=" + category + ", sizes="
				+ sizes + ", ratingCount=" + ratingCount + ", rating=" + rating + "]";
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSizes() {
		return sizes;
	}
	public void setSizes(String sizes) {
		this.sizes = sizes;
	}
	public int getRatingCount() {
		return ratingCount;
	}
	public void setRatingCount(int ratingCount) {
		this.ratingCount = ratingCount;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public double getSellingPrice() {
		return sellingPrice;
	}
	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
}
