package com.shopwise.objects;

import java.util.List;

public class CustomerDetails {

	public List<WishlistResponse> wishList;
	public List<Orders> orderList;
	public List<WishlistResponse> getWishList() {
		return wishList;
	}
	public void setWishList(List<WishlistResponse> wishList) {
		this.wishList = wishList;
	}
	public List<Orders> getOrderList() {
		return orderList;
	}
	public void setOrderList(List<Orders> orderList) {
		this.orderList = orderList;
	}
	
	
}
