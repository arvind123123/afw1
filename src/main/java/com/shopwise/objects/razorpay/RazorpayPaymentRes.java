package com.shopwise.objects.razorpay;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"razorpay_payment_id",
"razorpay_order_id",
"razorpay_signature"
})
public class RazorpayPaymentRes {

@JsonProperty("razorpay_payment_id")
private String razorpayPaymentId;
@JsonProperty("razorpay_order_id")
private String razorpayOrderId;
@JsonProperty("razorpay_signature")
private String razorpaySignature;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("razorpay_payment_id")
public String getRazorpayPaymentId() {
return razorpayPaymentId;
}

@JsonProperty("razorpay_payment_id")
public void setRazorpayPaymentId(String razorpayPaymentId) {
this.razorpayPaymentId = razorpayPaymentId;
}

@JsonProperty("razorpay_order_id")
public String getRazorpayOrderId() {
return razorpayOrderId;
}

@JsonProperty("razorpay_order_id")
public void setRazorpayOrderId(String razorpayOrderId) {
this.razorpayOrderId = razorpayOrderId;
}

@JsonProperty("razorpay_signature")
public String getRazorpaySignature() {
return razorpaySignature;
}

@JsonProperty("razorpay_signature")
public void setRazorpaySignature(String razorpaySignature) {
this.razorpaySignature = razorpaySignature;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}