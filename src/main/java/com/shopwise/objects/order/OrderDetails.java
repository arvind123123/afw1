package com.shopwise.objects.order;

import java.util.List;

public class OrderDetails {

	private int siteId;
	private int customerId;
	private String customerName;
	private String customerEmail;
	private String customerContact;
	private String paymentMode;
	private String orderComment;
	private BillingAddress billingAddress;
//	private PaymentDetail paymentDetail;
	private List<ProductDetail> productList;
	private ShippingAddress shippingAddress;
	private String couponApplied;
	private String couponCode;
	private double counponDiscount;
	
	public int getSiteId() {
		return siteId;
	}
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	public String getCustomerContact() {
		return customerContact;
	}
	public void setCustomerContact(String customerContact) {
		this.customerContact = customerContact;
	}
	public BillingAddress getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}
//	public PaymentDetail getPaymentDetail() {
//		return paymentDetail;
//	}
//	public void setPaymentDetail(PaymentDetail paymentDetail) {
//		this.paymentDetail = paymentDetail;
//	}
	
	public List<ProductDetail> getProductList() {
		return productList;
	}
	public void setProductList(List<ProductDetail> productList) {
		this.productList = productList;
	}
	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getOrderComment() {
		return orderComment;
	}
	public void setOrderComment(String orderComment) {
		this.orderComment = orderComment;
	}
	public String getCouponApplied() {
		return couponApplied;
	}
	public void setCouponApplied(String couponApplied) {
		this.couponApplied = couponApplied;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public double getCounponDiscount() {
		return counponDiscount;
	}
	public void setCounponDiscount(double counponDiscount) {
		this.counponDiscount = counponDiscount;
	}
	
	
}
