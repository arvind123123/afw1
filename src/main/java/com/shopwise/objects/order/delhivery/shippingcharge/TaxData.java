
package com.shopwise.objects.order.delhivery.shippingcharge;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "swacch_bharat_tax",
    "IGST",
    "SGST",
    "service_tax",
    "krishi_kalyan_cess",
    "CGST"
})
public class TaxData {

    @JsonProperty("swacch_bharat_tax")
    private Integer swacchBharatTax;
    @JsonProperty("IGST")
    private Integer iGST;
    @JsonProperty("SGST")
    private Double sGST;
    @JsonProperty("service_tax")
    private Integer serviceTax;
    @JsonProperty("krishi_kalyan_cess")
    private Integer krishiKalyanCess;
    @JsonProperty("CGST")
    private Double cGST;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("swacch_bharat_tax")
    public Integer getSwacchBharatTax() {
        return swacchBharatTax;
    }

    @JsonProperty("swacch_bharat_tax")
    public void setSwacchBharatTax(Integer swacchBharatTax) {
        this.swacchBharatTax = swacchBharatTax;
    }

    @JsonProperty("IGST")
    public Integer getIGST() {
        return iGST;
    }

    @JsonProperty("IGST")
    public void setIGST(Integer iGST) {
        this.iGST = iGST;
    }

    @JsonProperty("SGST")
    public Double getSGST() {
        return sGST;
    }

    @JsonProperty("SGST")
    public void setSGST(Double sGST) {
        this.sGST = sGST;
    }

    @JsonProperty("service_tax")
    public Integer getServiceTax() {
        return serviceTax;
    }

    @JsonProperty("service_tax")
    public void setServiceTax(Integer serviceTax) {
        this.serviceTax = serviceTax;
    }

    @JsonProperty("krishi_kalyan_cess")
    public Integer getKrishiKalyanCess() {
        return krishiKalyanCess;
    }

    @JsonProperty("krishi_kalyan_cess")
    public void setKrishiKalyanCess(Integer krishiKalyanCess) {
        this.krishiKalyanCess = krishiKalyanCess;
    }

    @JsonProperty("CGST")
    public Double getCGST() {
        return cGST;
    }

    @JsonProperty("CGST")
    public void setCGST(Double cGST) {
        this.cGST = cGST;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
