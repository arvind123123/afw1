
package com.shopwise.objects.order.delhivery.shippingcharge;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "md",
    "cgm",
    "o_pin",
    "ss",
    "d_pin"
})
public class ShippingChargeRequest {

    @JsonProperty("md")
    private String md;
    @JsonProperty("cgm")
    private String cgm;
    @JsonProperty("o_pin")
    private String oPin;
    @JsonProperty("ss")
    private String ss;
    @JsonProperty("d_pin")
    private String dPin;
    private String pt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("md")
    public String getMd() {
        return md;
    }

    @JsonProperty("md")
    public void setMd(String md) {
        this.md = md;
    }

    @JsonProperty("cgm")
    public String getCgm() {
        return cgm;
    }

    @JsonProperty("cgm")
    public void setCgm(String cgm) {
        this.cgm = cgm;
    }

    @JsonProperty("o_pin")
    public String getOPin() {
        return oPin;
    }

    @JsonProperty("o_pin")
    public void setOPin(String oPin) {
        this.oPin = oPin;
    }

    @JsonProperty("ss")
    public String getSs() {
        return ss;
    }

    @JsonProperty("ss")
    public void setSs(String ss) {
        this.ss = ss;
    }

    @JsonProperty("d_pin")
    public String getDPin() {
        return dPin;
    }

    @JsonProperty("d_pin")
    public void setDPin(String dPin) {
        this.dPin = dPin;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	public String getPt() {
		return pt;
	}

	public void setPt(String pt) {
		this.pt = pt;
	}
    
    

}
