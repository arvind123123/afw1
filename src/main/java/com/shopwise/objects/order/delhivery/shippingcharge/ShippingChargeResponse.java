
package com.shopwise.objects.order.delhivery.shippingcharge;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "charge_ROV",
    "charge_RTO",
    "charge_MPS",
    "charge_pickup",
    "tax_data",
    "charge_AWB",
    "zone",
    "wt_rule_id",
    "charge_FSC",
    "charge_COD",
    "charge_POD",
    "charge_CCOD",
    "gross_amount",
    "charge_DTO",
    "charge_FOV",
    "zonal_cl",
    "charge_DL",
    "total_amount",
    "charge_FOD",
    "charge_WOD",
    "charge_INS",
    "charge_FS",
    "charge_CNC",
    "charge_COVID",
    "charged_weight"
})
public class ShippingChargeResponse {

    @JsonProperty("charge_ROV")
    private Integer chargeROV;
    @JsonProperty("charge_RTO")
    private Integer chargeRTO;
    @JsonProperty("charge_MPS")
    private Integer chargeMPS;
    @JsonProperty("charge_pickup")
    private Integer chargePickup;
    @JsonProperty("tax_data")
    private TaxData taxData;
    @JsonProperty("charge_AWB")
    private Integer chargeAWB;
    @JsonProperty("zone")
    private String zone;
    @JsonProperty("wt_rule_id")
    private Object wtRuleId;
    @JsonProperty("charge_FSC")
    private Integer chargeFSC;
    @JsonProperty("charge_COD")
    private Integer chargeCOD;
    @JsonProperty("charge_POD")
    private Integer chargePOD;
    @JsonProperty("charge_CCOD")
    private Integer chargeCCOD;
    @JsonProperty("gross_amount")
    private Integer grossAmount;
    @JsonProperty("charge_DTO")
    private Integer chargeDTO;
    @JsonProperty("charge_FOV")
    private Integer chargeFOV;
    @JsonProperty("zonal_cl")
    private String zonalCl;
    @JsonProperty("charge_DL")
    private Integer chargeDL;
    @JsonProperty("total_amount")
    private Double totalAmount;
    @JsonProperty("charge_FOD")
    private Integer chargeFOD;
    @JsonProperty("charge_WOD")
    private Integer chargeWOD;
    @JsonProperty("charge_INS")
    private Integer chargeINS;
    @JsonProperty("charge_FS")
    private Integer chargeFS;
    @JsonProperty("charge_CNC")
    private Integer chargeCNC;
    @JsonProperty("charge_COVID")
    private Integer chargeCOVID;
    @JsonProperty("charged_weight")
    private Integer chargedWeight;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("charge_ROV")
    public Integer getChargeROV() {
        return chargeROV;
    }

    @JsonProperty("charge_ROV")
    public void setChargeROV(Integer chargeROV) {
        this.chargeROV = chargeROV;
    }

    @JsonProperty("charge_RTO")
    public Integer getChargeRTO() {
        return chargeRTO;
    }

    @JsonProperty("charge_RTO")
    public void setChargeRTO(Integer chargeRTO) {
        this.chargeRTO = chargeRTO;
    }

    @JsonProperty("charge_MPS")
    public Integer getChargeMPS() {
        return chargeMPS;
    }

    @JsonProperty("charge_MPS")
    public void setChargeMPS(Integer chargeMPS) {
        this.chargeMPS = chargeMPS;
    }

    @JsonProperty("charge_pickup")
    public Integer getChargePickup() {
        return chargePickup;
    }

    @JsonProperty("charge_pickup")
    public void setChargePickup(Integer chargePickup) {
        this.chargePickup = chargePickup;
    }

    @JsonProperty("tax_data")
    public TaxData getTaxData() {
        return taxData;
    }

    @JsonProperty("tax_data")
    public void setTaxData(TaxData taxData) {
        this.taxData = taxData;
    }

    @JsonProperty("charge_AWB")
    public Integer getChargeAWB() {
        return chargeAWB;
    }

    @JsonProperty("charge_AWB")
    public void setChargeAWB(Integer chargeAWB) {
        this.chargeAWB = chargeAWB;
    }

    @JsonProperty("zone")
    public String getZone() {
        return zone;
    }

    @JsonProperty("zone")
    public void setZone(String zone) {
        this.zone = zone;
    }

    @JsonProperty("wt_rule_id")
    public Object getWtRuleId() {
        return wtRuleId;
    }

    @JsonProperty("wt_rule_id")
    public void setWtRuleId(Object wtRuleId) {
        this.wtRuleId = wtRuleId;
    }

    @JsonProperty("charge_FSC")
    public Integer getChargeFSC() {
        return chargeFSC;
    }

    @JsonProperty("charge_FSC")
    public void setChargeFSC(Integer chargeFSC) {
        this.chargeFSC = chargeFSC;
    }

    @JsonProperty("charge_COD")
    public Integer getChargeCOD() {
        return chargeCOD;
    }

    @JsonProperty("charge_COD")
    public void setChargeCOD(Integer chargeCOD) {
        this.chargeCOD = chargeCOD;
    }

    @JsonProperty("charge_POD")
    public Integer getChargePOD() {
        return chargePOD;
    }

    @JsonProperty("charge_POD")
    public void setChargePOD(Integer chargePOD) {
        this.chargePOD = chargePOD;
    }

    @JsonProperty("charge_CCOD")
    public Integer getChargeCCOD() {
        return chargeCCOD;
    }

    @JsonProperty("charge_CCOD")
    public void setChargeCCOD(Integer chargeCCOD) {
        this.chargeCCOD = chargeCCOD;
    }

    @JsonProperty("gross_amount")
    public Integer getGrossAmount() {
        return grossAmount;
    }

    @JsonProperty("gross_amount")
    public void setGrossAmount(Integer grossAmount) {
        this.grossAmount = grossAmount;
    }

    @JsonProperty("charge_DTO")
    public Integer getChargeDTO() {
        return chargeDTO;
    }

    @JsonProperty("charge_DTO")
    public void setChargeDTO(Integer chargeDTO) {
        this.chargeDTO = chargeDTO;
    }

    @JsonProperty("charge_FOV")
    public Integer getChargeFOV() {
        return chargeFOV;
    }

    @JsonProperty("charge_FOV")
    public void setChargeFOV(Integer chargeFOV) {
        this.chargeFOV = chargeFOV;
    }

    @JsonProperty("zonal_cl")
    public String getZonalCl() {
        return zonalCl;
    }

    @JsonProperty("zonal_cl")
    public void setZonalCl(String zonalCl) {
        this.zonalCl = zonalCl;
    }

    @JsonProperty("charge_DL")
    public Integer getChargeDL() {
        return chargeDL;
    }

    @JsonProperty("charge_DL")
    public void setChargeDL(Integer chargeDL) {
        this.chargeDL = chargeDL;
    }

    @JsonProperty("total_amount")
    public Double getTotalAmount() {
        return totalAmount;
    }

    @JsonProperty("total_amount")
    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @JsonProperty("charge_FOD")
    public Integer getChargeFOD() {
        return chargeFOD;
    }

    @JsonProperty("charge_FOD")
    public void setChargeFOD(Integer chargeFOD) {
        this.chargeFOD = chargeFOD;
    }

    @JsonProperty("charge_WOD")
    public Integer getChargeWOD() {
        return chargeWOD;
    }

    @JsonProperty("charge_WOD")
    public void setChargeWOD(Integer chargeWOD) {
        this.chargeWOD = chargeWOD;
    }

    @JsonProperty("charge_INS")
    public Integer getChargeINS() {
        return chargeINS;
    }

    @JsonProperty("charge_INS")
    public void setChargeINS(Integer chargeINS) {
        this.chargeINS = chargeINS;
    }

    @JsonProperty("charge_FS")
    public Integer getChargeFS() {
        return chargeFS;
    }

    @JsonProperty("charge_FS")
    public void setChargeFS(Integer chargeFS) {
        this.chargeFS = chargeFS;
    }

    @JsonProperty("charge_CNC")
    public Integer getChargeCNC() {
        return chargeCNC;
    }

    @JsonProperty("charge_CNC")
    public void setChargeCNC(Integer chargeCNC) {
        this.chargeCNC = chargeCNC;
    }

    @JsonProperty("charge_COVID")
    public Integer getChargeCOVID() {
        return chargeCOVID;
    }

    @JsonProperty("charge_COVID")
    public void setChargeCOVID(Integer chargeCOVID) {
        this.chargeCOVID = chargeCOVID;
    }

    @JsonProperty("charged_weight")
    public Integer getChargedWeight() {
        return chargedWeight;
    }

    @JsonProperty("charged_weight")
    public void setChargedWeight(Integer chargedWeight) {
        this.chargedWeight = chargedWeight;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
