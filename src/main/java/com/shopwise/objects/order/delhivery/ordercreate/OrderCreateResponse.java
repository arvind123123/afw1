
package com.shopwise.objects.order.delhivery.ordercreate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cash_pickups_count",
    "package_count",
    "upload_wbn",
    "replacement_count",
    "rmk",
    "pickups_count",
    "packages",
    "cash_pickups",
    "cod_count",
    "success",
    "prepaid_count",
    "cod_amount"
})
public class OrderCreateResponse {

    @JsonProperty("cash_pickups_count")
    private Double cashPickupsCount;
    @JsonProperty("package_count")
    private Integer packageCount;
    @JsonProperty("upload_wbn")
    private String uploadWbn;
    @JsonProperty("replacement_count")
    private Integer replacementCount;
    @JsonProperty("rmk")
    private String rmk;
    @JsonProperty("pickups_count")
    private Integer pickupsCount;
    @JsonProperty("packages")
    private List<Package> packages = null;
    @JsonProperty("cash_pickups")
    private Double cashPickups;
    @JsonProperty("cod_count")
    private Integer codCount;
    @JsonProperty("success")
    private Boolean success;
    @JsonProperty("prepaid_count")
    private Integer prepaidCount;
    @JsonProperty("cod_amount")
    private Double codAmount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cash_pickups_count")
    public Double getCashPickupsCount() {
        return cashPickupsCount;
    }

    @JsonProperty("cash_pickups_count")
    public void setCashPickupsCount(Double cashPickupsCount) {
        this.cashPickupsCount = cashPickupsCount;
    }

    @JsonProperty("package_count")
    public Integer getPackageCount() {
        return packageCount;
    }

    @JsonProperty("package_count")
    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    @JsonProperty("upload_wbn")
    public String getUploadWbn() {
        return uploadWbn;
    }

    @JsonProperty("upload_wbn")
    public void setUploadWbn(String uploadWbn) {
        this.uploadWbn = uploadWbn;
    }

    @JsonProperty("replacement_count")
    public Integer getReplacementCount() {
        return replacementCount;
    }

    @JsonProperty("replacement_count")
    public void setReplacementCount(Integer replacementCount) {
        this.replacementCount = replacementCount;
    }

    @JsonProperty("rmk")
    public String getRmk() {
        return rmk;
    }

    @JsonProperty("rmk")
    public void setRmk(String rmk) {
        this.rmk = rmk;
    }

    @JsonProperty("pickups_count")
    public Integer getPickupsCount() {
        return pickupsCount;
    }

    @JsonProperty("pickups_count")
    public void setPickupsCount(Integer pickupsCount) {
        this.pickupsCount = pickupsCount;
    }

    @JsonProperty("packages")
    public List<Package> getPackages() {
        return packages;
    }

    @JsonProperty("packages")
    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

    @JsonProperty("cash_pickups")
    public Double getCashPickups() {
        return cashPickups;
    }

    @JsonProperty("cash_pickups")
    public void setCashPickups(Double cashPickups) {
        this.cashPickups = cashPickups;
    }

    @JsonProperty("cod_count")
    public Integer getCodCount() {
        return codCount;
    }

    @JsonProperty("cod_count")
    public void setCodCount(Integer codCount) {
        this.codCount = codCount;
    }

    @JsonProperty("success")
    public Boolean getSuccess() {
        return success;
    }

    @JsonProperty("success")
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    @JsonProperty("prepaid_count")
    public Integer getPrepaidCount() {
        return prepaidCount;
    }

    @JsonProperty("prepaid_count")
    public void setPrepaidCount(Integer prepaidCount) {
        this.prepaidCount = prepaidCount;
    }

    @JsonProperty("cod_amount")
    public Double getCodAmount() {
        return codAmount;
    }

    @JsonProperty("cod_amount")
    public void setCodAmount(Double codAmount) {
        this.codAmount = codAmount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
