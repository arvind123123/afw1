
package com.shopwise.objects.order.delhivery.ordercreate;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "return_name",
    "return_pin",
    "return_city",
    "return_phone",
    "return_add",
    "return_state",
    "return_country",
    "order",
    "phone",
    "products_desc",
    "cod_amount",
    "name",
    "country",
    "seller_inv_date",
    "order_date",
    "total_amount",
    "seller_add",
    "seller_cst",
    "add",
    "seller_name",
    "seller_inv",
    "seller_tin",
    "pin",
    "quantity",
    "payment_mode",
    "state",
    "city",
    "client"
})
public class Shipment {

    @JsonProperty("return_name")
    private String returnName;
    @JsonProperty("return_pin")
    private String returnPin;
    @JsonProperty("return_city")
    private String returnCity;
    @JsonProperty("return_phone")
    private String returnPhone;
    @JsonProperty("return_add")
    private String returnAdd;
    @JsonProperty("return_state")
    private String returnState;
    @JsonProperty("return_country")
    private String returnCountry;
    @JsonProperty("order")
    private String order;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("products_desc")
    private String productsDesc;
    @JsonProperty("cod_amount")
    private String codAmount;
    @JsonProperty("name")
    private String name;
    @JsonProperty("country")
    private String country;
    @JsonProperty("seller_inv_date")
    private String sellerInvDate;
    @JsonProperty("order_date")
    private String orderDate;
    @JsonProperty("total_amount")
    private String totalAmount;
    @JsonProperty("seller_add")
    private String sellerAdd;
    @JsonProperty("seller_cst")
    private String sellerCst;
    @JsonProperty("add")
    private String add;
    @JsonProperty("seller_name")
    private String sellerName;
    @JsonProperty("seller_inv")
    private String sellerInv;
    @JsonProperty("seller_tin")
    private String sellerTin;
    @JsonProperty("pin")
    private String pin;
    @JsonProperty("quantity")
    private String quantity;
    @JsonProperty("payment_mode")
    private String paymentMode;
    @JsonProperty("state")
    private String state;
    @JsonProperty("city")
    private String city;
    @JsonProperty("client")
    private String client;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("return_name")
    public String getReturnName() {
        return returnName;
    }

    @JsonProperty("return_name")
    public void setReturnName(String returnName) {
        this.returnName = returnName;
    }

    @JsonProperty("return_pin")
    public String getReturnPin() {
        return returnPin;
    }

    @JsonProperty("return_pin")
    public void setReturnPin(String returnPin) {
        this.returnPin = returnPin;
    }

    @JsonProperty("return_city")
    public String getReturnCity() {
        return returnCity;
    }

    @JsonProperty("return_city")
    public void setReturnCity(String returnCity) {
        this.returnCity = returnCity;
    }

    @JsonProperty("return_phone")
    public String getReturnPhone() {
        return returnPhone;
    }

    @JsonProperty("return_phone")
    public void setReturnPhone(String returnPhone) {
        this.returnPhone = returnPhone;
    }

    @JsonProperty("return_add")
    public String getReturnAdd() {
        return returnAdd;
    }

    @JsonProperty("return_add")
    public void setReturnAdd(String returnAdd) {
        this.returnAdd = returnAdd;
    }

    @JsonProperty("return_state")
    public String getReturnState() {
        return returnState;
    }

    @JsonProperty("return_state")
    public void setReturnState(String returnState) {
        this.returnState = returnState;
    }

    @JsonProperty("return_country")
    public String getReturnCountry() {
        return returnCountry;
    }

    @JsonProperty("return_country")
    public void setReturnCountry(String returnCountry) {
        this.returnCountry = returnCountry;
    }

    @JsonProperty("order")
    public String getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(String order) {
        this.order = order;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("products_desc")
    public String getProductsDesc() {
        return productsDesc;
    }

    @JsonProperty("products_desc")
    public void setProductsDesc(String productsDesc) {
        this.productsDesc = productsDesc;
    }

    @JsonProperty("cod_amount")
    public String getCodAmount() {
        return codAmount;
    }

    @JsonProperty("cod_amount")
    public void setCodAmount(String codAmount) {
        this.codAmount = codAmount;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("seller_inv_date")
    public String getSellerInvDate() {
        return sellerInvDate;
    }

    @JsonProperty("seller_inv_date")
    public void setSellerInvDate(String sellerInvDate) {
        this.sellerInvDate = sellerInvDate;
    }

    @JsonProperty("order_date")
    public String getOrderDate() {
        return orderDate;
    }

    @JsonProperty("order_date")
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    @JsonProperty("total_amount")
    public String getTotalAmount() {
        return totalAmount;
    }

    @JsonProperty("total_amount")
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    @JsonProperty("seller_add")
    public String getSellerAdd() {
        return sellerAdd;
    }

    @JsonProperty("seller_add")
    public void setSellerAdd(String sellerAdd) {
        this.sellerAdd = sellerAdd;
    }

    @JsonProperty("seller_cst")
    public String getSellerCst() {
        return sellerCst;
    }

    @JsonProperty("seller_cst")
    public void setSellerCst(String sellerCst) {
        this.sellerCst = sellerCst;
    }

    @JsonProperty("add")
    public String getAdd() {
        return add;
    }

    @JsonProperty("add")
    public void setAdd(String add) {
        this.add = add;
    }

    @JsonProperty("seller_name")
    public String getSellerName() {
        return sellerName;
    }

    @JsonProperty("seller_name")
    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    @JsonProperty("seller_inv")
    public String getSellerInv() {
        return sellerInv;
    }

    @JsonProperty("seller_inv")
    public void setSellerInv(String sellerInv) {
        this.sellerInv = sellerInv;
    }

    @JsonProperty("seller_tin")
    public String getSellerTin() {
        return sellerTin;
    }

    @JsonProperty("seller_tin")
    public void setSellerTin(String sellerTin) {
        this.sellerTin = sellerTin;
    }

    @JsonProperty("pin")
    public String getPin() {
        return pin;
    }

    @JsonProperty("pin")
    public void setPin(String pin) {
        this.pin = pin;
    }

    @JsonProperty("quantity")
    public String getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("payment_mode")
    public String getPaymentMode() {
        return paymentMode;
    }

    @JsonProperty("payment_mode")
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("client")
    public String getClient() {
        return client;
    }

    @JsonProperty("client")
    public void setClient(String client) {
        this.client = client;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
