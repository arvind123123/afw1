
package com.shopwise.objects.order.delhivery.ordercreate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "client",
    "sort_code",
    "remarks",
    "waybill",
    "cod_amount",
    "payment",
    "serviceable",
    "refnum"
})
public class Package {

    @JsonProperty("status")
    private String status;
    @JsonProperty("client")
    private String client;
    @JsonProperty("sort_code")
    private Object sortCode;
    @JsonProperty("remarks")
    private List<String> remarks = null;
    @JsonProperty("waybill")
    private String waybill;
    @JsonProperty("cod_amount")
    private Double codAmount;
    @JsonProperty("payment")
    private String payment;
    @JsonProperty("serviceable")
    private Boolean serviceable;
    @JsonProperty("refnum")
    private String refnum;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("client")
    public String getClient() {
        return client;
    }

    @JsonProperty("client")
    public void setClient(String client) {
        this.client = client;
    }

    @JsonProperty("sort_code")
    public Object getSortCode() {
        return sortCode;
    }

    @JsonProperty("sort_code")
    public void setSortCode(Object sortCode) {
        this.sortCode = sortCode;
    }

    @JsonProperty("remarks")
    public List<String> getRemarks() {
        return remarks;
    }

    @JsonProperty("remarks")
    public void setRemarks(List<String> remarks) {
        this.remarks = remarks;
    }

    @JsonProperty("waybill")
    public String getWaybill() {
        return waybill;
    }

    @JsonProperty("waybill")
    public void setWaybill(String waybill) {
        this.waybill = waybill;
    }

    @JsonProperty("cod_amount")
    public Double getCodAmount() {
        return codAmount;
    }

    @JsonProperty("cod_amount")
    public void setCodAmount(Double codAmount) {
        this.codAmount = codAmount;
    }

    @JsonProperty("payment")
    public String getPayment() {
        return payment;
    }

    @JsonProperty("payment")
    public void setPayment(String payment) {
        this.payment = payment;
    }

    @JsonProperty("serviceable")
    public Boolean getServiceable() {
        return serviceable;
    }

    @JsonProperty("serviceable")
    public void setServiceable(Boolean serviceable) {
        this.serviceable = serviceable;
    }

    @JsonProperty("refnum")
    public String getRefnum() {
        return refnum;
    }

    @JsonProperty("refnum")
    public void setRefnum(String refnum) {
        this.refnum = refnum;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
