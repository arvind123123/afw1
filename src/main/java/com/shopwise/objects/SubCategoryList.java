package com.shopwise.objects;

import java.util.List;

public class SubCategoryList {

	private String name;
	private int id;
	private String metaTitle;
	private String metaKeywords;
	private String metaDescription;
	
	
	
	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public String getMetaKeywords() {
		return metaKeywords;
	}

	public void setMetaKeywords(String metaKeywords) {
		this.metaKeywords = metaKeywords;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private List<SubSubCategoryList> subSubCatList;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SubSubCategoryList> getSubSubCatList() {
		return subSubCatList;
	}

	public void setSubSubCatList(List<SubSubCategoryList> subSubCatList) {
		this.subSubCatList = subSubCatList;
	}
	
	
}
