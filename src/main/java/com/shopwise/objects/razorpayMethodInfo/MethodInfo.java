package com.shopwise.objects.razorpayMethodInfo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"entity",
"name",
"last4",
"network",
"type",
"issuer",
"international",
"emi",
"sub_type"
})
public class MethodInfo {

@JsonProperty("id")
private String id;
@JsonProperty("entity")
private String entity;
@JsonProperty("name")
private String name;
@JsonProperty("last4")
private String last4;
@JsonProperty("network")
private String network;
@JsonProperty("type")
private String type;
@JsonProperty("issuer")
private String issuer;
@JsonProperty("international")
private Boolean international;
@JsonProperty("emi")
private Boolean emi;
@JsonProperty("sub_type")
private String subType;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("id")
public String getId() {
return id;
}

@JsonProperty("id")
public void setId(String id) {
this.id = id;
}

@JsonProperty("entity")
public String getEntity() {
return entity;
}

@JsonProperty("entity")
public void setEntity(String entity) {
this.entity = entity;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("last4")
public String getLast4() {
return last4;
}

@JsonProperty("last4")
public void setLast4(String last4) {
this.last4 = last4;
}

@JsonProperty("network")
public String getNetwork() {
return network;
}

@JsonProperty("network")
public void setNetwork(String network) {
this.network = network;
}

@JsonProperty("type")
public String getType() {
return type;
}

@JsonProperty("type")
public void setType(String type) {
this.type = type;
}

@JsonProperty("issuer")
public String getIssuer() {
return issuer;
}

@JsonProperty("issuer")
public void setIssuer(String issuer) {
this.issuer = issuer;
}

@JsonProperty("international")
public Boolean getInternational() {
return international;
}

@JsonProperty("international")
public void setInternational(Boolean international) {
this.international = international;
}

@JsonProperty("emi")
public Boolean getEmi() {
return emi;
}

@JsonProperty("emi")
public void setEmi(Boolean emi) {
this.emi = emi;
}

@JsonProperty("sub_type")
public String getSubType() {
return subType;
}

@JsonProperty("sub_type")
public void setSubType(String subType) {
this.subType = subType;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}