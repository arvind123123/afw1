
package com.shopwise.objects.razorpayPayment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "entity",
    "amount",
    "currency",
    "status",
    "order_id",
    "invoice_id",
    "international",
    "method",
    "amount_refunded",
    "refund_status",
    "captured",
    "description",
    "card_id",
    "bank",
    "wallet",
    "vpa",
    "email",
    "contact",
    "notes",
    "fee",
    "tax",
    "error_code",
    "error_description",
    "error_source",
    "error_step",
    "error_reason",
    "acquirer_data",
    "created_at"
})
public class RazorpayPaymentResponse {

    @JsonProperty("id")
    private String id;
    @JsonProperty("entity")
    private String entity;
    @JsonProperty("amount")
    private Double amount;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("status")
    private String status;
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("invoice_id")
    private Object invoiceId;
    @JsonProperty("international")
    private Boolean international;
    @JsonProperty("method")
    private String method;
    @JsonProperty("amount_refunded")
    private Integer amountRefunded;
    @JsonProperty("refund_status")
    private Object refundStatus;
    @JsonProperty("captured")
    private Boolean captured;
    @JsonProperty("description")
    private String description;
    @JsonProperty("card_id")
    private String cardId;
    @JsonProperty("bank")
    private String bank;
    @JsonProperty("wallet")
    private String wallet;
    @JsonProperty("vpa")
    private String vpa;
    @JsonProperty("email")
    private String email;
    @JsonProperty("contact")
    private String contact;
    @JsonProperty("notes")
    private List<Object> notes = null;
    @JsonProperty("fee")
    private Integer fee;
    @JsonProperty("tax")
    private Integer tax;
    @JsonProperty("error_code")
    private Object errorCode;
    @JsonProperty("error_description")
    private Object errorDescription;
    @JsonProperty("error_source")
    private Object errorSource;
    @JsonProperty("error_step")
    private Object errorStep;
    @JsonProperty("error_reason")
    private Object errorReason;
    @JsonProperty("acquirer_data")
    private AcquirerData acquirerData;
    @JsonProperty("created_at")
    private Integer createdAt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("entity")
    public String getEntity() {
        return entity;
    }

    @JsonProperty("entity")
    public void setEntity(String entity) {
        this.entity = entity;
    }

    @JsonProperty("amount")
    public Double getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("invoice_id")
    public Object getInvoiceId() {
        return invoiceId;
    }

    @JsonProperty("invoice_id")
    public void setInvoiceId(Object invoiceId) {
        this.invoiceId = invoiceId;
    }

    @JsonProperty("international")
    public Boolean getInternational() {
        return international;
    }

    @JsonProperty("international")
    public void setInternational(Boolean international) {
        this.international = international;
    }

    @JsonProperty("method")
    public String getMethod() {
        return method;
    }

    @JsonProperty("method")
    public void setMethod(String method) {
        this.method = method;
    }

    @JsonProperty("amount_refunded")
    public Integer getAmountRefunded() {
        return amountRefunded;
    }

    @JsonProperty("amount_refunded")
    public void setAmountRefunded(Integer amountRefunded) {
        this.amountRefunded = amountRefunded;
    }

    @JsonProperty("refund_status")
    public Object getRefundStatus() {
        return refundStatus;
    }

    @JsonProperty("refund_status")
    public void setRefundStatus(Object refundStatus) {
        this.refundStatus = refundStatus;
    }

    @JsonProperty("captured")
    public Boolean getCaptured() {
        return captured;
    }

    @JsonProperty("captured")
    public void setCaptured(Boolean captured) {
        this.captured = captured;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("card_id")
    public String getCardId() {
        return cardId;
    }

    @JsonProperty("card_id")
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    @JsonProperty("bank")
    public String getBank() {
        return bank;
    }

    @JsonProperty("bank")
    public void setBank(String bank) {
        this.bank = bank;
    }

    @JsonProperty("wallet")
    public String getWallet() {
        return wallet;
    }

    @JsonProperty("wallet")
    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    @JsonProperty("vpa")
    public String getVpa() {
        return vpa;
    }

    @JsonProperty("vpa")
    public void setVpa(String vpa) {
        this.vpa = vpa;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("contact")
    public String getContact() {
        return contact;
    }

    @JsonProperty("contact")
    public void setContact(String contact) {
        this.contact = contact;
    }

    @JsonProperty("notes")
    public List<Object> getNotes() {
        return notes;
    }

    @JsonProperty("notes")
    public void setNotes(List<Object> notes) {
        this.notes = notes;
    }

    @JsonProperty("fee")
    public Integer getFee() {
        return fee;
    }

    @JsonProperty("fee")
    public void setFee(Integer fee) {
        this.fee = fee;
    }

    @JsonProperty("tax")
    public Integer getTax() {
        return tax;
    }

    @JsonProperty("tax")
    public void setTax(Integer tax) {
        this.tax = tax;
    }

    @JsonProperty("error_code")
    public Object getErrorCode() {
        return errorCode;
    }

    @JsonProperty("error_code")
    public void setErrorCode(Object errorCode) {
        this.errorCode = errorCode;
    }

    @JsonProperty("error_description")
    public Object getErrorDescription() {
        return errorDescription;
    }

    @JsonProperty("error_description")
    public void setErrorDescription(Object errorDescription) {
        this.errorDescription = errorDescription;
    }

    @JsonProperty("error_source")
    public Object getErrorSource() {
        return errorSource;
    }

    @JsonProperty("error_source")
    public void setErrorSource(Object errorSource) {
        this.errorSource = errorSource;
    }

    @JsonProperty("error_step")
    public Object getErrorStep() {
        return errorStep;
    }

    @JsonProperty("error_step")
    public void setErrorStep(Object errorStep) {
        this.errorStep = errorStep;
    }

    @JsonProperty("error_reason")
    public Object getErrorReason() {
        return errorReason;
    }

    @JsonProperty("error_reason")
    public void setErrorReason(Object errorReason) {
        this.errorReason = errorReason;
    }

    @JsonProperty("acquirer_data")
    public AcquirerData getAcquirerData() {
        return acquirerData;
    }

    @JsonProperty("acquirer_data")
    public void setAcquirerData(AcquirerData acquirerData) {
        this.acquirerData = acquirerData;
    }

    @JsonProperty("created_at")
    public Integer getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
