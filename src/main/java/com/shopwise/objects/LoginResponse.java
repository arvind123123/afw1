package com.shopwise.objects;

import java.util.List;


import com.shopwise.objects.order.BillingAddress;
import com.shopwise.objects.order.ShippingAddress;



public class LoginResponse {
	
 private ResponseStatus responseStatus;
 private int customerId;
 private String name;
 private String emailId;
 private int productId;
 private List<Products> productList;
 private List<BillingAddress> billingAddress;
 private List<ShippingAddress> shippingAddress;
 
public List<BillingAddress> getBillingAddress() {
	return billingAddress;
}
public void setBillingAddress(List<BillingAddress> billingAddress) {
	this.billingAddress = billingAddress;
}
public List<Products> getProductList() {
	return productList;
}
public void setProductList(List<Products> productList) {
	this.productList = productList;
}
public ResponseStatus getResponseStatus() {
	return responseStatus;
}
public void setResponseStatus(ResponseStatus responseStatus) {
	this.responseStatus = responseStatus;
}
public int getCustomerId() {
	return customerId;
}
public void setCustomerId(int customerId) {
	this.customerId = customerId;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public int getProductId() {
	return productId;
}
public void setProductId(int productId) {
	this.productId = productId;
}
public List<ShippingAddress> getShippingAddress() {
	return shippingAddress;
}
public void setShippingAddress(List<ShippingAddress> shippingAddress) {
	this.shippingAddress = shippingAddress;
}
 
 
 
}
