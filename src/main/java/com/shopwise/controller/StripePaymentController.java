package com.shopwise.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stripe.model.Coupon;
import com.shopwise.bean.CheckoutForm;
import com.shopwise.utility.*;



@Controller
public class StripePaymentController {

	@Value("${stripe.key.public}")
	private String API_PUBLIC_KEY;

	private com.shopwise.service.impl.StripeService stripeService;

	public StripePaymentController(com.shopwise.service.impl.StripeService stripeService) {
		this.stripeService = stripeService;
	}

	@GetMapping("/charge")
	public String chargePage(Model model,HttpServletRequest request) {
		CheckoutForm checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
		double cartTotal = 0;
		for (int i = 0; i < checkoutForm.getProductList().size(); i++) {
			cartTotal = (cartTotal + (checkoutForm.getProductList().get(i).getPrice()
					* checkoutForm.getProductList().get(i).getQuantity()));
		}
		model.addAttribute("stripePublicKey", API_PUBLIC_KEY);
		model.addAttribute("orderNum", checkoutForm.getOrderNum());
		model.addAttribute("amount", cartTotal);
		return "charge";
	}

	@PostMapping("/coupon-validator")
	public @ResponseBody StripeResponse couponValidator(String code) {

		Coupon coupon = stripeService.retriveCoupon(code);

		if (coupon != null && coupon.getValid()) {
			String details = (coupon.getPercentOff() == null ? "$" + (coupon.getAmountOff() / 100)
					: coupon.getPercentOff() + "%") + "OFF" + coupon.getDuration();
			return new StripeResponse(true, details);
		}
		return new StripeResponse(false, "This coupon code is not available. This may be because it has expired or has "
				+ "already been applied to your account.");
	}

	@PostMapping("/create-charge")
	public @ResponseBody StripeResponse createCharge(String email, String token,HttpServletRequest request) {
		
		CheckoutForm checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
		double cartTotal = 0;
		for (int i = 0; i < checkoutForm.getProductList().size(); i++) {
			cartTotal = (cartTotal + (checkoutForm.getProductList().get(i).getPrice()
					* checkoutForm.getProductList().get(i).getQuantity()));
		}

		if (token == null) {
			return new StripeResponse(false, "Stripe payment token is missing. please try again later.");
		}

		String chargeId = stripeService.createCharge(email, token, cartTotal);// 9.99 usd

		if (chargeId == null) {
			return new StripeResponse(false, "An error accurred while trying to charge.");
		}

		// You may want to store charge id along with order information

		return new StripeResponse(true, "Success your charge id is " + chargeId);
	}
}
