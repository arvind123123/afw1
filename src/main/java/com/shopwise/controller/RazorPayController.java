package com.shopwise.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shopwise.bean.CheckoutForm;
import com.shopwise.objects.order.PaymentStatusUpdate;
import com.shopwise.objects.razorpay.RazorpayPaymentRes;
import com.shopwise.objects.razorpay.RazorpayResponse;
import com.shopwise.objects.razorpay.Signature;
import com.shopwise.objects.razorpayMethodInfo.MethodInfo;
import com.shopwise.objects.razorpayPayment.RazorpayPaymentResponse;
import com.shopwise.service.HomeService;

@Controller
public class RazorPayController {

	@Autowired
	HomeService homeSevice;

	@GetMapping("/razorpay")
	public String orderCreateds(HttpServletRequest request, Model model) {
		// rzp_test_tsn5jLZB33NO5m,qT1NvkDms29j6mfmhqbqNCYo

		RestTemplate restTemplate = getRestTemplate("rzp_test_20Y6DRtftYvwHA", "i24FoxnhtJi25pduIaW3AlHs"); // test
		// RestTemplate restTemplate =
		// rajorPayController.getRestTemplate("rzp_live_XXUxxrZQk1ZJAX",
		// "AqpyVjnaKlOTnozIP8QIoCSC");//Live
		// Create a list for the message converters
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		// Add the String Message converter
		messageConverters.add(new StringHttpMessageConverter());
		// Add the message converters to the restTemplate
		restTemplate.setMessageConverters(messageConverters);
		CheckoutForm checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
		int cartTotal = 0;
		for (int i = 0; i < checkoutForm.getProductList().size(); i++) {
			cartTotal = (int) (cartTotal + (checkoutForm.getProductList().get(i).getPrice()
					* checkoutForm.getProductList().get(i).getQuantity()));
		}
		String xmlString = "{\"amount\":" + cartTotal + ",\"currency\":\"USD\",\"receipt\":\""
				+ checkoutForm.getOrderNum() + "\"}";
		
		model.addAttribute("amt", cartTotal);
		model.addAttribute("contact", checkoutForm.getBillingaddress().getContactNum());
		model.addAttribute("description",checkoutForm.getOrderNum());
		model.addAttribute("billingName",checkoutForm.getBillingaddress().getFirstName());
		model.addAttribute("email",checkoutForm.getBillingaddress().getEmail());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("User-Agent", "Firefox/1.0");
		headers.set("Accept-Encoding", "gzip, deflate");
		// headers.set("Content-Type", "text/xml");
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));

		HttpEntity<String> requests = new HttpEntity<String>(xmlString, headers);

		ResponseEntity<String> response = null;
		try {
			response = restTemplate.postForEntity("https://api.razorpay.com/v1/orders", requests, String.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println(response.getStatusCode());
		System.out.println("Rajorpay Response:" + response.getBody());
		RazorpayResponse payResponse = new RazorpayResponse();
		ObjectMapper mapper = new ObjectMapper();
		try {
			payResponse = mapper.readValue(response.getBody(), RazorpayResponse.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		request.getSession().setAttribute("Razorpay", payResponse);
		return "/Razorpay";
	}

	public static RestTemplate getRestTemplate(String userName, String Password) {
		RestTemplate restTemplate = new RestTemplate();
		/*
		 * HttpHost proxy =null; RequestConfig config=null; String credentials =
		 * userName + ":" + Password; String encodedAuthorization =
		 * Base64.getEncoder().encodeToString(credentials.getBytes());
		 * 
		 * Header header = new BasicHeader(HttpHeaders.AUTHORIZATION, "Basic " +
		 * encodedAuthorization); List<Header> headers = new ArrayList<>();
		 * headers.add(header); // if we need proxy config=
		 * RequestConfig.custom().build(); CloseableHttpClient httpClient =
		 * HttpClientBuilder.create().setDefaultRequestConfig(config)
		 * .setDefaultHeaders(headers).build();
		 * 
		 * HttpComponentsClientHttpRequestFactory factory = new
		 * HttpComponentsClientHttpRequestFactory(httpClient);
		 * restTemplate.setRequestFactory(factory);
		 */
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName, Password));

		return restTemplate;
	}

	@PostMapping(value = "/payment/success", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String charges(@RequestBody RazorpayPaymentRes razorpayPaymentRes) {
		System.out.println(razorpayPaymentRes.getRazorpayOrderId());
		System.out.println(razorpayPaymentRes.getRazorpayPaymentId());
		System.out.println(razorpayPaymentRes.getRazorpaySignature());
		return "NO";
	}

	@PostMapping(value = "/cart/order/complete", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String charges(@RequestBody MultiValueMap<String, String> formParams, HttpServletRequest request,
			Model model) {
		String paymentId = formParams.getFirst("razorpay_payment_id");
		String razorpaySignature = formParams.getFirst("razorpay_signature");
		String orderId = formParams.getFirst("razorpay_order_id");
		JSONObject options = new JSONObject();
		System.out.println(paymentId);
		System.out.println(razorpaySignature);
		System.out.println(orderId);

		if (paymentId != "" && razorpaySignature != "" && orderId != "") {
			RazorpayResponse payResponse = (RazorpayResponse) request.getSession().getAttribute("Razorpay");
			try {
				options.put("razorpay_payment_id", paymentId);
				options.put("razorpay_order_id", orderId);
				options.put("razorpay_signature", razorpaySignature);
				String data = payResponse.getId() + "|" + paymentId;
				String key = Signature.calculateRFC2104HMAC(data, "i24FoxnhtJi25pduIaW3AlHs");
				System.out.println(key);
				CheckoutForm checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
				model.addAttribute("checkoutForm", checkoutForm);
				model.addAttribute("orderNum", checkoutForm.getOrderNum());
				if (key.equals(razorpaySignature)) {
					System.out.println("success");
					RestTemplate restTemplate = getRestTemplate("rzp_test_20Y6DRtftYvwHA", "i24FoxnhtJi25pduIaW3AlHs"); /// test
					// RestTemplate restTemplate =
					// rajorPayController.getRestTemplate("rzp_live_XXUxxrZQk1ZJAX",
					// "AqpyVjnaKlOTnozIP8QIoCSC"); // Live
					// Create a list for the message converters
					List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
					// Add the String Message converter
					messageConverters.add(new StringHttpMessageConverter());
					// Add the message converters to the restTemplate
					restTemplate.setMessageConverters(messageConverters);

					ResponseEntity<String> response = restTemplate
							.getForEntity("https://api.razorpay.com/v1/payments/" + paymentId, String.class);
					ResponseEntity<String> cardResponse = restTemplate
							.getForEntity("https://api.razorpay.com/v1/payments/" + paymentId + "/card", String.class);
					System.out.println(response.getStatusCode());
					System.out.println("Rajorpay payment Response:" + response.getBody());
					System.out.println("Rajorpay cardResponse Response:" + cardResponse);
					RazorpayPaymentResponse paymentResponse = null;
					MethodInfo cardRespose = null;
					ObjectMapper mapper = new ObjectMapper();
					try {
						paymentResponse = mapper.readValue(response.getBody(), RazorpayPaymentResponse.class);
						// cardRespose = mapper.readValue(cardRespose.getbo, MethodInfo.class);
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					// System.out.println("Method:"+ paymentResponse.getMethod());
					PaymentStatusUpdate paymentStatus = new PaymentStatusUpdate();
					paymentStatus.setOrderNum(checkoutForm.getOrderNum());
					paymentStatus.setOrderStatus("Success");
					homeSevice.updatePaymentStatus(paymentStatus);
					return "order-complete";
				} else {
					System.out.println("fail");
					PaymentStatusUpdate paymentStatus = new PaymentStatusUpdate();
					paymentStatus.setOrderNum(checkoutForm.getOrderNum());
					paymentStatus.setOrderStatus("Failed");
					homeSevice.updatePaymentStatus(paymentStatus);
					return "order-complete";
				}
			} catch (Exception e) {
				System.out.println("Exception caused because of " + e.getMessage());
				return "order-complete";
			}
		}
		return "order-complete";
	}
}
