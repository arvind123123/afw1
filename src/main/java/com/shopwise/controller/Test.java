package com.shopwise.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shopwise.objects.order.delhivery.ordercreate.OrderCreate;
import com.shopwise.objects.order.delhivery.ordercreate.PickupLocation;
import com.shopwise.objects.order.delhivery.ordercreate.Shipment;
import com.shopwise.objects.order.delhivery.shippingcharge.ShippingChargeRequest;

@Controller
public class Test {

	public static void main(String[] args) throws URISyntaxException {
		
		/*HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Token e9ee49bc550f148030f8207063b11b382a282894");
		OrderCreate orderCreate = new OrderCreate();
		PickupLocation pickup = new PickupLocation();
		pickup.setAdd("B 44, Bhabha Marg, B Block, Sector 59, Noida, Uttar Pradesh 201301");
		pickup.setCity("Noida");
		pickup.setCountry("India");
		pickup.setName("SNVAVENTURESSURFACE-B2C");
		pickup.setPhone("9555151333");
		pickup.setPin("201301");
		pickup.setState("Uttar Pradesh");
		List<Shipment> shipList = new ArrayList<Shipment>();
		Shipment ship = new Shipment();
		ship.setAdd("karol bagh");
		ship.setCity("Delhi");
		ship.setClient("SNVAVENTURESSURFACE-B2C");
		ship.setCodAmount("0");
		ship.setCountry("India");
		ship.setName("Prince");
		ship.setOrder("1245631");
		ship.setOrderDate("2020-11-25");
		ship.setPaymentMode("Prepaid");
		ship.setPhone("9717143334");
		ship.setPin("110074");
		ship.setProductsDesc("Shoes");
		ship.setReturnAdd("B 44, Bhabha Marg, B Block, Sector 59, Noida, Uttar Pradesh 201301");
		ship.setReturnCity("Noida");
		ship.setReturnCountry("India");
		ship.setReturnName("SNVAVENTURES SURFACE");
		ship.setReturnPhone("9555151333");
		ship.setReturnPin("201301");
		ship.setReturnState("Uttar Pradesh");
		ship.setSellerAdd("");
		ship.setSellerCst("");
		ship.setSellerInv("");
		ship.setSellerInvDate("");
		ship.setSellerName("SNVA");
		ship.setSellerTin("");
		ship.setQuantity("1");
		ship.setState("Delhi");
		ship.setTotalAmount("10");
		shipList.add(ship);
		orderCreate.setPickupLocation(pickup);
		orderCreate.setShipments(shipList);
		ObjectMapper mapper = new ObjectMapper();
		String body = "";
		try {
		body = 	mapper.writeValueAsString(orderCreate);
		System.out.println(body);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpEntity<String> entity = new HttpEntity<String>("format=json&data="+body,headers);
		
		RestTemplate rest = new RestTemplate();
		
		String res = rest.postForObject("https://staging-express.delhivery.com/api/cmu/create.json", entity, String.class);
		System.out.println(res);*/
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Token e9ee49bc550f148030f8207063b11b382a282894");
		ShippingChargeRequest shiping = new ShippingChargeRequest();
		shiping.setCgm("10");
		shiping.setDPin("110074");
		shiping.setMd("S");
		shiping.setOPin("201301");
		shiping.setSs("Delivered");
		ObjectMapper mapper = new ObjectMapper();
		String body = "";
		try {
		body = 	mapper.writeValueAsString(shiping);
		System.out.println(body);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpEntity<String> entity = new HttpEntity<String>(body,headers);
		
		RestTemplate rest = new RestTemplate();
		
		//String url = "https://track.delhivery.com/api/kinko/v1/invoice/charges?querystring="+body+"headers = {\"authorization\": \"Token ebf63398e5d760414088b1920d272d3cd1b380f1\",\"accept\": \"application/json\",\"content-type\": \"application/json\"}";
		String url = "https://staging-express.delhivery.com/api/kinko/v1/invoice/charges";
		System.out.println(url);
		URI uri = new URI(url);
		//String res = rest.getForObject("https://staging-express.delhivery.com/api/kinko/v1/invoice/charges/.json/"+entity.toString(),  String.class);
		//String res = rest.getForObject(url, String.class,entity);
		 rest.exchange(uri, HttpMethod.GET, entity, String.class);
		//System.out.println(res);

	}
	
	
}
