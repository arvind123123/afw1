package com.shopwise.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.shopwise.bean.CheckoutForm;
import com.shopwise.object.paypal.PaypalService;
import com.shopwise.objects.EmailSend;
import com.shopwise.objects.order.OrderDetails;
import com.shopwise.objects.order.PaymentStatusUpdate;
import com.shopwise.objects.order.ShippingAddress;
import com.shopwise.service.HomeService;

@Controller
public class PaypalController {

	@Autowired
	PaypalService service;
	
	@Autowired
	HomeService homeService;
	
	@Autowired
	MailController mailController;

	public static final String SUCCESS_URL = "pay/success";
	public static final String CANCEL_URL = "pay/cancel";

//	@GetMapping("/")
//	public String home() {
//		return "home";
//	}

	@GetMapping("/paypal")

	public String payment(CheckoutForm checkoutForm, HttpServletRequest request) {
		checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
		double cartTotal = 0;
		for (int i = 0; i < checkoutForm.getProductList().size(); i++) {
			cartTotal = (cartTotal + (checkoutForm.getProductList().get(i).getPrice()
					* checkoutForm.getProductList().get(i).getQuantity()));
		}
		try {

			Payment payment = service.createPayment(cartTotal, "AUD", checkoutForm.getPaymentDetail().getPaymentMode(),
					"Sale", "Sale", "http://localhost:8098/" + CANCEL_URL,
					"http://localhost:8098/"+ SUCCESS_URL);
			for (Links link : payment.getLinks()) {
				if (link.getRel().equals("approval_url")) {
					return "redirect:" + link.getHref();
				}
			}

		} catch (PayPalRESTException e) {

			e.printStackTrace();
		}
		return "redirect:/";
	}

	@GetMapping(value = CANCEL_URL)
	public String cancelPay(HttpServletRequest request,Model model) {
		CheckoutForm checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
		PaymentStatusUpdate paymentStatus = new PaymentStatusUpdate();
		paymentStatus.setOrderNum(checkoutForm.getOrderNum());
		paymentStatus.setOrderStatus("Failed");
		homeService.updatePaymentStatus(paymentStatus);
		model.addAttribute("orderNum", checkoutForm.getOrderNum());
		return "order-cancel";
	}

	@GetMapping(value = SUCCESS_URL)
	public String successPay(@RequestParam("paymentId") String paymentId, @RequestParam("PayerID") String payerId,HttpServletRequest request,Model model) {
		try {
			Payment payment = service.executePayment(paymentId, payerId);
			CheckoutForm checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
			System.out.println(payment.toJSON());
			if (payment.getState().equals("approved")) {
				request.getSession().setAttribute("cartList", null);
				PaymentStatusUpdate paymentStatus = new PaymentStatusUpdate();
				paymentStatus.setOrderNum(checkoutForm.getOrderNum());
				paymentStatus.setOrderStatus("Success");
				model.addAttribute("orderNum", checkoutForm.getOrderNum());
				homeService.updatePaymentStatus(paymentStatus);
				request.getSession().setAttribute("cartList", null);
				return "order-complete";
			}
		} catch (PayPalRESTException e) {
			System.out.println(e.getMessage());
		}
		return "redirect:/";
	}

}
