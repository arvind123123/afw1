package com.shopwise.controller;

import java.io.Console;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shopwise.bean.AddToCartForm;
import com.shopwise.bean.CheckoutForm;
import com.shopwise.bean.CouponForm;
import com.shopwise.bean.ReviewForm;
import com.shopwise.bean.UserForm;
import com.shopwise.objects.Categories;
import com.shopwise.objects.CategoryList;
import com.shopwise.objects.CouponResponse;
import com.shopwise.objects.EmailSend;
import com.shopwise.objects.LoginResponse;
import com.shopwise.objects.ProductDetail;
import com.shopwise.objects.Products;
import com.shopwise.objects.SubCategoryList;
import com.shopwise.objects.WishlistRequest;
import com.shopwise.objects.order.OrderDetails;
import com.shopwise.objects.order.ShippingAddress;
import com.shopwise.objects.order.delhivery.shippingcharge.ShippingChargeRequest;
import com.shopwise.objects.order.delhivery.shippingcharge.ShippingChargeResponse;
import com.shopwise.service.HomeService;
import com.shopwise.utility.StripeUtility;

@Controller

public class HomeController {

	@Autowired
	HomeService homeService;

	@Value("${siteId}")
	private int siteId;

	@Value("${currency}")
	private String currency;

	@Value("${delhiveryAPI}")
	private String delhiveryAPI;

	@Value("${delhiveryToken}")
	private String delhiveryToken;

	@Autowired
	MailController mailController;
	
//	@GetMapping("/")
//	public String home(Model model, HttpServletRequest request) {
//		model.addAttribute("categories", homeService.getCategories(siteId));
//		model.addAttribute("exclusiveProducts",homeService.getExclusiveProducts(siteId));
//		model.addAttribute("featuredProducts", homeService.getFeaturedProducts(siteId));
//		request.getSession().setAttribute("currency", currency);
//		return "index";
//	}

	@GetMapping("/")
	public String home(Model model, HttpServletRequest request) {
		Products[] exclusiveProducts =  homeService.getExclusiveProducts(siteId);
		Products[] featureProducts = homeService.getFeaturedProducts(siteId);
		Categories[] categoriesMain = homeService.getCategories(siteId);
		SubCategoryList[] subCategoryList = homeService.getSubcategory(siteId);
		
		model.addAttribute("categories", categoriesMain);
		model.addAttribute("exclusiveProducts", exclusiveProducts);
		model.addAttribute("featuredProducts", featureProducts);
		request.getSession().setAttribute("currency", currency);
		
		Map<Integer,String> exclusiveMap = new HashMap<Integer, String>();
		Map<Integer, String> featuredMap = new HashMap<Integer, String>();
		Map<Integer,String> categoriesMap = new HashMap<Integer, String>();
		Map<Integer, String> subcatMap = new HashMap<Integer, String>();
		
		for (SubCategoryList subCategoryList2 : subCategoryList) {
			subcatMap.put(subCategoryList2.getId(), getCleanSEOURL(subCategoryList2.getName())); 
		}
		
		for (Products products : featureProducts) {
			featuredMap.put(products.getProductId(), getCleanSEOURL(products.getName()));
		}
		for (Products products : exclusiveProducts) {
			exclusiveMap.put(products.getProductId(), getCleanSEOURL(products.getName()));
		}
		for (Categories categories : categoriesMain) {
			categoriesMap.put(categories.getId(), getCleanSEOURL(categories.getName()));
		}
		
		model.addAttribute("subcatMap", subcatMap);
		model.addAttribute("exclusiveUrlMap",exclusiveMap);
		model.addAttribute("featuredUrlMap", featuredMap);
		model.addAttribute("categoriesMap", categoriesMap);
		return "index";

	}
	
	public String getCleanSEOURL(String name)                           
    {
 	   String greatName = name.toLowerCase();
// 	   String[] slipt = ;
 	   return greatName.split("-")[0].trim().replace(" ","-");
    }

	@GetMapping("/index-demo")
	public String homeDEmo(Model model, HttpServletRequest request) {
		model.addAttribute("categories", homeService.getCategories(siteId));
		model.addAttribute("exclusiveProducts", homeService.getExclusiveProducts(siteId));
		model.addAttribute("featuredProducts", homeService.getFeaturedProducts(siteId));
		request.getSession().setAttribute("currency", currency);
		return "index-demo";

	}
	
	
	//Dheeraj Started 
	
//	@GetMapping("/catProd/{catId}")
//	public String categoryProd(Model model,  @PathVariable("catId") int catId,HttpServletRequest request) {
//		model.addAttribute("products",homeService.getCatProducts(catId);
//		model.addAttribute("categories", homeService.getSubCatProducts(siteId));
//		model.addAttribute("exclusiveProducts", homeService.getExclusiveProducts(siteId));
//		model.addAttribute("featuredProducts", homeService.getFeaturedProducts(siteId));
//		request.getSession().setAttribute("currency", currency);
//		return "cat-subcat-products";
//	}
	
	
	
	@GetMapping("{catName}")
	public String categoryProd(Model model,@PathVariable("catName") String catName,HttpServletRequest request) {
		model.addAttribute("catName", catName);
		String finalName = catName.replace("-", " ");
		model.addAttribute("breadCrumCatName", finalName);
		Products[] categoryProducts = homeService.getCatProducts(finalName);
		if (categoryProducts != null) {
			Categories[] categories = homeService.getCategories(siteId);
			SubCategoryList[] subCategoryList = homeService.getSubcategory(siteId);
			
			model.addAttribute("products", categoryProducts);
			model.addAttribute("categories", categories);
			request.getSession().setAttribute("currency", currency);
			
			Map<Integer, String> categoryMap = new HashMap<Integer, String>();
			Map<Integer, String> subCatMap = new HashMap<Integer, String>();
			Map<String, String> metaTittleMap = new HashMap<String, String>();
			Map<String, String> metaDescriptionMap = new HashMap<String, String>();
			Map<String, String> metaKeywordMap = new HashMap<String, String>();
	 		
			for (SubCategoryList subCategoryList2 : subCategoryList) {
				subCatMap.put(subCategoryList2.getId(), getCleanSEOURL(subCategoryList2.getName()));
			}
			
			for (Products products : categoryProducts) {
				categoryMap.put(products.getProductId(),getCleanSEOURL(products.getName()));
			}
			String metaTittle,metaDescription,metaKeyword;
			for (Categories categoriesMeta : categories) {
				metaTittle = categoriesMeta.getName().toLowerCase();
				metaDescription = categoriesMeta.getName().toLowerCase();
				metaKeyword = categoriesMeta.getName().toLowerCase();
				
				metaTittleMap.put(metaTittle, categoriesMeta.getMetaTitle());
				metaDescriptionMap.put(metaDescription, categoriesMeta.getMetaDescription());
				metaKeywordMap.put(metaKeyword, categoriesMeta.getMetaKeywords());
			}
			
			
			model.addAttribute("categoryMap", categoryMap);
			model.addAttribute("subCatMap", subCatMap);
			model.addAttribute("metaTittle", metaTittleMap.get(finalName));
			model.addAttribute("metaDescription", metaDescriptionMap.get(finalName));
			model.addAttribute("metaKeywords", metaKeywordMap.get(finalName));
			
			return "cat-products";
		}
		else {
			String name = finalName+" - 12 Bottles";
			String productName = name.toLowerCase();
			System.out.println(productName);
			ProductDetail productDetails = homeService.getProductDetails(productName);

				model.addAttribute("productDetail", productDetails);
				
				List<Products> products = productDetails.getSimilarProducts();
				Map<Integer, String> smilarProdMap = new HashMap<Integer, String>();
				
				for (Products products2 : products) {
					smilarProdMap.put(products2.getProductId(), getCleanSEOURL(products2.getName()));
				}
				model.addAttribute("smimilarProductMap", smilarProdMap);
//				model.addAttribute("productName", getCleanSEOURL(productDetails.getName()));
				
				return "product-details";
		}
	
	}
	

	@GetMapping("/{catName}/{subCatName}")
	public String subCategoryProd(Model model,@PathVariable("catName")String catName,@PathVariable("subCatName") String subCatName, HttpServletRequest request) {
		String finalName = subCatName.replace("-", " ");
		System.out.println(finalName);
		Products[] subCatProducts = homeService.getSubCatProducts(finalName);
		CategoryList[] categoryList = homeService.getAllCategories(siteId);
		SubCategoryList[] subCategoryList = homeService.getSubcategory(siteId);
		
		model.addAttribute("products", subCatProducts);
		model.addAttribute("categories", categoryList);
		request.getSession().setAttribute("currency", currency);
		
		Map<Integer, String> cat = new HashMap<Integer, String>();
		Map<Integer,String> subCatMap = new HashMap<Integer, String>();
		Map<String, String> metaTittleMap = new HashMap<String, String>();
		Map<String, String> metaDescriptionMap = new HashMap<String, String>();
		Map<String, String> metaKeywordMap = new HashMap<String, String>();
		
		
		for (Products products : subCatProducts) {
			 subCatMap.put(products.getProductId(), getCleanSEOURL(products.getName()));
		}
		String metaTittle,metaDescription,metaKeyword;
		for (SubCategoryList subCategoryList2 : subCategoryList) {
			metaTittle = subCategoryList2.getName().toLowerCase();
			metaDescription = subCategoryList2.getName().toLowerCase();
			metaKeyword = subCategoryList2.getName().toLowerCase();
			metaTittleMap.put(metaTittle, subCategoryList2.getMetaTitle());
			metaDescriptionMap.put(metaDescription, subCategoryList2.getMetaDescription());
			metaKeywordMap.put(metaKeyword, subCategoryList2.getMetaKeywords());
		}
		String breadCat = catName.replace("-", " ");
		
		model.addAttribute("breadCat", breadCat);
		model.addAttribute("breadSubCat", finalName);
		model.addAttribute("subCatName", subCatName);
		model.addAttribute("catName",catName);
		model.addAttribute("subCatMap", subCatMap);
		model.addAttribute("metaTittle", metaTittleMap.get(finalName));
		model.addAttribute("metaDescription", metaDescriptionMap.get(finalName));
		model.addAttribute("metaKeywords", metaKeywordMap.get(finalName));
		return "subcat-products";
	}
	
	@GetMapping("/{catName}/{subCatName}/{productName}")
	public String productDetailById(@PathVariable String catName,@PathVariable String subCatName,@PathVariable String productName, @ModelAttribute AddToCartForm addToCartForm,
			@ModelAttribute ReviewForm reviewForm, Model model) {
		String Name = productName.replace("-"," ")+" - 12 Bottles";
		String finalName = Name.toLowerCase();
		System.out.println(finalName);
		ProductDetail productDetails = homeService.getProductDetailsByName(finalName);
		model.addAttribute("productDetail", productDetails);
		
		List<Products> products = productDetails.getSimilarProducts();
		Map<Integer, String> smilarProdMap = new HashMap<Integer, String>();
		
		for (Products products2 : products) {
			smilarProdMap.put(products2.getProductId(), getCleanSEOURL(products2.getName()));
		}
		model.addAttribute("smimilarProductMap", smilarProdMap);
//		model.addAttribute("productName", getCleanSEOURL(productDetails.getName()));
		
		return "product-details";
	}
	
	
   
	@GetMapping("/header")
	public String header(Model model) {
		CategoryList[] categories = homeService.getAllCategories(siteId);
		SubCategoryList[] subCategoryList = homeService.getSubcategory(siteId);
		
		model.addAttribute("categories", categories);
		model.addAttribute("siteId", siteId);
		
		Map<Integer, String> categoriesMap = new HashMap<Integer, String>();
		Map<Integer, String> subCategoriesMap = new HashMap<Integer, String>();
		
		for (SubCategoryList subCategoryList2 : subCategoryList) {
			subCategoriesMap.put(subCategoryList2.getId(), getCleanSEOURL(subCategoryList2.getName()));
		}
		
		for (CategoryList categoryList : categories) {
			categoriesMap.put(categoryList.getId(), getCleanSEOURL(categoryList.getName()));
		}
		
		model.addAttribute("categoriesMap", categoriesMap);
		model.addAttribute("subCategoriesMap", subCategoriesMap);
		return "header";

	}

	@GetMapping("/header-demo")
	public String headerDemo(Model model) {
		// model.addAttribute("categories", homeService.getCategories(siteId));
		model.addAttribute("categories", homeService.getAllCategories(siteId));
		model.addAttribute("siteId", siteId);
		return "header-demo";

	}

//	@PostMapping("/header")
//	public String headerPost(Model model) {
//		// model.addAttribute("categories", homeService.getCategories(siteId));
//		model.addAttribute("categories", homeService.getAllCategories(siteId));
//		model.addAttribute("siteId", siteId);
//		return "header";
//
//	}

	@GetMapping("/myorders")
	public String myorders(@ModelAttribute UserForm userForm, HttpServletRequest request, Model model) {
		if (request.getSession().getAttribute("loginresponse") == null) {
			return "redirect:/login";
		}
		LoginResponse loginresponse = (LoginResponse) request.getSession().getAttribute("loginresponse");
		model.addAttribute("customerDetails", homeService.getCustomerDetails(loginresponse.getCustomerId()));
		System.out.println(loginresponse.getCustomerId());
		return "my-account";

	}

	@GetMapping("/vieworderdetail/{orderNum}")
	public String vieworderdetail(@PathVariable String orderNum, HttpServletRequest request, Model model) {
		if (request.getSession().getAttribute("loginresponse") == null) {
			return "redirect:/login";
		}
		model.addAttribute("orderDetails", homeService.getOrderDetails(orderNum));
		model.addAttribute("orderHistory", homeService.getOrderHistory(orderNum));
		return "view-order-detail";

	}

	@GetMapping("/get_search_list")
	public @ResponseBody String[] getSearchList(@RequestParam("term") String query) {
		System.out.println("search query is :; " + query);
		String[] searchList = homeService.getsearchResponse(query);
		System.out.println(searchList.length);
		return searchList;

	}

	@GetMapping("/searchproduct")
	public String searchproduct(HttpServletRequest request, @RequestParam("item") String item, Model model) {
		Products [] products= homeService.getSearchedProducts(item);
		Categories[] categoriesMain = homeService.getCategories(siteId);
		SubCategoryList[] subCategoryList = homeService.getSubcategory(siteId);
		
		Map<Integer, String> catMap = new HashMap<Integer, String>();
		Map<Integer,String> subCatMap = new HashMap<Integer, String>();
		Map<Integer, String> prodMap = new HashMap<Integer, String>();
		for (Products products2 : products) {
			prodMap.put(products2.getProductId(), getCleanSEOURL(products2.getName()));
		}
		for (Categories categories : categoriesMain) {
			catMap.put(categories.getId(), getCleanSEOURL(categories.getName()));
		}
		for (SubCategoryList subCategoryList2 : subCategoryList) {
			subCatMap.put(subCategoryList2.getId(), getCleanSEOURL(subCategoryList2.getName())); 
		}
		
		model.addAttribute("prodMap", prodMap);
		model.addAttribute("catMap", catMap);
		model.addAttribute("subCatMap", subCatMap);
		model.addAttribute("item",products );
		
		System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
		for (Products products2 : products) {
			System.out.println(products2);
		}
		System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
		//model.addAttribute("item", item);
		return "products";

	}

	@GetMapping("/getProductResponse/{item}")
	public @ResponseBody Products[] getProductResponse(@PathVariable String item) {
		System.out.println("angular getProductResponse called : " + item);
		return homeService.getSearchedProducts(item);
	}
	
//	@GetMapping("/product/detail/{productId}")
//	public String productDetailById(@PathVariable int productId, @ModelAttribute AddToCartForm addToCartForm,
//			@ModelAttribute ReviewForm reviewForm, Model model) {
//		model.addAttribute("productDetail", homeService.getProductDetails(productId));
//		return "product-details";
//	}
	// <-- This Whole code is commented as of new Controller above -->
//	@GetMapping("/{productName}")
//	public String productDetailById(@PathVariable String productName, @ModelAttribute AddToCartForm addToCartForm,
//			@ModelAttribute ReviewForm reviewForm, Model model) {
//		String Name = productName.replace("-"," ")+" - 12 Bottles";
//		String finalName = Name.toLowerCase();
//		System.out.println(finalName);
//		ProductDetail productDetails = homeService.getProductDetailsByName(finalName);
//		model.addAttribute("productDetail", productDetails);
//		
//		List<Products> products = productDetails.getSimilarProducts();
//		Map<Integer, String> smilarProdMap = new HashMap<Integer, String>();
//		
//		for (Products products2 : products) {
//			smilarProdMap.put(products2.getProductId(), getCleanSEOURL(products2.getName()));
//		}
//		model.addAttribute("smimilarProductMap", smilarProdMap);
////		model.addAttribute("productName", getCleanSEOURL(productDetails.getName()));
//		
//		return "product-details";
//	}
	
	

	@GetMapping("/checkpincode/{pincode}")
	public @ResponseBody String checkPincode(@PathVariable String pincode) {
		RestTemplate rest = new RestTemplate();
		String pincodeRes = rest.getForObject(
				delhiveryAPI + "/c/api/pin-codes/json/?token=" + delhiveryToken + "&filter_codes=" + pincode + "",
				String.class);
		// System.out.println(pincodeRes);
		return pincodeRes;
	}

	@PostMapping("/product/addreview")
	public @ResponseBody String addreview(@RequestBody ReviewForm reviewForm) {
		System.out.println(reviewForm.getEmail());
		System.out.println(reviewForm.getName());
		System.out.println(reviewForm.getRating());
		System.out.println(reviewForm.getReviewText());
		System.out.println(reviewForm.getProductId());
		reviewForm.setSiteId(siteId);
		homeService.addProductReview(reviewForm);
		return "Review Added Succesfully!";

	}

	@PostMapping("/product/addtocart")
	public String productAddtocart(@RequestBody AddToCartForm addToCartForm, HttpServletRequest request, Model model) {
		System.out.println(addToCartForm.getProductName());
		System.out.println(addToCartForm.getColor());
		System.out.println(addToCartForm.getPrice());
		System.out.println(addToCartForm.getProductId());
		System.out.println(addToCartForm.getProductImage());
		System.out.println(addToCartForm.getSize());
		System.out.println(addToCartForm.getQuantity());
		System.out.println(addToCartForm.getSubSubCatId());
		List<AddToCartForm> cartList = new ArrayList<AddToCartForm>();
		if (request.getSession().getAttribute("cartList") != null) {
			cartList = (List<AddToCartForm>) request.getSession().getAttribute("cartList");
		}

		cartList.add(addToCartForm);
		request.getSession().setAttribute("cartList", cartList);
		model.addAttribute("cartList", cartList);
		return "cart-popup";

	}

	@GetMapping("/product/getcartpopup")
	public String getcartpopup(HttpServletRequest request, Model model) {
		List<AddToCartForm> cartList = new ArrayList<AddToCartForm>();
		if (request.getSession().getAttribute("cartList") != null) {
			cartList = (List<AddToCartForm>) request.getSession().getAttribute("cartList");
		}

		model.addAttribute("cartList", cartList);
		return "cart-popup";
	}

	@GetMapping({ "/product/viewshoppingcart", "/product/updateshoppingcart" })
	public String viewshoppingcart(HttpServletRequest request, Model model, @ModelAttribute CheckoutForm checkoutForm) {
		List<AddToCartForm> cartList = new ArrayList<AddToCartForm>();
		if (request.getSession().getAttribute("cartList") != null) {
			cartList = (List<AddToCartForm>) request.getSession().getAttribute("cartList");
		}

		model.addAttribute("cartList", cartList);
		model.addAttribute("siteId", siteId);
		return "shopping-cart";
	}

	@PostMapping("/product/updateshoppingcart")
	public String updateshoppingcart(HttpServletRequest request, Model model) {
		List<AddToCartForm> cartList = new ArrayList<AddToCartForm>();
		if (request.getSession().getAttribute("cartList") != null) {
			cartList = (List<AddToCartForm>) request.getSession().getAttribute("cartList");
		}
		for (int i = 0; i < cartList.size(); i++) {
			// System.out.println("quantity : "+ request.getParameter("quantity"+i));
			String quantity = request.getParameter("quantity" + i);
			cartList.get(i).setQuantity(Integer.parseInt(quantity));
		}

		request.getSession().setAttribute("cartList", cartList);
		model.addAttribute("cartList", cartList);
		return "redirect:/product/updateshoppingcart";
	}

	@GetMapping("/product/removecart/{productId}")
	public String removecart(@PathVariable int productId, HttpServletRequest request, Model model) {
		List<AddToCartForm> cartList = new ArrayList<AddToCartForm>();
		if (request.getSession().getAttribute("cartList") != null) {
			cartList = (List<AddToCartForm>) request.getSession().getAttribute("cartList");

		}
		/*
		 * for (AddToCartForm addToCartForm : cartList) {
		 * if(addToCartForm.getProductId() == productId){
		 * cartList.remove(addToCartForm); } }
		 */
		int removeIndex = 0;
		boolean remove = false;
		for (int i = 0; i < cartList.size(); i++) {
			if (cartList.get(i).getProductId() == productId) {
				removeIndex = i;
				remove = true;
			}
		}
		if (remove == true) {
			cartList.remove(removeIndex);
		}
		model.addAttribute("cartList", cartList);
		request.getSession().setAttribute("cartList", cartList);
		return "cart-popup";

	}

	@GetMapping("/product/viewshoppingcartremove/{productId}")
	public String viewshoppingcartremove(HttpServletRequest request, @PathVariable int productId) {
		List<AddToCartForm> cartList = new ArrayList<AddToCartForm>();
		if (request.getSession().getAttribute("cartList") != null) {
			cartList = (List<AddToCartForm>) request.getSession().getAttribute("cartList");
		}

		int removeIndex = 0;
		boolean remove = false;
		for (int i = 0; i < cartList.size(); i++) {
			if (cartList.get(i).getProductId() == productId) {
				removeIndex = i;
				remove = true;
			}
		}
		if (remove == true) {
			cartList.remove(removeIndex);
		}
		request.getSession().setAttribute("cartList", cartList);
		return "redirect:/product/viewshoppingcart";
	}

//	@GetMapping("/product/compare/add/{productId}")
//	public String productCompareAdd(HttpServletRequest request, @PathVariable int productId) {
//		List<ProductDetail> compareList = new ArrayList<ProductDetail>();
//		if (request.getSession().getAttribute("compareList") != null) {
//			compareList = (List<ProductDetail>) request.getSession().getAttribute("compareList");
//
//		}
//		if (compareList.size() == 3) {
//			compareList.remove(0);
//		}
//		// CompareForm compare = new CompareForm();
//		ProductDetail productDetail = homeService.getProductDetails(productId);
//		compareList.add(productDetail);
//		request.getSession().setAttribute("compareList", compareList);
//		return "redirect:/product/compare";
//
//	}

	@GetMapping("/product/compare/remove/{productId}")
	public String productCompareRemove(HttpServletRequest request, @PathVariable int productId) {
		List<ProductDetail> compareList = new ArrayList<ProductDetail>();
		if (request.getSession().getAttribute("compareList") != null) {
			compareList = (List<ProductDetail>) request.getSession().getAttribute("compareList");
		}

		int removeIndex = 0;
		boolean remove = false;
		for (int i = 0; i < compareList.size(); i++) {
			if (compareList.get(i).getProductId() == productId) {
				removeIndex = i;
				remove = true;
			}
		}
		if (remove == true) {
			compareList.remove(removeIndex);
		}
		request.getSession().setAttribute("compareList", compareList);
		return "redirect:/product/compare";
	}

	@GetMapping("/product/compare")
	public String compare(HttpServletRequest request, Model model) {
		List<ProductDetail> compareList = new ArrayList<ProductDetail>();
		if (request.getSession().getAttribute("compareList") != null) {
			compareList = (List<ProductDetail>) request.getSession().getAttribute("compareList");

		}
		model.addAttribute("compareList", compareList);
		return "compare";
	}

	@GetMapping("/product/addtowishlist/{productId}")
	public String addtowishlist(HttpServletRequest request, @PathVariable int productId, Model model) {
		WishlistRequest wish = new WishlistRequest();
		wish.setProductId(productId);
		if (request.getSession().getAttribute("loginresponse") == null) {
			return "redirect:/loginwishlist?addtowishlist=" + productId;
		}
		LoginResponse loginResponse = (LoginResponse) request.getSession().getAttribute("loginresponse");
		wish.setCustomerId(loginResponse.getCustomerId());
		homeService.saveWishlist(wish);
		// model.addAttribute("wishlist",
		// homeService.getWishlist(wish.getCustomerId()));
		return "redirect:/wishlist";

	}

	@GetMapping("/wishlist")
	public String wishlist(HttpServletRequest request, Model model) {
		if (request.getSession().getAttribute("loginresponse") == null) {
			return "redirect:/login";
		}
		LoginResponse loginResponse = (LoginResponse) request.getSession().getAttribute("loginresponse");
		model.addAttribute("wishlist", homeService.getWishlist(loginResponse.getCustomerId()));
		return "wishlist";
	}

	@GetMapping("/product/removewishlist/{productId}")
	public String removewishlist(@PathVariable int productId, HttpServletRequest request) {
		if (request.getSession().getAttribute("loginresponse") == null) {
			return "redirect:/login";
		}
		LoginResponse loginResponse = (LoginResponse) request.getSession().getAttribute("loginresponse");
		WishlistRequest wish = new WishlistRequest();
		wish.setProductId(productId);
		wish.setCustomerId(loginResponse.getCustomerId());
		homeService.removeFromWishlist(wish);
		return "redirect:/wishlist";

	}

	@PostMapping("/product/checkcoupon")
	public @ResponseBody CouponResponse checkcoupon(@RequestBody CouponForm couponForm) {
		System.out.println(couponForm.getCouponCode());
		System.out.println(couponForm.getCustomerId());
		System.out.println(couponForm.getProductId());
		System.out.println(couponForm.getSiteId());
		System.out.println(couponForm.getSubSubCategoryId());
		System.out.println(couponForm.getTotalAmount());
		return homeService.checkCoupon(couponForm);

	}

	@PostMapping("/product/shippingcharge")
	public @ResponseBody ShippingChargeResponse shippingChargeResponse(
			@RequestBody ShippingChargeRequest shippingChargeRequest) {
		return homeService.getShippingCharge(shippingChargeRequest);
	}

	@GetMapping("/cart/checkout")
	public String cartcheckout(@ModelAttribute CheckoutForm checkoutForm, HttpServletRequest request, Model model) {
		List<AddToCartForm> cartList = new ArrayList<AddToCartForm>();
		if (request.getSession().getAttribute("cartList") != null) {
			cartList = (List<AddToCartForm>) request.getSession().getAttribute("cartList");
		}
		model.addAttribute("user", "false");
		if (request.getSession().getAttribute("loginresponse") != null) {
			model.addAttribute("user", "true");
		}

		model.addAttribute("cartList", cartList);
		model.addAttribute("countryList", StripeUtility.getCountryList());
		model.addAttribute("siteId", siteId);
		model.addAttribute("checkoutForm", checkoutForm);
		return "checkout";

	}

	@PostMapping("/cart/checkout")
	public String cartcheckoutPost(@ModelAttribute CheckoutForm checkoutForm, HttpServletRequest request, Model model) {
		System.out.println("coupon applied : " + checkoutForm.getCouponApplied());
		List<AddToCartForm> cartList = new ArrayList<AddToCartForm>();
		if (request.getSession().getAttribute("cartList") != null) {
			cartList = (List<AddToCartForm>) request.getSession().getAttribute("cartList");
		}
		model.addAttribute("user", "false");
		if (request.getSession().getAttribute("loginresponse") != null) {
			model.addAttribute("user", "true");
		}

		model.addAttribute("cartList", cartList);
		model.addAttribute("countryList", StripeUtility.getCountryList());
		model.addAttribute("siteId", siteId);
		model.addAttribute("checkoutForm", checkoutForm);
		return "checkout";

	}

	@PostMapping("/cart/order")
	public String cartorder(@ModelAttribute CheckoutForm checkoutForm, HttpServletRequest request, Model model)
			throws JsonProcessingException {
		
		List<AddToCartForm> cartList = new ArrayList<AddToCartForm>();
		
		if (request.getSession().getAttribute("cartList") != null) {
			cartList = (List<AddToCartForm>) request.getSession().getAttribute("cartList");
		}
		
		List<com.shopwise.objects.order.ProductDetail> productList = new ArrayList<com.shopwise.objects.order.ProductDetail>();
		
		for (int i = 0; i < cartList.size(); i++) {
			com.shopwise.objects.order.ProductDetail product = new com.shopwise.objects.order.ProductDetail();
			product.setPrice(cartList.get(i).getPrice());
			product.setProductCode("");
			product.setProductId(cartList.get(i).getProductId());
			product.setProductName(cartList.get(i).getProductName());
			product.setQuantity(cartList.get(i).getQuantity());
			product.setSize(cartList.get(i).getSize());
			product.setColor(cartList.get(i).getColor());
			productList.add(product);
		}
		
		checkoutForm.setProductList(productList);

		ObjectMapper mapper = new ObjectMapper();
		System.out.println("cart order : " + mapper.writeValueAsString(checkoutForm));
		OrderDetails orderDetails = new OrderDetails();
		
		orderDetails.setBillingAddress(checkoutForm.getBillingaddress());
		orderDetails.setShippingAddress(checkoutForm.getShippingaddress());
		
		if (checkoutForm.isDifferentShipping()) {
			orderDetails.setCustomerContact(checkoutForm.getShippingaddress().getContactNum());
			orderDetails.setCustomerEmail(checkoutForm.getShippingaddress().getEmail());
			orderDetails.setCustomerName(checkoutForm.getShippingaddress().getFirstName() + " "
					+ checkoutForm.getShippingaddress().getLastName());
		} else {
			ShippingAddress shipping = new ShippingAddress();
			shipping.setAddress(checkoutForm.getBillingaddress().getAddress());
			shipping.setAddress1(checkoutForm.getBillingaddress().getAddress1());
			shipping.setCity(checkoutForm.getBillingaddress().getCity());
			shipping.setContactNum(checkoutForm.getBillingaddress().getContactNum());
			shipping.setCountry(checkoutForm.getBillingaddress().getCountry());
			shipping.setEmail(checkoutForm.getBillingaddress().getEmail());
			shipping.setFirstName(checkoutForm.getBillingaddress().getFirstName());
			shipping.setLastName(checkoutForm.getBillingaddress().getLastName());
			shipping.setState(checkoutForm.getBillingaddress().getState());
			shipping.setZipCode(checkoutForm.getBillingaddress().getZipCode());
			orderDetails.setShippingAddress(shipping);
			orderDetails.setCustomerContact(checkoutForm.getBillingaddress().getContactNum());
			orderDetails.setCustomerEmail(checkoutForm.getBillingaddress().getEmail());
			orderDetails.setCustomerName(checkoutForm.getBillingaddress().getFirstName() + " "
					+ checkoutForm.getShippingaddress().getLastName());
		}
		
		orderDetails.setCustomerId(checkoutForm.getCustomerId());
		orderDetails.setOrderComment(checkoutForm.getOrderNotes());
//		orderDetails.setPaymentDetail(checkoutForm.getPaymentDetail());
		orderDetails.setPaymentMode(checkoutForm.getPaymentDetail().getPaymentMode());
		orderDetails.setProductList(checkoutForm.getProductList());

		orderDetails.setSiteId(checkoutForm.getSiteId());
		orderDetails.setCounponDiscount(checkoutForm.getCouponDiscount());
		orderDetails.setCouponApplied(checkoutForm.getCouponApplied());
		if (checkoutForm.getCouponApplied().equalsIgnoreCase("")) {
			orderDetails.setCouponApplied("No");
		}

		orderDetails.setCouponCode(checkoutForm.getCouponText());
		String orderNum = homeService.orderProducts(orderDetails);
		checkoutForm.setOrderNum(orderNum);
		// homeService.delhiveryOrderCreate(orderDetails,orderNum);
		EmailSend emailSend = new EmailSend();
		emailSend.setBccEmail("");
		emailSend.setBookingID(0);
		emailSend.setCcEmail("");
		emailSend.setFromEmail("");
		emailSend.setMailBody("");
		emailSend.setMailSubject("Your Shopping Order Number : " + orderNum);
		emailSend.setToEmail(orderDetails.getShippingAddress().getEmail());
		String mailRes = "false";
		try {
			mailRes = mailController.SendMail(emailSend, "", "", "");
		} catch (IOException | URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Order Num " + orderNum + " Mail Send : " + mailRes);
		request.getSession().setAttribute("checkoutForm", checkoutForm);
		System.out.println("orderNum is : " + orderNum);
		model.addAttribute("orderNum", orderNum);
		// return "redirect:/rajorPay";

		//String paytm
		// stripe
		// razorpay
		// paypal

		String redirectString = "redirect:/paytm/payment/charge";

		switch (checkoutForm.getPaymentDetail().getPaymentMode()) {
		case "paytm":
			redirectString = "redirect:/paytm/payment/charge";
			break;
		case "stripe":
			redirectString = "redirect:/eway";
			break;
		case "Eway":
			redirectString = "redirect:/eway";
			break;
		case "paypal":
			redirectString = "redirect:/eway";
			break;
		default:
			System.out.println(checkoutForm.getPaymentDetail().getPaymentMode());
			break;
		}

		return redirectString;

	}

}
