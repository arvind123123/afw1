package com.shopwise.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

	@GetMapping("/faqs")
	public String faqs(){
		return "faq";
	}
	
	@GetMapping("/about-us")
	public String aboutus(){
		return "aboutus";
	}
	
	@GetMapping("/contact-us")
	public String contactus(){
		return "contact-us";
		
	}
	
	@GetMapping("/terms-conditions")
	public String terms(){
		return "terms-conditions";
		
	}
	
	@GetMapping("/affirmpay")  
	public String affirm(){
		return "affirm";
		
	}
	@GetMapping("/delivery-returns")
	public String dilivery() {
		return "dilivery";
	}
	
	@GetMapping("/vip")
	public String vip() {
		return "vipmember";
	}
	
	@GetMapping("/wine-reserve")
	public String wineReserve() {
		return "wineReserve";
	}
	
	@GetMapping("/trophyClub")
	public String trophyClub() {
		return "trophyClub";
	}
	
	@GetMapping("/referandearn")
	public String referAndEarn() {
		return "referAndEarn";
	}
	
	@GetMapping("/blog")
	public String blog() {
		return "blog";
	}
	
	@GetMapping("/faq")
	public String faq() {
		return "faq";
	}
	
	@GetMapping("/wineries")
	public String wineries() {
		return "wineries";
	}
	
	@GetMapping("/wine-regions")
	public String wineRegions() {
		return "wineRegions";
	}
	
	@GetMapping("/why-us")
	public String whyus() {
		return "whybuyus";
	}
	
	@GetMapping("/why-sell")
	public String whysell() {
		return "whysellus";
	}
	
//	@GetMapping("/terms-conditions")
//	public String tandc(){
//		return "t&c";
//	}
	
	@GetMapping("/privacy-policy")
	public String privacypolicy() {
		return "privacypolicy";
	}
	
}

