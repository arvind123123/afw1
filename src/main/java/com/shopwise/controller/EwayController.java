package com.shopwise.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.eway.payment.rapid.sdk.*;
import com.eway.payment.rapid.sdk.beans.external.*;
import com.eway.payment.rapid.sdk.output.*;
import com.shopwise.bean.CheckoutForm;
import com.shopwise.objects.order.PaymentStatusUpdate;
import com.shopwise.service.HomeService;

@Controller
public class EwayController {
	
	@Autowired
	HomeService homeService;
	
//	@Value("${eway.api.key}")
//	private String API_PUBLIC_KEY;
//	
//	@Value("${eway.api.password}")
//	private String API_PASSWORD;
	
	public static final String SUCCESS_URL = "eway/success";
	public static final String CANCEL_URL = "eway/cancel";

	String apiKey = "60CF3AOSqr0iv+O+idaPFBhpqAywh5QgAjg0n/yMakhVd3dK0gzNEOjKyWW2k31yWN2rzm";
	String password = "m7Rt10mU";
	String rapidEndpoint = "Production";

	@GetMapping("/eway")
	public String payment(CheckoutForm checkoutForm, HttpServletRequest request) {
		RapidClient client = RapidSDK.newRapidClient(apiKey, password, rapidEndpoint);
		checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
		double cartTotal = 0;
		for (int i = 0; i < checkoutForm.getProductList().size(); i++) {
			cartTotal = (cartTotal + (checkoutForm.getProductList().get(i).getPrice()
					* checkoutForm.getProductList().get(i).getQuantity()));
		}
		
		Transaction transaction = new Transaction();
		Customer customerDetails =  new Customer();
		customerDetails.setFirstName(checkoutForm.getBillingaddress().getFirstName());
		customerDetails.setLastName(checkoutForm.getBillingaddress().getLastName());
		customerDetails.setMobile(checkoutForm.getBillingaddress().getContactNum());
		customerDetails.setEmail(checkoutForm.getBillingaddress().getEmail());
		
		
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setCurrencyCode("AUD");
		paymentDetails.setTotalAmount((int) cartTotal * 100);
		paymentDetails.setInvoiceReference(checkoutForm.getOrderNum());

		transaction.setPaymentDetails(paymentDetails);
		transaction.setRedirectURL("https://allfinewines.com.au/"+ SUCCESS_URL);
		transaction.setCancelURL("https://allfinewines.com.au/"+ CANCEL_URL);
		transaction.setTransactionType(TransactionType.Purchase);

		CreateTransactionResponse response = client.create(PaymentMethod.ResponsiveShared, transaction);
		String redirectURL = null;
		if (response.getErrors().isEmpty()) {
		    // Redirect the user to this location
		    redirectURL = response.getSharedPaymentUrl();
		} else {
		    for (String errorcode: response.getErrors()) {
		        System.out.println("Response Messages: " + RapidSDK.userDisplayMessage(errorcode, "en"));
		    };
		}
		
		return "redirect:"+redirectURL;
	}
	
	@GetMapping("eway/success")
	public String paymentSucess(CheckoutForm checkoutForm, HttpServletRequest request,Model model) {
		checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
		PaymentStatusUpdate paymentStatus = new PaymentStatusUpdate();
		paymentStatus.setOrderNum(checkoutForm.getOrderNum());
		paymentStatus.setOrderStatus("Success");
		paymentStatus.setPaymentMode(checkoutForm.getPaymentDetail().getPaymentMode());
		model.addAttribute("orderNum", checkoutForm.getOrderNum());
		homeService.updatePaymentStatus(paymentStatus);
		request.getSession().setAttribute("cartList", null);
		return "order-complete";
	}
	
	@GetMapping("eway/cancel")
	public String paymentCancle(CheckoutForm checkoutForm, HttpServletRequest request,Model model) {
		checkoutForm = (CheckoutForm) request.getSession().getAttribute("checkoutForm");
		PaymentStatusUpdate paymentStatus = new PaymentStatusUpdate();
		paymentStatus.setOrderNum(checkoutForm.getOrderNum());
		paymentStatus.setOrderStatus("Failed");
		paymentStatus.setPaymentMode(checkoutForm.getPaymentDetail().getPaymentMode());
		homeService.updatePaymentStatus(paymentStatus);
		model.addAttribute("orderNum", checkoutForm.getOrderNum());
		return "order-cancel";
	}
}
