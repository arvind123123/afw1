package com.shopwise.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shopwise.bean.UserForm;
import com.shopwise.objects.LoginResponse;
import com.shopwise.service.LoginService;

@Controller
public class LoginController {

	@Autowired
	LoginService loginService;
	
	@GetMapping("/loginwishlist")
	public String login(@ModelAttribute UserForm userForm,@RequestParam("addtowishlist") int addtowishlist){
		userForm.setProductId(addtowishlist);
		return "login";
		
	}
	
	@GetMapping("/login")
	public String login(@ModelAttribute UserForm userForm){
		return "login";
		
	}
	
	@GetMapping("/logout")
	public String logout(@ModelAttribute UserForm userForm,HttpServletRequest request){
		request.getSession().removeAttribute("loginresponse");
		return "login";
		
	}
	
	@GetMapping("/signup")
	public String signup(@ModelAttribute UserForm userForm){
		return "signup";
		
	}
	
	@PostMapping("/signup")
	public String signupPost(@ModelAttribute UserForm userForm,Model model){
		String res = loginService.registerUser(userForm);
		model.addAttribute("success", res);
		return "signup";
	}
	
	@PostMapping("/cart/signup")
	public String cartSignupPost(@ModelAttribute UserForm userForm,Model model){
		String res = loginService.checkRegisterUser(userForm);
		if (res.equalsIgnoreCase("User Already Registered!")) {
			return "userPresent";
		}
		return null;
	}
	
	
	
	@PostMapping("/login")
	public String loginPost(@ModelAttribute UserForm userForm,Model model,HttpServletRequest request){
		System.out.println("user losgin post product id is : " + userForm.getProductId());
		String res = loginService.loginUser(userForm);
		ObjectMapper mapper = new ObjectMapper();
		LoginResponse loginresponse = null;
		try {
			loginresponse = 	mapper.readValue(res, LoginResponse.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(">>>" + res);
		if(loginresponse.getResponseStatus().getMessage().equalsIgnoreCase("success")){
			request.getSession().setAttribute("loginresponse", loginresponse);
			if(userForm.getProductId() != 0){
				return "redirect:/wishlist";
			}
			else{
			return "redirect:/";
			}
		}
		model.addAttribute("success", loginresponse.getResponseStatus().getMessage());
		return "login";
		
	}
	
	@PostMapping("/cart/checkout/login")
	public @ResponseBody LoginResponse cartCheckoutLogin(@RequestBody UserForm userForm,HttpServletRequest request,Model model){
		System.out.println("cart checkout login : " + userForm.getEmail());
		System.out.println("cart checkout login : " + userForm.getPassword());
		
		String res = loginService.loginUser(userForm);
		ObjectMapper mapper = new ObjectMapper();
		LoginResponse loginresponse = null;
		try {
			loginresponse = 	mapper.readValue(res, LoginResponse.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(loginresponse.getResponseStatus().getMessage().equalsIgnoreCase("success")){
			request.getSession().setAttribute("loginresponse", loginresponse);
		}
		return loginresponse;
		//return loginresponse.getResponseStatus().getMessage();
		
	}
	
	@PostMapping("/updateregisteruser")
	public @ResponseBody String updateregisteruser(@RequestBody UserForm userForm,Model model,HttpServletRequest request){
		if(request.getSession().getAttribute("loginresponse") == null){
			return "redirect:/login";
		}
		LoginResponse loginresponse  = (LoginResponse) request.getSession().getAttribute("loginresponse");
		String res = loginService.updateUser(userForm,loginresponse.getCustomerId());
		return res;
	}
}
