package com.shopwise.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import com.shopwise.objects.EmailSend;
import com.sun.mail.util.MailSSLSocketFactory;

@Service
public class MailController {

	private  String sharedKey = "SHARED_KEY";
	
	
	public String SendMail(EmailSend emailFormat,String host,String mailport,String pass) throws IOException, URISyntaxException {
		String authcode = "SHARED_KEY";
		String status="false";
		String bccMail="";
		if (sharedKey.equalsIgnoreCase(authcode)) {
			try {
				Map<String,String> emailId=new HashMap<String,String>();

				emailId.put(emailFormat.getFromEmail().trim(),pass.trim());
				Properties props = new Properties();
		
					
			 		if(host.equalsIgnoreCase("smtp.gmail.com")) {


						props.put("mail.smtp.host", host);
						props.put("mail.smtp.socketFactory.port", mailport);
						props.put("mail.smtp.socketFactory.class",
								"javax.net.ssl.SSLSocketFactory");
						props.put("mail.smtp.auth", "true");
			 			props.put("mail.smtp.port", "465"); 
			 			bccMail="tech@traveloes.com";
					}else {
						bccMail="prince@w2c.co.in";
						props.put("mail.transport.protocol", "smtp");
						props.put("mail.smtp.host", host);
						props.put("mail.smtp.port", mailport);

						props.put("mail.smtp.auth", "true");
						if( mailport.equals("587") ) {
							props.put("mail.smtp.starttls.enable", "true");
						} else {
							props.put("mail.smtps.ssl.enable", "true");
						}
						props.put("mail.smtp.ssl.trust",host);
						props.put("mail.smtp.socketFactory", "javax.net.ssl.SSLSocketFactory");
						props.put("mail.smtp.socketFactory.port", "465");
						props.put("mail.smtp.socketFactory.fallback", "false");
						props.put("mail.smtp.debug", "true");
						props.put("mail.smtp.quitwait", "false");
						MailSSLSocketFactory sf = new MailSSLSocketFactory();
						sf.setTrustAllHosts(true); 
						props.put("mail.imap.ssl.trust", "*");
						props.put("mail.imap.ssl.socketFactory", sf);
					}
				


				
				try {

					Session session = Session.getInstance(props,
							new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(emailFormat.getFromEmail(), emailId.get(emailFormat.getFromEmail()));
						}
					});
 
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(emailFormat.getFromEmail()));
					message.setRecipients(Message.RecipientType.TO,
							InternetAddress.parse(emailFormat.getToEmail()));
					message.setRecipients(Message.RecipientType.CC,
							InternetAddress.parse(emailFormat.getCcEmail()));
					/*message.setRecipients(Message.RecipientType.BCC,
						InternetAddress.parse(emailFormat.getBccEmail()));*/
					if(emailFormat.getBookingID() != null) {
						message.setRecipients(Message.RecipientType.BCC,
								InternetAddress.parse(bccMail));
					}
					message.setSubject(emailFormat.getMailSubject());
					//message.setText(emailFormat.getMailBody());
					message.setContent(emailFormat.getMailBody(), "text/html");
					Transport.send(message);

					try {
						System.out.println("Mail Sent from:"+emailFormat.getFromEmail()+" to id:"+emailFormat.getToEmail());
					} catch (Exception e) {

					}
					status="true";
				} catch (Exception e) {
					e.printStackTrace();
					status="false";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/*try {
			TraveloesMail entity=new TraveloesMail();
			entity.setBookingId(emailFormat.getBookingID());
			entity.setSendStatus(status);
			traveloesMailService.save(entity);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		return status;

	}



	
	
}
