package com.shopwise.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.shopwise.bean.UserForm;
import com.shopwise.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService{

	@Value("${apiUrl}")
	private String apiUrl;
	
	@Value("${authCode}")
	private String authCode; 
	
	@Override
	public String registerUser(UserForm userForm) {
		RestTemplate client = new RestTemplate();
		String res = client.postForObject(apiUrl+"/user/register?authCode="+authCode, userForm, String.class);
		return res;
	}
	
	@Override
	public String checkRegisterUser(UserForm userForm) {
		RestTemplate client = new RestTemplate();
		String res = client.postForObject(apiUrl+"/cart/userRegister?authCode="+authCode, userForm, String.class);
		return res;
	}
	
	@Override
	public String loginUser(UserForm userForm) {
		RestTemplate client = new RestTemplate();
		String res = client.postForObject(apiUrl+"/user/login?authCode="+authCode, userForm, String.class);
		System.out.println(res);
		return res;
	}

	@Override
	public String updateUser(UserForm userForm,int customerId) {
		RestTemplate client = new RestTemplate();
		String res = client.postForObject(apiUrl+"/user/update/"+customerId+"?authCode="+authCode, userForm, String.class);
		return res;
	}

}
