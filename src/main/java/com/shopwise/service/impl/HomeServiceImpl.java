package com.shopwise.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shopwise.bean.CouponForm;
import com.shopwise.bean.ReviewForm;
import com.shopwise.objects.Categories;
import com.shopwise.objects.CategoryList;
import com.shopwise.objects.CouponResponse;
import com.shopwise.objects.CustomerDetails;
import com.shopwise.objects.ProductDetail;
import com.shopwise.objects.Products;
import com.shopwise.objects.SubCategoryList;
import com.shopwise.objects.WishlistRequest;
import com.shopwise.objects.WishlistResponse;
import com.shopwise.objects.order.OrderDetails;
import com.shopwise.objects.order.OrderHistory;
import com.shopwise.objects.order.PaymentStatusUpdate;
import com.shopwise.objects.order.delhivery.ordercreate.OrderCreate;
import com.shopwise.objects.order.delhivery.ordercreate.OrderCreateResponse;
import com.shopwise.objects.order.delhivery.ordercreate.PickupLocation;
import com.shopwise.objects.order.delhivery.ordercreate.Shipment;
import com.shopwise.objects.order.delhivery.shippingcharge.ShippingChargeRequest;
import com.shopwise.objects.order.delhivery.shippingcharge.ShippingChargeResponse;
import com.shopwise.service.HomeService;

@Service
public class HomeServiceImpl implements HomeService{

	@Value("${apiUrl}")
	private String apiUrl;
	
	@Value("${authCode}")
	private String authCode;
	
	@Value("${siteId}")
	private String siteId;
	
	@Value("${delhiveryToken}")
	private String delhiveryToken;
	
	@Value("${delhiveryAPI}")
	private String delhiveryAPI;
	
	@Override
	public Categories[] getCategories(int siteId) {
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		Categories[] categories = null;
		String res = client.getForObject(apiUrl+"/getcategories/"+siteId+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
		 categories = 	mapper.readValue(res, Categories[].class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return categories;
		
		
	}
	
	@Override
	public SubCategoryList[] getSubcategory(int siteId) {
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		SubCategoryList[] subCategory = null;
		String res = client.getForObject(apiUrl+"/getsubCategory/"+siteId+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
		 subCategory = mapper.readValue(res, SubCategoryList[].class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return subCategory;
		
		
	}

	@Override
	public Products[] getExclusiveProducts(int siteId) {
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		Products[] prodcuts = null;
		String res = client.getForObject(apiUrl+"/getexclusiveproducts/"+siteId+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
			prodcuts = 	mapper.readValue(res, Products[].class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prodcuts;
		
		
	}

	@Override
	public Products[] getFeaturedProducts(int siteId) {
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		Products[] prodcuts = null;
		String res = client.getForObject(apiUrl+"/getfeaturedproducts/"+siteId+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
			prodcuts = 	mapper.readValue(res, Products[].class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prodcuts;
		
		
	}
//	@Override
//	public ProductDetail getProductDetails(int productId) {
//		RestTemplate client = new RestTemplate();
//		ObjectMapper mapper = new ObjectMapper();
//		ProductDetail productDetail = null;
//		String res = client.getForObject(apiUrl+"/getproductdetails/"+siteId+"/"+productId+"?authCode="+authCode, String.class);
//		System.out.println(res);
//		try {
//			productDetail = 	mapper.readValue(res, ProductDetail.class);
//		} catch (JsonParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (JsonMappingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return productDetail;
//		
//		
//	}
	
	@Override
	public ProductDetail getProductDetails(String productName) {
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		ProductDetail productDetail = null;
		String res = client.getForObject(apiUrl+"/getproductdetails/"+siteId+"/"+productName+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
			productDetail = 	mapper.readValue(res, ProductDetail.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productDetail;
		
		
	}
	
	@Override
	public ProductDetail getProductDetailsByName(String productName) {
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		ProductDetail productDetail = null;
		String res = client.getForObject(apiUrl+"/getproductdetailscontaining/"+siteId+"/"+productName+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
			productDetail = 	mapper.readValue(res, ProductDetail.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productDetail;
		
	}


	@Override
	public void saveWishlist(WishlistRequest wish) {
		RestTemplate client = new RestTemplate();
		String res = client.postForObject(apiUrl+"/product/addtowishlist/?authCode="+authCode, wish,String.class);
		//System.out.println(res);
			
		
	}

	@Override
	public WishlistResponse[] getWishlist(int customerId) {
		System.out.println("customerId in homeserviceimpl : " + customerId);
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		WishlistResponse[] wishRes = null;
		System.out.println(apiUrl+"/product/getwishlist/"+customerId+"?authCode="+authCode);
		String res = client.getForObject(apiUrl+"/product/getwishlist/"+customerId+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
			wishRes = 	mapper.readValue(res, WishlistResponse[].class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return wishRes;
		
		
	
	}

	@Override
	public void removeFromWishlist(WishlistRequest wish) { 
		RestTemplate client = new RestTemplate();
		String res = client.postForObject(apiUrl+"/product/removefromwishlist?authCode="+authCode, wish,String.class);
		
	}

	@Override
	public void addProductReview(ReviewForm review) {
		RestTemplate client = new RestTemplate();
		String res = client.postForObject(apiUrl+"/product/addreview?authCode="+authCode, review,String.class);
		
	}

	@Override
	public String[] getsearchResponse(String query) {
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String res = client.getForObject(apiUrl+"/product/searchresponse?authCode="+authCode+"&query="+query, String.class);
		System.out.println(res);
		
		try {
			return mapper.readValue(res, String[].class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Products[] getSearchedProducts(String item) {
		

		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		Products[] products = null;
		//String res = client.getForObject(apiUrl+"/getsearched/product/"+siteId+"/"+item+"?authCode="+authCode, String.class);
		String res = client.getForObject(apiUrl+"/getproductsbysubcat/"+item+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
			products = 	mapper.readValue(res, Products[].class);
			
			for (Products products2 : products) {
				String []imageUrl= products2.getImageUrl().split("/");
				if (imageUrl.length>3) {
					System.out.println(imageUrl[imageUrl.length-2]);
					products2.setImageUrl(imageUrl[imageUrl.length-2]);
				}
				
				//System.out.println(imageUrl.substring(imageUrl.indexOf("d"), imageUrl.indexOf("edit")));
				//products2.getImageUrl().substring(products2.getImageUrl().indexOf("d/"), products2.getImageUrl().indexOf("/edit")));
			}
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return products;
	
	}

	@Override
	public CategoryList[] getAllCategories(int siteId) {
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		CategoryList[] categories = null;
		String res = client.getForObject(apiUrl+"/getallcategories/"+siteId+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
		 categories = 	mapper.readValue(res, CategoryList[].class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return categories;
		
		
	}

	@Override
	public CustomerDetails getCustomerDetails(int customerId) {

		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		CustomerDetails customerDetails = null;
		String res = client.getForObject(apiUrl+"/getaccountdetails/"+customerId+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
			customerDetails = 	mapper.readValue(res, CustomerDetails.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return customerDetails;
		
		
	
	}

	@Override
	public OrderDetails getOrderDetails(String orderNum) {

		System.out.println("getorderdetails order num : " + orderNum);
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		OrderDetails orderDetails = null;
		String res = client.getForObject(apiUrl+"/getorderdetails/"+orderNum+"?authCode="+authCode, String.class);
		System.out.println("order detail respoinse : "+ res);
		try {
			orderDetails = 	mapper.readValue(res, OrderDetails.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return orderDetails;
		
		
	
	}

	@Override
	public OrderHistory[] getOrderHistory(String orderNum) {

		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		OrderHistory[] orderhistory = null;
		String res = client.getForObject(apiUrl+"/getorderhistory/"+orderNum+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
			orderhistory = 	mapper.readValue(res, OrderHistory[].class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return orderhistory;
		
		
	
	}

	@Override
	public CouponResponse checkCoupon(CouponForm couponForm) {

		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String res = client.postForObject(apiUrl+"/product/checkcoupon/?authCode="+authCode, couponForm,String.class);
		CouponResponse couponResponse=null;
		try {
			couponResponse = mapper.readValue(res, CouponResponse.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return couponResponse;
	
	}

	@Override
	public String orderProducts(OrderDetails orderDetails) {
		RestTemplate client = new RestTemplate();
		String res = client.postForObject(apiUrl+"/product/order/checkout?authCode="+authCode, orderDetails,String.class);
		return res;
	}

	@Override
	public void updatePaymentStatus(PaymentStatusUpdate payment) {
		RestTemplate client = new RestTemplate();
		client.postForObject(apiUrl+"/order/status/update?authCode="+authCode, payment,String.class);
		
	}

	@Override
	public OrderCreateResponse delhiveryOrderCreate(OrderDetails orderDetails,String orderNum) {
		
		OrderCreateResponse response = new OrderCreateResponse();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Token "+delhiveryToken);
		OrderCreate orderCreate = new OrderCreate();
		PickupLocation pickup = new PickupLocation();
		pickup.setAdd("B 44, Bhabha Marg, B Block, Sector 59, Noida, Uttar Pradesh 201301");
		pickup.setCity("Noida");
		pickup.setCountry("India");
		pickup.setName("SNVAVENTURESSURFACE-B2C");
		pickup.setPhone("9555151333");
		pickup.setPin("201301");
		pickup.setState("Uttar Pradesh");
		List<Shipment> shipList = new ArrayList<Shipment>();
		Shipment ship = new Shipment();
		ship.setAdd(orderDetails.getShippingAddress().getAddress()+" "+orderDetails.getShippingAddress().getAddress1());
		ship.setCity(orderDetails.getShippingAddress().getCity());
		ship.setClient("SNVAVENTURESSURFACE-B2C");
		ship.setCodAmount("0");
		ship.setCountry(orderDetails.getShippingAddress().getCountry());
		ship.setName(orderDetails.getShippingAddress().getFirstName()+" "+orderDetails.getShippingAddress().getLastName());
		ship.setOrder(orderNum);
		ship.setOrderDate(new Date().toString());
		ship.setPaymentMode("Prepaid");
		ship.setPhone(orderDetails.getShippingAddress().getContactNum());
		ship.setPin(orderDetails.getShippingAddress().getZipCode());
		ship.setState(orderDetails.getShippingAddress().getState());
		
		ship.setReturnAdd("B 44, Bhabha Marg, B Block, Sector 59, Noida, Uttar Pradesh 201301");
		ship.setReturnCity("Noida");
		ship.setReturnCountry("India");
		ship.setReturnName("SNVAVENTURES SURFACE");
		ship.setReturnPhone("9555151333");
		ship.setReturnPin("201301");
		ship.setReturnState("Uttar Pradesh");
		ship.setSellerAdd("");
		ship.setSellerCst("");
		ship.setSellerInv("");
		ship.setSellerInvDate("");
		ship.setSellerName("SNVA");
		ship.setSellerTin("");
		for (int i = 0; i < orderDetails.getProductList().size(); i++) {
			
		
		ship.setProductsDesc(orderDetails.getProductList().get(i).getProductName()+"-"+orderDetails.getProductList().get(i).getProductCode());
		ship.setQuantity(String.valueOf(orderDetails.getProductList().get(i).getQuantity()));
		ship.setTotalAmount(String.valueOf(orderDetails.getProductList().get(i).getPrice()));
		shipList.add(ship);
		}
		orderCreate.setPickupLocation(pickup);
		orderCreate.setShipments(shipList);
		ObjectMapper mapper = new ObjectMapper();
		String body = "";
		try {
		body = 	mapper.writeValueAsString(orderCreate);
		System.out.println(body);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpEntity<String> entity = new HttpEntity<String>("format=json&data="+body,headers);
		
		RestTemplate rest = new RestTemplate();
		
		String res = rest.postForObject(delhiveryAPI+"/api/cmu/create.json", entity, String.class);
		System.out.println("Delhivery Order Create Response : " + res);
		return response;
		
		
	}

	@Override
	public ShippingChargeResponse getShippingCharge(ShippingChargeRequest shippingCharge) {
		ShippingChargeResponse[] response = null;
		RestTemplate rest  = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		//header.setaccept
		header.set("Authorization", "Token ebf63398e5d760414088b1920d272d3cd1b380f1");
		String body = "";
		HttpEntity<String> entity = new HttpEntity<String>(body,header);
		//String res = rest.getForObject("https://track.delhivery.com/api/kinko/v1/invoice/charges?md="+shippingCharge.getMd()+"&pt="+shippingCharge.getPt()+"&cgm="+shippingCharge.getCgm()+"&o_pin=201301&d_pin="+shippingCharge.getDPin()+"&ss=Delivered"+entity ,String.class);
		ResponseEntity<String> result =rest.exchange("https://track.delhivery.com/api/kinko/v1/invoice/charges?md="+shippingCharge.getMd()+"&pt="+shippingCharge.getPt()+"&cgm="+shippingCharge.getCgm()+"&o_pin=201301&d_pin="+shippingCharge.getDPin()+"&ss=Delivered",HttpMethod.GET, entity,String.class);
		System.out.println(result.getBody());
		try {
			response =  mapper.readValue(result.getBody(), ShippingChargeResponse[].class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response[0];
	}

	

//	@Override
//	public Products[] getCatProducts(int catid) {
//		
//		RestTemplate client = new RestTemplate();
//		ObjectMapper mapper = new ObjectMapper();
//		Products[] products = null;
//		//String res = client.getForObject(apiUrl+"/getsearched/product/"+siteId+"/"+item+"?authCode="+authCode, String.class);
//		String res = client.getForObject(apiUrl+"/getprodbycat/"+siteId+"/"+catid+"?authCode="+authCode, String.class);
//		System.out.println(res);
//		try {
//			products = 	mapper.readValue(res, Products[].class);
//			
//			for (Products products2 : products) {
//				String []imageUrl= products2.getImageUrl().split("/");
//				if (imageUrl.length>3) {
//					System.out.println(imageUrl[imageUrl.length-2]);
//					products2.setImageUrl(imageUrl[imageUrl.length-2]);
//				}
//				
//				//System.out.println(imageUrl.substring(imageUrl.indexOf("d"), imageUrl.indexOf("edit")));
//				//products2.getImageUrl().substring(products2.getImageUrl().indexOf("d/"), products2.getImageUrl().indexOf("/edit")));
//			}
//			
//		} catch (JsonParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (JsonMappingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return products;
//	
//	}
	
	@Override
	public Products[] getCatProducts(String catName) {
		
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		Products[] products = null;
		//String res = client.getForObject(apiUrl+"/getsearched/product/"+siteId+"/"+item+"?authCode="+authCode, String.class);
		String res = client.getForObject(apiUrl+"/getprodbycat/"+siteId+"/"+catName+"?authCode="+authCode, String.class);
		System.out.println(res);
		if (res == null) {
			return null;
		}
		try {
			products = 	mapper.readValue(res, Products[].class);
			
			for (Products products2 : products) {
				String []imageUrl= products2.getImageUrl().split("/");
				if (imageUrl.length>3) {
					System.out.println(imageUrl[imageUrl.length-2]);
					products2.setImageUrl(imageUrl[imageUrl.length-2]);
				}
				
				//System.out.println(imageUrl.substring(imageUrl.indexOf("d"), imageUrl.indexOf("edit")));
				//products2.getImageUrl().substring(products2.getImageUrl().indexOf("d/"), products2.getImageUrl().indexOf("/edit")));
			}
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return products;
	
	}


	@Override
	public Products[] getSubCatProducts(String subCatName) {
		RestTemplate client = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		Products[] products = null;
		//String res = client.getForObject(apiUrl+"/getsearched/product/"+siteId+"/"+item+"?authCode="+authCode, String.class);
		String res = client.getForObject(apiUrl+"/getprodbysubcat/"+siteId+"/"+subCatName+"?authCode="+authCode, String.class);
		System.out.println(res);
		try {
			products = 	mapper.readValue(res, Products[].class);
			
			for (Products products2 : products) {
				String []imageUrl= products2.getImageUrl().split("/");
				if (imageUrl.length>3) {
					System.out.println(imageUrl[imageUrl.length-2]);
					products2.setImageUrl(imageUrl[imageUrl.length-2]);
				}
				
				//System.out.println(imageUrl.substring(imageUrl.indexOf("d"), imageUrl.indexOf("edit")));
				//products2.getImageUrl().substring(products2.getImageUrl().indexOf("d/"), products2.getImageUrl().indexOf("/edit")));
			}
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return products;
	
	}
	
	

	
}
