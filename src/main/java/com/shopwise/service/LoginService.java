package com.shopwise.service;

import com.shopwise.bean.UserForm;

public interface LoginService {

	public String registerUser(UserForm userForm);
	public String loginUser(UserForm userForm);
	public String updateUser(UserForm userForm,int customerId);
	public String checkRegisterUser(UserForm userForm);
}
