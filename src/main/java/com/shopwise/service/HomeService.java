package com.shopwise.service;

import com.shopwise.bean.CouponForm;
import com.shopwise.bean.ReviewForm;
import com.shopwise.objects.Categories;
import com.shopwise.objects.CategoryList;
import com.shopwise.objects.CouponResponse;
import com.shopwise.objects.CustomerDetails;
import com.shopwise.objects.ProductDetail;
import com.shopwise.objects.Products;
import com.shopwise.objects.SubCategoryList;
import com.shopwise.objects.WishlistRequest;
import com.shopwise.objects.WishlistResponse;
import com.shopwise.objects.order.OrderDetails;
import com.shopwise.objects.order.OrderHistory;
import com.shopwise.objects.order.PaymentStatusUpdate;
import com.shopwise.objects.order.delhivery.ordercreate.OrderCreateResponse;
import com.shopwise.objects.order.delhivery.shippingcharge.ShippingChargeRequest;
import com.shopwise.objects.order.delhivery.shippingcharge.ShippingChargeResponse;

public interface HomeService {
	// dheeraj changes 
//	Products[] getSubCatProducts(int subCatId);
	Products[] getSubCatProducts(String subCatName);
//	Products[] getCatProducts(int catId);
	Products[] getCatProducts(String catName);
	
	// dheeraj changes 
	public Categories[] getCategories(int siteId);
	
	public SubCategoryList[] getSubcategory(int siteId);

	public Products[] getExclusiveProducts(int siteId);

//	public ProductDetail getProductDetails(int productId);
	
	public ProductDetail getProductDetails(String productName);

	public void saveWishlist(WishlistRequest wish);

	public WishlistResponse[] getWishlist(int customerId);

	public void removeFromWishlist(WishlistRequest wish);

	public void addProductReview(ReviewForm review);

	public String[] getsearchResponse(String query);

	public Products[] getSearchedProducts(String item);

	// public Products[] getSearchedProductsByCatId(Integer id);
	public CategoryList[] getAllCategories(int siteId);

	public Products[] getFeaturedProducts(int siteId);

	public CustomerDetails getCustomerDetails(int customerId);

	public OrderDetails getOrderDetails(String orderNum);

	public OrderHistory[] getOrderHistory(String orderNum);

	public CouponResponse checkCoupon(CouponForm couponForm);

	public String orderProducts(OrderDetails orderDetails);

	public void updatePaymentStatus(PaymentStatusUpdate payment);

	public ShippingChargeResponse getShippingCharge(ShippingChargeRequest shippingCharge);

	public OrderCreateResponse delhiveryOrderCreate(OrderDetails orderDetails, String orderNum);
	//New 
	public ProductDetail getProductDetailsByName(String productName);
	
}
