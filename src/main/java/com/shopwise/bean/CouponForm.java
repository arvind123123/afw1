package com.shopwise.bean;

public class CouponForm {

	private String couponCode;
	private int siteId;
	private int customerId;
	private double totalAmount;
	private String productId;
	private String subSubCategoryId;
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public int getSiteId() {
		return siteId;
	}
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getSubSubCategoryId() {
		return subSubCategoryId;
	}
	public void setSubSubCategoryId(String subSubCategoryId) {
		this.subSubCategoryId = subSubCategoryId;
	}
	
	
	
}
