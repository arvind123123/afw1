package com.shopwise.bean;

import java.util.List;

import com.shopwise.objects.order.BillingAddress;
import com.shopwise.objects.order.PaymentDetail;
import com.shopwise.objects.order.ProductDetail;
import com.shopwise.objects.order.ShippingAddress;

public class CheckoutForm {

	private BillingAddress billingaddress;
	private ShippingAddress shippingaddress;
	private boolean differentShipping;
	private String orderNotes;
	private String password;
	private String couponApplied;
	private String couponText;
	private double couponDiscount;
	private PaymentDetail paymentDetail;
	private int siteId;
	private int customerId;
	private List<ProductDetail> productList;
	private String orderNum;
	
	public BillingAddress getBillingaddress() {
		return billingaddress;
	}
	public void setBillingaddress(BillingAddress billingaddress) {
		this.billingaddress = billingaddress;
	}
	public ShippingAddress getShippingaddress() {
		return shippingaddress;
	}
	public void setShippingaddress(ShippingAddress shippingaddress) {
		this.shippingaddress = shippingaddress;
	}
	
	public boolean isDifferentShipping() {
		return differentShipping;
	}
	public void setDifferentShipping(boolean differentShipping) {
		this.differentShipping = differentShipping;
	}
	public String getOrderNotes() {
		return orderNotes;
	}
	public void setOrderNotes(String orderNotes) {
		this.orderNotes = orderNotes;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCouponApplied() {
		return couponApplied;
	}
	public void setCouponApplied(String couponApplied) {
		this.couponApplied = couponApplied;
	}
	
	public String getCouponText() {
		return couponText;
	}
	public void setCouponText(String couponText) {
		this.couponText = couponText;
	}
	public double getCouponDiscount() {
		return couponDiscount;
	}
	public void setCouponDiscount(double couponDiscount) {
		this.couponDiscount = couponDiscount;
	}
	public PaymentDetail getPaymentDetail() {
		return paymentDetail;
	}
	public void setPaymentDetail(PaymentDetail paymentDetail) {
		this.paymentDetail = paymentDetail;
	}
	public int getSiteId() {
		return siteId;
	}
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public List<ProductDetail> getProductList() {
		return productList;
	}
	public void setProductList(List<ProductDetail> productList) {
		this.productList = productList;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	
	
}
